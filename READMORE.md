Case: 1
Example: CS > superior court
Subgroup with no list of agencies, with NO ORI associated to it
The user input the ORI > server validate ORI > Fetch the reason if it is valid
Case: 2
Example: CS > supreme court
Subgroup with no list of agencies, with ONE ORI associated to it
Display the ORI associated subgroup
Fetch the reason using the ori, group and subgroup

Case: 3
Example: EA > private school
There are a list of agencies associated with the subgroup
Choose an agency and use the agency’s ORI to fetch the reason

Case: 4
Example: CCGC > firefighter
There only ONE agency associated with the subgroup
Use the agency’s ORI to fetch the reason

Case: 5
Example: DBHDD
There are no subgroups so just display the ORI associated to the group
Fetch the reason with the correct ORI

Case: 6
Example: DCH
There are group, subgroup and ORI tied to subgroup. Display second ORI as DCH to applicants, but for registrations, use the second ori provided from API
Fetch the reason with the correct ORI

Case Special: DECAL
This department (subgroup) has two special reasons, if chosen, will automatically choose payment method to be 'Agency' and, unlike normal case, will not append new text field 'Billing Code' and 'Billing Password'
