# FAQ

#### Update your distributionUrl

When setting up a new project, I may get this error:
Could not determine java version from '11.0.1'.

android/gradle/wrapper/gradle-wrapper.properties
distributionUrl=https\://services.gradle.org/distributions/gradle-5.0-all.zip

#### Remove The Wrapper Task

// task wrapper(type: Wrapper) {
// gradleVersion = '4.4'
// distributionUrl = distributionUrl.replace("bin", "all")
// }

#### Create Local Properties File

android/local.properties
sdk.dir = /Users/losethequit/Library/Android/sdk

#### Copy & Link Dependencies

copy package.json
npm install
react-native link

#### Copy Android Files

copy and page every thing below package:
com.\*\*\*\*\*\*
in MainApplication.java

<manifest xmlns:
    android="http://schemas.android.com/apk/res/android"
    package="com.musicplayer69">

in AndroidManifest.xml

##### Disconnected/Reconnected Phone

export ANDROID_HOME=~/Library/Android/sdk
export PATH=${PATH}:${ANDROID_HOME}/tools
export PATH=${PATH}:${ANDROID_HOME}/platform-tools

##### How to use git - TLDR

git checkout -b dev000
git add ./commit -am ""
git push origin dev000
git checkout master
git pull origin master
git merge dev000
git push origin master

##### Deprecated

export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-11.0.1.jdk/Contents/Home
export JAVA_HOME=â€œ/Library/Java/JavaVirtualMachines/jdk-11.0.1.jdk/Contents/Homeâ€

##### Release it!

keytool -genkey -v -keystore my-release-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000

adb shell input keyevent 82
launch developer mode on device without shaking it
