import React, { Component } from "react";
import { YellowBox } from "react-native";
import MainScreen from "./src/screens/MainScreen";

YellowBox.ignoreWarnings(["WebView"]);

export default class Setup extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <MainScreen />;
  }
}
