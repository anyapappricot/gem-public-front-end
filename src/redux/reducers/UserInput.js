import {
  GET_DEPARTMENTS,
  GET_DEPARTMENT_INDEX,
  SAVE_DATA,
  SET_REASONS,
  SET_REGISTRATION_DATA,
  GET_REG_ID,
  POST_NEW_REGISTRATION,
  SAVE_CHECKBOX,
  SAVE_CHECKBOX_SAME_ADDRESS,
  SET_GROUP_ID,
  SET_SUBGROUP_ID,
  SET_ORI,
  SET_AGENCY_PAY,
  RESET_AGENCY_PAY
} from "../../redux/actions/types";

const initialState = {
  datas: [],
  demographics: {},
  reasons: [],
  registration_data: {},
  formData: {},
  reg_id: null,
  new_reg_data: null,
  checkBox: "N",
  check_box_same_address: false,
  grp_id: null,
  subgrp_id: null,
  ori: null,
  agency_pay_result: null
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_AGENCY_PAY: {
      return {
        ...state,
        agency_pay_result: action.data
      };
    }
    case RESET_AGENCY_PAY: {
      return {
        ...state,
        agency_pay_result: action.data
      };
    }
    case SET_GROUP_ID: {
      return {
        ...state,
        grp_id: action.data
      };
    }
    case SET_SUBGROUP_ID: {
      return {
        ...state,
        subgrp_id: action.data
      };
    }
    case SET_ORI: {
      return {
        ...state,
        ori: action.data
      };
    }

    case SET_REGISTRATION_DATA: {
      return {
        ...state,
        registration_data: action.data
      };
    }
    case SET_REASONS: {
      return {
        ...state,
        reasons: action.data
      };
    }
    case GET_DEPARTMENTS: {
      return {
        ...state,
        datas: action.data
      };
    }
    case GET_DEPARTMENT_INDEX: {
      return {
        ...state,
        [action.key]: action.datas
      };
    }
    case SAVE_DATA: {
      // console.warn("KEY", action.dataField);
      // console.warn("VALUE", action.data);
      return {
        ...state,
        formData: {
          [action.dataField]: action.data
        }
      };
    }
    case GET_REG_ID: {
      return {
        ...state,
        reg_id: action.data
      };
    }
    case POST_NEW_REGISTRATION: {
      return {
        ...state,
        new_reg_data: action.data
      };
    }
    case SAVE_CHECKBOX: {
      return {
        ...state,
        checkBox: action.data
      };
    }
    case SAVE_CHECKBOX_SAME_ADDRESS: {
      return {
        ...state,
        check_box_same_address: action.data
      };
    }
    default:
      return state;
  }
};
