import { SET_DEMOGRAPHIC_DATA } from "../actions/types";

import demographics from "../../assets/json/demographics";

const initialState = {
  data: demographics
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DEMOGRAPHIC_DATA: {
      return {
        ...state,
        data: action.data
      };
    }
    default:
      return state;
  }
};
