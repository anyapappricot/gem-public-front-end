import { GET_LOCATION } from "../../redux/actions/types";

const initialState = {
  datas: {}
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LOCATION: {
      return {
        ...state,
        datas: action.datas
      };
    }
    default:
      return state;
  }
};
