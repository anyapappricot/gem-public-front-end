import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";
import { reducer as formReducer } from "redux-form";
import { reducer as userInputReducer } from "../../redux/reducers/UserInput";
import { reducer as locationReducer } from "../../redux/reducers/LocationReducer";
import { reducer as demographicsReducer } from "../reducers/DemographicReducer";
import { reducer as DevReducer } from "../reducers/DevReducer";

const rootReducer = combineReducers({
  form: formReducer,
  userInput: userInputReducer,
  locations: locationReducer,
  demographics: demographicsReducer,
  dev_reducer: DevReducer
});

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunkMiddleware))
);

export default store;
