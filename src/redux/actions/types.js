export const SET_DEMOGRAPHIC_DATA = "SET_DEMOGRAPHIC_TYPE";
export const GET_DEPARTMENTS = "GET_DEPARTMENTS";
export const GET_DEPARTMENT_INDEX = "GET_DEPARTMENT_INDEX";
export const SAVE_DATA = "SAVE_DATA";
export const SAVE_DATA_DEV = "SAVE_DATA_DEV";
export const RECEIVE_DATA = "RECEIVE_DATA";
export const REQUEST_SUCCEEDED = "REQUEST_SUCCEEDED";
export const REQUEST_FAILED = "REQUEST_FAILED";
export const GET_LOCATION = "GET_LOCATION";
export const SET_REASONS = "SET_REASONS";
export const SET_REGISTRATION_DATA = "SET_REGISTRATION_DATA";
export const GET_REG_ID = "GET_REG_ID";
export const POST_NEW_REGISTRATION = "POST_NEW_REGISTRATION";
export const SET_GROUP_ID = "SET_GROUP_ID";
export const SET_SUBGROUP_ID = "SET_SUBGROUP_ID";
export const SET_ORI = "SET_ORI";
export const SAVE_CHECKBOX = "SAVE_CHECKBOX";
export const SAVE_CHECKBOX_SAME_ADDRESS = "SAVE_CHECKBOX_SAME_ADDRESS";
export const SET_AGENCY_PAY = "SET_AGENCY_PAY";
export const RESET_AGENCY_PAY = "RESET_AGENCY_PAY";
export const SAVE_TEST = "SAVE_TEST";
