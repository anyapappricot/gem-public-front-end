import axios from "axios";

import {
  SET_DEMOGRAPHIC_DATA,
  GET_DEPARTMENTS,
  GET_DEPARTMENT_INDEX,
  SAVE_DATA,
  GET_LOCATION,
  SET_REASONS,
  SET_REGISTRATION_DATA,
  GET_REG_ID,
  POST_NEW_REGISTRATION,
  SAVE_CHECKBOX,
  SET_GROUP_ID,
  SET_SUBGROUP_ID,
  SET_ORI,
  SAVE_DATA_DEV,
  SAVE_CHECKBOX_SAME_ADDRESS,
  SET_AGENCY_PAY,
  RESET_AGENCY_PAY
} from "./types";

export const saveData = (dataField, data) => ({
  type: SAVE_DATA,
  dataField,
  data
});

export const save_data_dev = (dataField, data) => ({
  type: SAVE_DATA_DEV,
  dataField,
  data
});

export const saveCheckBox = check_box => ({
  type: SAVE_CHECKBOX,
  data: check_box
});

export const saveCheckBoxSameAddress = check_box_same_address => ({
  type: SAVE_CHECKBOX_SAME_ADDRESS,
  data: check_box_same_address
});

const setGroupId = grp_id => ({ type: SET_GROUP_ID, data: grp_id });
const setSubGroupId = subgrp_id => ({ type: SET_SUBGROUP_ID, data: subgrp_id });
const setORI = ori => ({ type: SET_ORI, data: ori });
const setDemographicData = data => ({ type: SET_DEMOGRAPHIC_DATA, data });
const getDepartments = data => ({ type: GET_DEPARTMENTS, data });

export const setDepartmentIndex = (key, datas) => ({
  type: GET_DEPARTMENT_INDEX,
  key,
  datas
});

const postNewRegistrationToServer = data => ({
  type: POST_NEW_REGISTRATION,
  data
});

const getLocations = datas => ({ type: GET_LOCATION, datas });
const setReasons = data => ({ type: SET_REASONS, data });
const set_agency_pay = data => ({ type: SET_AGENCY_PAY, data });
export const reset_agency_pay = () => ({ type: RESET_AGENCY_PAY, data: null });
const setRegistrationData = data => ({ type: SET_REGISTRATION_DATA, data });

const getRegID = data => ({ type: GET_REG_ID, data });

const HOME_API_BASE_URL = "http://localhost:8787";
const DEV_API_BASE_URL = "https://gem-alto-clxxxii.c9users.io";
const PROD_API_BASE_URL = "https://pciqa.aps.gemalto.com/api";

const API_URL = PROD_API_BASE_URL;

export function postNewApplicant(data) {
  return dispatch =>
    axios({
      method: "POST",
      url: `${API_URL}/applicant`,
      data: data
    })
      .then(response => {
        // console.warn("POST SUCCESSFUL", response.data);
        dispatch(postNewRegistrationToServer(response.data.status));
      })
      .catch(error => {
        // console.warn(
        //   `ERROR WITH postNewApplicant: `,
        //   JSON.stringify(error, null)
        // );
        dispatch(postNewRegistrationToServer(-1));
      });
}

export function agency_pay(trans_type, ori, oca, billing_password) {
  const payload = {
    trans_type,
    ori,
    oca,
    billing_password
  };

  return dispatch =>
    axios({
      method: "GET",
      url: `${API_URL}/validate/billing`,
      params: payload
    })
      .then(response => {
        dispatch(set_agency_pay(response.data));
      })
      .catch(error => {
        // dispatch(setReasons(default_reason.reasons));
      });
}

export function getRegistrationStatus(data) {
  return dispatch =>
    axios({
      method: "POST",
      url: `${API_URL}/registration`,
      data: data
    })
      .then(response => {
        dispatch(setRegistrationData(response.data));
        // console.warn("!", JSON.stringify(response.data, null, 2));
      })
      .catch(error => {
        // console.warn("err", JSON.stringify(error, null, 2));
      });
}

export const resetRegistrationStatus = data => {
  return dispatch => dispatch(setRegistrationData({ data: {} }));
};

export function fetchDepartments() {
  return dispatch =>
    axios
      .get(`${API_URL}/group`)
      .then(response => {
        dispatch(getDepartments(response.data));
      })
      .catch(error => {
        // console.warn(`ERROR WITH fetchDepartments: `, error);
      });
}

export function getRegistrationID() {
  return dispatch =>
    axios
      .get(`${API_URL}/applicant`)
      .then(response => {
        dispatch(getRegID(response.data.reg_id));
      })
      .catch(error => {
        // console.warn(`ERROR WITH getRegID: `, error);
      });
}

const default_reason = {
  reasons: ["There are no reasons associated with this department..."]
};

export function fetchReasons(grp_id, subgrp_id, ori) {
  const payload = {
    grp_id,
    subgrp_id,
    ori
  };

  return dispatch =>
    axios({
      method: "GET",
      url: `${API_URL}/validate/reason`,
      params: payload
    })
      .then(response => {
        response.data.reasons.length !== 0
          ? dispatch(setReasons(response.data.reasons))
          : dispatch(setReasons(default_reason.reasons));

        dispatch(setGroupId(grp_id || `_null_`));
        dispatch(setSubGroupId(subgrp_id || `_null_`));
        dispatch(setORI(ori || `_null_`));
      })
      .catch(error => {
        dispatch(setReasons(default_reason.reasons));
      });
}

export function fetchDemographics() {
  return dispatch =>
    axios
      .get(`${API_URL}/demographic`)
      .then(response => {
        dispatch(setDemographicData(response.data));
      })
      .catch(error => {
        // console.warn(`ERROR WITH fetchDemographic: `, error);
      });
}

export function fetchLocation() {
  return dispatch =>
    axios
      .get(`${API_URL}/locations`)
      .then(response => {
        dispatch(getLocations(response.data));
      })
      .catch(error => {
        // console.warn(`ERROR WITH getRegID: `, error);
      });
}
