export const required = value =>
  value || typeof value === "number" ? undefined : "Required";

export const required_ga = value =>
  value[0] === "G" && value[1] === "A" && value.length === 9
    ? undefined
    : "Not a valid Agency ID";

export const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;
export const maxLength100 = maxLength(100);
export const maxLength50 = maxLength(50);
export const maxLength30 = maxLength(30);
export const maxLength25 = maxLength(25);
export const maxLength9 = maxLength(9);
export const maxLength8 = maxLength(8);
export const maxLength5 = maxLength(5);
export const maxLength3 = maxLength(3);
export const minLength = min => value =>
  value && value.length < min ? `Must be ${min} characters or more` : undefined;
export const minLength1 = minLength(1);
export const SSNMatch = (value, allValues) =>
  value !== allValues.SSN ? "Don't match" : undefined;
export const addressMatch = (value, allValues) =>
  value !== allValues.address ? "Don't match" : undefined;
export const cityMatch = (value, allValues) =>
  value !== allValues.city ? "Don't match" : undefined;
export const zipcodeMatch = (value, allValues) =>
  value !== allValues.zipcode ? "Don't match" : undefined;
export const number = value =>
  value && isNaN(Number(value)) ? "Must be a number" : undefined;
export const minValue = min => value =>
  value && value < min ? `Must be at least ${min}` : undefined;
export const minValue13 = minValue(13);
export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Invalid email address"
    : undefined;
export const tooYoung = value =>
  value && value < 13
    ? "You do not meet the minimum age requirement!"
    : undefined;
export const aol = value =>
  value && /.+@aol\.com/.test(value)
    ? "Really? You still use AOL for your email?"
    : undefined;
export const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? "Only alphanumeric characters and/or hyphen"
    : undefined;

export const alphaNumericPlusHyphen = value =>
  value && /[^a-zA-Z0-9\- ]/i.test(value)
    ? "Only alphanumeric characters and/or hyphen"
    : undefined;

export const phoneNumber = value =>
  value && !/^(0|[1-9][0-9]{9})$/i.test(value)
    ? "Invalid phone number, must be 10 digits"
    : undefined;
export const birthDate = value =>
  value && !/^(0[1-9]|1[0-2])(0[1-9]|1\d|2\d|3[01])(19|20)\d{2}$/.test(value)
    ? "Needs to be in format MMDDYYYY"
    : undefined;
