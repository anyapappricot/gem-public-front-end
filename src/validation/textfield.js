import React from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import { Input, Text } from 'native-base';

const TextField = props => (
  <View style={style.input} >
  <Input 
      style={{ padding: 5 }}
  />
  props.error ? <Text>{props.error}</Text> : null
</View>
);

export default TextField;

const style = StyleSheet.create({
    input: {
        top: Platform.OS === 'ios' ? 10 : 20,
        marginBottom: Platform.OS === 'ios' ? 0 : 20,
        width: Platform.OS === 'android' ? 380 : 330,
        right: Platform.OS === 'ios' ? 25 : 0,
        borderWidth: 1,
        borderColor: '#707070',
        textAlign: 'left'
    },
});
