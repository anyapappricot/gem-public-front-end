const validation = {
    lastName: {
        // You need to pick a username too
        presence: true,
        // And it must be between 3 and 20 characters long
        length: {
          minimum: 3,
          maximum: 20
        },
        format: {
          // We don't allow anything that a-z and 0-9
          pattern: '[a-z0-9]+',
          // but we don't care if the username is uppercase or lowercase
          flags: 'i',
          message: 'can only contain a-z and 0-9'
        }
      },
      firstName: {
        // You need to pick a username too
        presence: true,
        // And it must be between 3 and 20 characters long
        length: {
          minimum: 3,
          maximum: 20
        },
        format: {
          // We don't allow anything that a-z and 0-9
          pattern: '[a-z0-9]+',
          // but we don't care if the username is uppercase or lowercase
          flags: 'i',
          message: 'can only contain a-z and 0-9'
        }
      },
      middletName: {
        // You need to pick a username too
        presence: true,
        // And it must be between 3 and 20 characters long
        length: {
          minimum: 3,
          maximum: 20
        },
        format: {
          // We don't allow anything that a-z and 0-9
          pattern: '[a-z0-9]+',
          // but we don't care if the username is uppercase or lowercase
          flags: 'i',
          message: 'can only contain a-z and 0-9'
        }
      },
      SSN: {
        // SSN is also required
        presence: true,
        // And must be at least 5 characters long
        length: {
          minimum: 5
        }
      },
      confirmSSN: {
        // You need to confirm your SSN
        presence: true,
        // and it needs to be equal to the other SSN
        equality: {
          attribute: 'SSN',
          message: '^The SSNs does not match'
        }
      },
  };
  
  export default validation
;
