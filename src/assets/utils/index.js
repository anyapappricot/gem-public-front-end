export const _U = str =>
  str === null || str === undefined || str === "_null_"
    ? null
    : str.toUpperCase().trim();

export const company_email = "garegistration@gemalto.com";
export const company_phone_number = "1-888-439-2512";
