import { StyleSheet, Dimensions } from "react-native";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

module.exports = StyleSheet.create({
  kb_container: { justifyContent: "center" },
  form_progress_container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 20
  },
  checkbox_container: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  modal_button_color: { color: "white" },
  modal_button_width: { width: width / 3 },
  modal_container: {
    flex: 1,
    justifyContent: "center",
    padding: 20
  },
  modal_headline: {
    fontSize: 50,
    color: "#E06C08",
    paddingTop: 50,
    paddingBottom: 25
  },
  modal_container: {
    fontSize: 50,
    color: "#E06C08",
    paddingTop: 50,
    paddingBottom: 25
  },
  modal_text: { fontSize: 18, color: "#000000", paddingBottom: 25 },
  modal_button_container: { flexDirection: "row", justifyContent: "center" },
  modal_padding: { padding: 20 },
  mail_address_container: { flexDirection: "column" },
  mail_address_headline: { alignSelf: "center", paddingBottom: 20 },
  mail_address_cta: { flexDirection: "row" },

  activity_indicator_container: {
    height: height,
    justifyContent: "center",
    alignItems: "center"
  },
  submit_container: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginTop: 25,
    marginBottom: 50
  },
  checkbox_text_container: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 40,
    paddingRight: 40
  },
  blue_line: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: "#01366C",
    height: 0.5,
    width: width - 80
  },
  newRegistrationStyle: {
    fontFamily: "Roboto-Medium",
    fontSize: 15,
    textAlign: "center",
    color: "#1F1F1D",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    padding: 20
  },
  column_centered: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  sectionStyle: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  sectionTextStyle: {
    fontSize: 15,
    fontFamily: "Segoeui",
    color: "#01366C"
  },
  tickBoxiOS: {
    left: 20,
    width: 25,
    height: 25
  },
  tickBoxAndroid: {
    margin: 10,
    width: 20,
    height: 20,
    borderRadius: 0,
    borderColor: "#707070"
  },
  tickText: {
    margin: 10,
    overflow: "hidden",
    fontFamily: "OpenSans-Regular",
    fontSize: 13,
    fontWeight: "bold",
    color: "#3e4649"
  },
  progressBar:
    width < 361
      ? {
          width: 355 / 1.25,
          height: 40 / 1.25
          // alignSelf: "center",
          // justifyContent: "center",
          // alignItems: "center",
          // flexDirection: "column",
          // marginTop: 30,
          // marginBottom: 30,
          // marginLeft: 30
        }
      : width > 760
      ? {
          // alignSelf: "center",
          // justifyContent: "center",
          // alignItems: "center",
          // flexDirection: "column",
          width: 450,
          height: 50
          // marginTop: 20,
          // marginBottom: 20,
          // marginLeft: 40
          // backgroundColor: "green"
        }
      : {
          // justifyContent: "center",
          // alignItems: "center",
          width: 360,
          height: 50
          // marginTop: 20,
          // marginBottom: 20,
          // marginLeft: 20
          // backgroundColor: "red"
        },

  checkBoxText: {
    flex: 1,
    textAlign: "center",
    // bottom: 20,
    // left: 20,
    width: 330,
    color: "#7A7A7A",
    fontFamily: "Segoeui",
    fontSize: 14
  },
  modalWrapper: { flex: 1, justifyContent: "center", height: 300 },
  modalHeaderText: {
    fontSize: 50,
    color: "#E06C08",
    paddingTop: 50,
    paddingBottom: 25
  },
  modalText: {
    fontSize: 18,
    color: "#000",
    paddingBottom: 25
  }
});
