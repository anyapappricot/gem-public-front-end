"use strict";
import { StyleSheet, Platform } from "react-native";

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  modalWrapper: { flex: 1, justifyContent: "center" },
  modalHeaderText: {
    fontSize: 50,
    color: "#E06C08",
    paddingTop: 50,
    paddingBottom: 25
  },
  modalText: {
    fontSize: 18,
    color: "#000",
    paddingBottom: 25
  },
  bodyStyleWrapper: { margin: 20, flexDirection: "row" },
  screenTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  bodyDepartmentLogo: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  bodyTextWrapper1: {
    flex: 3,
    justifyContent: "center",
    alignItems: "center"
  },
  bodyTextWrapper2: {
    textAlign: "center",
    fontFamily: "OpenSans-Regular",
    fontSize: 12
  },
  bodyTextWrapper3: {
    textAlign: "center",
    fontFamily: "OpenSans-Regular",
    fontSize: 17
  },
  line: {
    textAlign: "center",
    borderColor: "#C4C4C4",
    height: 1,
    borderWidth: 0.5,
    marginRight: 20,
    marginLeft: 20,
    marginTop: 20,
    marginBottom: 20
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  },
  departmentLogo: {
    margin: 12,
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  }
});
