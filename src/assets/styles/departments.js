"use strict";
import { StyleSheet, Platform } from "react-native";

module.exports = StyleSheet.create({
  contacts: {
    padding: 25,
    alignSelf: "flex-start"
  },
  contactInfoTitle: {
    fontFamily: "OpenSans-Semibold",
    fontSize: 11,
    color: "#2A50A6"
  },
  contactInfo: {
    fontFamily: "OpenSans-Regular",
    fontSize: 11,
    color: "#787878",
    textDecorationLine: "underline"
  },
  contactInfoTitleGrey: {
    fontFamily: "OpenSans-Semibold",
    fontSize: 11,
    color: "#1F1F1D"
  },
  contactInfoSubTitle: {
    fontFamily: "OpenSans-Regular",
    fontSize: 11,
    color: "#787878",
    textDecorationLine: "underline"
  },
  website: {
    fontFamily: "OpenSans-Regular",
    fontSize: 11,
    color: "#1E73BE",
    textDecorationLine: "underline"
  },
  subTitle: {
    fontFamily: "OpenSans-Semibold",
    fontWeight: "400",
    fontSize: 12,
    textAlign: "center",
    top: 15
  },

  // HIGHLY QUESTIONABLE

  button: {
    justifyContent: "center",
    alignItems: "center",
    top: 20,
    marginHorizontal: Platform.OS === "ios" ? 50 : 80,
    marginBottom: 170,
    width: 259,
    height: 32,
    backgroundColor: "#b8bdc4",
    borderRadius: 0
  }
});
