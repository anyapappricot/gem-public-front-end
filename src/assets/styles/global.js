"use strict";
import { StyleSheet, Platform } from "react-native";

module.exports = StyleSheet.create({
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 11,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "left",
    color: "#1f1f1d",
    paddingTop: 25,
    paddingLeft: 25,
    paddingRight: 20
  }
});
