const department_data = {
  DJJ: {
    image_url: require("../departments/6_DJJ.png")
  },
  DPH: {
    image_url: require("../departments/4_DPH.png")
  },
  EA: {
    image_url: require("../departments/13_EA.png")
  },
  GBI: {
    image_url: require("../departments/17_GBI.png")
  },
  OIC: {
    image_url: require("../departments/5_OIC.png")
  },
  RECAB: {
    image_url: require("../departments/10_RECAB.png")
  },
  SOS: {
    image_url: require("../departments/8_SOS.png")
  },
  DOD: {
    image_url: require("../departments/18_DOD.png")
  },
  GVRA: {
    image_url: require("../departments/12_GVRA.png")
  },
  CCGC: {
    image_url: require("../departments/15_CCGC.png")
  },
  CS: {
    image_url: require("../departments/1_GCS.png")
  },
  DBF: {
    image_url: require("../departments/16_DBF.png")
  },
  DBHDD: {
    image_url: require("../departments/2_DBHDD.png")
  },
  DCH_ORI: {
    image_url: require("../departments/6_DJJ.png")
  },
  DDS: {
    image_url: require("../departments/9_DDS.png")
  },
  DECAL: {
    image_url: require("../departments/7_DECAL.png")
  },
  DHS: {
    image_url: require("../departments/11_DHS.png")
  },
  DCS: {
    image_url: require("../departments/19_DCS.png")
  },
  DCH: {
    image_url: require("../departments/3_DCH.png")
  },
  DR: {
    image_url: require("../departments/6_DJJ.png")
  }
};

export default department_data;
