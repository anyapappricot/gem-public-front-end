import React, { Component } from "react";
import { View, StyleSheet, Platform } from "react-native";
import { Container, Content, Button, Right, Text, List } from "native-base";
import Image from "react-native-remote-svg";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";
import DepartmentHeader from "../components/departments/DepartmentHeader";
import BodyWrapper from "../components/BodyWrapper";

import CustomListItem from "../components/CustomListItem";
import BackButton from "../components/BackButton";

class CompleteNotConnected extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content
            style={{
              flex: 1,
              width: Platform.OS === "android" ? 500 : "auto",
              backgroundColor: "#ffffff"
            }}
          >
            <DepartmentHeader />
            <View
              style={{
                flexDirection: "row",
                alignSelf: "center",
                marginBottom: 0
              }}
            >
              <Text style={style.subTitle}>Registration Complete</Text>
              <Image
                style={{ top: 10, right: Platform.OS === "ios" ? -10 : 30 }}
                source={require("../../src/assets/departments/smallSuccess.png")}
              />
            </View>
            <Text
              style={{
                alignSelf: "center",
                color: "#c8c8c8",
                right: Platform.OS === "ios" ? 0 : 40
              }}
            >
              _______________________________________________
            </Text>

            <Text style={style.receipt}>Receipt</Text>

            <View style={style.box}>
              <Image
                style={{ top: 20, left: 30 }}
                source={require("../../src/assets/departments/barcode.png")}
              />
              <List style={{ paddingTop: 20, paddingBottom: 20 }}>
                <CustomListItem label="Reviewing Agency ID:" text="input" />
                <CustomListItem label="ORI:" text={this.props.reviewAgencyID} />
                <CustomListItem label="Last Name:" text={this.props.lastName} />
                <CustomListItem
                  label="First Name:"
                  text={this.props.firstName}
                />
                <CustomListItem
                  label="Transaction Type:"
                  text={this.props.reason}
                />
                <CustomListItem
                  label="Payment Type:"
                  text={this.props.payment}
                />
                <CustomListItem label="Transaction Fee:" text="FPO" />
                <CustomListItem label="Payment Confirmation #:" text="FPO" />
                <CustomListItem label="Message:" text="FPO" />
              </List>
            </View>

            <View
              style={{
                top: 20,
                right: Platform.OS === "ios" ? 0 : 40,
                bottom: 30,
                alignSelf: "center",
                height: 250
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  width: 325,
                  fontSize: 15,
                  fontFamily: "Segoeui",
                  color: "#3E4649"
                }}
              >
                This registration will expire after 90 days from the
                registration date. It will be cancelled and any payment refunded
                at this time if the applicant has not been fingerprinted Please
                print information if needed and present it to fingerprint site.
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("HomeScreen")}
          >
            <BackButton />
          </Button>
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  reviewAgencyID:
    state.form && state.form.s1 && state.form.s1.values
      ? state.form.s1.values.reviewAgencyID
      : null,
  reason:
    state.form && state.form.s1 && state.form.s1.values
      ? state.form.s1.values.reason
      : null,
  // payment:
  // state.form && state.form.s1 & state.form.s1.values
  //   ? state.form.s1.values.payment
  //   : null,
  lastName:
    state.form && state.form.s2 && state.form.s2.values
      ? state.form.s2.values.lastName
      : null,
  firstName:
    state.form && state.form.s2 && state.form.s2.values
      ? state.form.s2.values.firstName
      : null,
  UserInput: state.userInput.formData
});

const Complete = connect(mapStateToProps)(CompleteNotConnected);

export default withNavigation(Complete);

const style = StyleSheet.create({
  departmentBg: {
    width: "auto",
    height: 74.45703125,
    backgroundColor: "#f1f4f8"
  },
  courtLogo: {
    top: 8,
    left: 8,
    width: 52,
    height: 52
  },
  departmentTitle: {
    bottom: 30,
    left: 80,
    width: 250,
    height: 60,
    overflow: "hidden",
    fontFamily: "OpenSans-Regular",
    fontSize: 13,
    fontWeight: "bold",
    textAlign: "left",
    color: "#3e4649"
  },
  subTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 19,
    textAlign: "center",
    right: Platform.OS === "ios" ? 0 : 40,
    top: 15,
    paddingBottom: Platform.OS === "ios" ? 0 : 20,
    color: "#01366C"
  },
  box: {
    top: Platform.OS === "ios" ? 0 : 5,
    left: Platform.OS === "ios" ? 40 : 45,
    bottom: Platform.OS === "ios" ? 0 : 50,
    width: 329,
    height: Platform.OS === "ios" ? 650 : 560,
    borderRadius: 3,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#c8c8c8",
    backgroundColor: "#ffffff"
  },
  input: {
    top: 10,
    width: 330,
    height: 20,
    borderWidth: 1,
    borderColor: "#707070",
    textAlign: "center",
    fontFamily: "Segoeui-Semibold"
  },
  receipt: {
    paddingTop: 10,
    paddingLeft: 40,
    fontFamily: "Segoeui",
    fontSize: 15,
    textAlign: "left",
    color: "#2c2c29"
  }
});
