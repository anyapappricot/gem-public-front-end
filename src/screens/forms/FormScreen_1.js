import React, { Component } from "react";
import { View, Dimensions } from "react-native";

import Image from "react-native-remote-svg";
import { Button, Text, Form } from "native-base";
import { withNavigation } from "react-navigation";

import { Field, reduxForm, formValueSelector } from "redux-form";
import { connect } from "react-redux";

import {
  fetchReasons,
  getRegistrationID,
  saveCheckBox,
  agency_pay,
  reset_agency_pay
} from "../../redux/actions/actions";

import {
  required,
  minLength1,
  required_ga
} from "../../validation/validations";

import BodyWrapper from "../../components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import SmartInput from "../../components/SmartInput";
import SmartButton from "../../components/SmartButton";
import BackButton from "../../components/BackButton";
import SmartCheckBox from "../../components/SmartCheckBox";
import styles from "../../assets/styles/forms";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const width = Dimensions.get("window").width;

class FormScreen_1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boxTicked: false,
      paymentType: null,
      isModalVisible: false
    };

    this.setPaymentType = this.setPaymentType.bind(this);
    this.toggleTick = this.toggleTick.bind(this);
    this.toggleImage = this.toggleImage.bind(this);
    // these imports are useless
  }

  setPaymentType = paymentType => {
    this.setState({
      paymentType
    });
  };

  toggleTick = async () => {
    await this.setState({
      boxTicked: !this.state.boxTicked
    });

    this.props.dispatch(saveCheckBox(this.state.boxTicked ? "Y" : "N"));
  };

  componentWillUnmount = () => {};

  toggleImage() {
    const ticked = require("../../assets/departments/checked.png");
    const notTicked = require("../../assets/departments/uncheck.png");
    return this.state.boxTicked ? ticked : notTicked;
  }

  submit = () => {
    this.props.navigation.navigate("FormScreen_2");
  };

  renderReasons() {
    const grp_id = this.props.departments[this.props.departmentIndex].grp_id;
    const subgrp_id = this.props.UserInput.subgroup.subgrp_id;
    const ori = this.props.reviewing_agency_id;

    this.props.dispatch(fetchReasons(grp_id, subgrp_id, ori));
    this.props.dispatch(getRegistrationID());
  }

  renderReviewingAgencyORIField = group => {
    const { side_bar_switch } = this.state;
    if (group.subgroup.agency.length === 0 && group.subgroup.ori === null) {
      // CASE 1: Subgroup with no list of agencies, with NO ORI associated to it
      // court service > superior court
      // console.warn("CASE: 1");
      return (
        <Field
          label="Reviewing Agency ID *"
          name="reviewAgencyID"
          component={SmartInput}
          type={"text"} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
          auto_fill={side_bar_switch ? "GA044015Y" : null}
          validate={[required, minLength1, required_ga]}
        />
      );
    }

    if (group.subgroup.agency.length === 0 && group.subgroup.ori !== null) {
      // CASE 2: Subgroup with no list of agencies, with ONE ORI associated to it
      // court service > supreme court
      // console.warn("CASE: 2");

      return (
        <Field
          label="Reviewing Agency ID *"
          name="reviewAgencyID"
          component={SmartInput}
          type={"text"} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
          auto_fill={side_bar_switch ? "GA044015Y" : null}
          validate={[required, minLength1, required_ga]}
        />
      );
    }
    if (group.subgroup.agency.length > 1 && group.subgroup.ori === null) {
      // CASE 3: There are a list of agencies associated with the subgroup
      // education > private school
      //Anya's note: no longer needed - handled by SmartInput.js
      // console.warn("CASE: 3");

      return (
        <Field
          type={"ori"}
          label="Reviewing Agency ID *"
          name="reviewAgencyID"
          component={SmartInput} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
          auto_fill={side_bar_switch ? "GA044015Y" : null}
          validate={[required, minLength1, required_ga]}
        />
      );
    }
    if (group.subgroup.agency.length === 1 && group.subgroup.ori === null) {
      // CASE 4: There is only ONE agency associated with the subgroup
      // education > private school
      // console.warn("CASE: 4");

      if (group.subgroup.subgrp_id === "AUT") {
        return (
          <Field
            label="Reviewing Agency ID *"
            name="reviewAgencyID"
            component={SmartInput}
            agency_info={group.subgroup.agency[0].name}
            type={"EDGE_CASE"} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
            auto_fill={side_bar_switch ? "GA044015Y" : null}
            validate={[required, minLength1, required_ga]}
          />
        );
      } else {
        return (
          <Field
            label="Reviewing Agency ID *"
            name="reviewAgencyID"
            component={SmartInput}
            type={"text"} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
            auto_fill={side_bar_switch ? "GA044015Y" : null}
            validate={[required, minLength1, required_ga]}
          />
        );
      }
    }

    // if (group.subgroup.name === "MEDICAID HIGH RISK PROVIDERS") {
    //   // CASE 6: DCH
    //   // I COULD ADD
    //   // || group.subgroup.name === "GCHEXS REGISTRATIONS"
    //   // ASK TIFF

    //   // THIS COULD ALSO BE NESTED IN CASE 2
    //   return (
    //     <Field
    //       label="Reviewing Agency ID *"
    //       name="reviewAgencyID"
    //       component={SmartInput}
    //       type={"text"} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
    //       validate={[required, minLength1]}
    //       grp_id={this.props.departments[this.props.departmentIndex].grp_id} // has to be 'DCH'
    //     />
    //   );
    // }
    //
  };

  renderPayment = side_bar_switch => {
    if (
      this.props.selected_reason ===
        "DECAL- Child Care Provider/Director/Employee. Support Center Employee." ||
      this.props.selected_reason ===
        "TCSG - Technical College Student or Employee Providing Direct Child Care"
    ) {
      return (
        <View>
          <Field
            disabledField={{ disabled: true }}
            type={"payment"}
            label="Payment *"
            name="payment"
            component={SmartInput}
            setPaymentType={this.setPaymentType}
            auto_fill={side_bar_switch ? "Agency" : "Agency"}
            validate={side_bar_switch ? null : [required, minLength1]}
          />
          {/* <Field type="hidden" name="payment" component={SmartInput} /> */}
        </View>
      );
    } else {
      return (
        <Field
          type={"payment"}
          label="Payment *"
          name="payment"
          component={SmartInput}
          setPaymentType={this.setPaymentType}
          auto_fill={side_bar_switch ? "Credit Card" : null}
          validate={side_bar_switch ? null : [required, minLength1]}
        />
      );
    }
  };

  _go_back = () => {
    this.props.navigation.goBack();
  };

  _reset_agency_pay = async () => {
    const { dispatch } = this.props;
    try {
      await dispatch(reset_agency_pay());
    } catch (e) {
      console.warn("ERRRR", e);
    }
  };

  _handle_submit = async callback => {
    const {
      change,
      handleSubmit,
      reset,
      reviewing_agency_id,
      requesting_agency_id,
      side_bar_switch,
      selected_reason,
      payment,
      navigation,
      billing_code,
      billing_password,
      agency_pay_result,
      dispatch
    } = this.props;

    if (this.state.paymentType !== "Agency") {
      callback();
    } else {
      if (
        this.props.selected_reason ===
          "DECAL- Child Care Provider/Director/Employee. Support Center Employee." ||
        this.props.selected_reason ===
          "TCSG - Technical College Student or Employee Providing Direct Child Care"
      ) {
        // console.warn("selected_reason - is EDGE CASE");
        const trans_type = selected_reason;
        const ori = requesting_agency_id;
        const oca = billing_code;
        const billing_password = billing_password;

        // console.warn(trans_type);
        // console.warn(ori);
        // console.warn(oca);
        // console.warn(billing_password);

        if (!agency_pay_result) {
          await dispatch(agency_pay(trans_type, ori, oca, billing_password));
          this._handle_submit(callback);
        }
      } else {
        // console.warn("selected_reason - is NOT EDGE CASE");
        const trans_type = selected_reason;
        const ori = requesting_agency_id;
        const oca = billing_code;
        const billing_password = billing_password;

        if (!agency_pay_result) {
          await dispatch(agency_pay(trans_type, ori, oca, billing_password));
          this._handle_submit(callback);
        }
      }
    }
  };

  render() {
    const {
      handleSubmit,
      reset,
      reviewing_agency_id,
      side_bar_switch,
      agency_pay_result
    } = this.props;
    return (
      <BodyWrapper
        {...this.props}
        body={
          !agency_pay_result ? (
            <KeyboardAwareScrollView
              contentContainerStyle={{ justifyContent: "center" }}
            >
              <DepartmentHeader />

              <View>
                <Text style={styles.newRegistrationStyle}>
                  New Registration: Please Enter Your Information
                </Text>
              </View>
              <View style={styles.form_progress_container}>
                <Image
                  style={styles.progressBar}
                  source={require("../../assets/forms/form_progress_1.svg")}
                />
              </View>

              <View style={styles.sectionStyle}>
                <Text style={styles.sectionTextStyle}>
                  Transaction Information
                </Text>
                <Text style={styles.blue_line} />
              </View>

              <Form>
                {this.renderReviewingAgencyORIField(this.props.UserInput)}

                {reviewing_agency_id !== undefined ? (
                  reviewing_agency_id[0] === "G" &&
                  reviewing_agency_id[1] === "A" ? (
                    <Field
                      label="Reasons *"
                      name="reason"
                      component={SmartInput}
                      type={"reason"}
                      auto_fill={side_bar_switch ? "Insurance Licensees" : null}
                      validate={side_bar_switch ? null : [required, minLength1]}
                    >
                      {this.renderReasons()}
                    </Field>
                  ) : null
                ) : null}

                <Field
                  label="Position applied for"
                  name="positionAppliedFor"
                  component={SmartInput}
                  type={"text"}
                  auto_fill={side_bar_switch ? "Teacher" : null}
                />

                {this.renderPayment(side_bar_switch)}

                {this.state.paymentType === "Agency" &&
                (this.props.selected_reason !==
                  "DECAL- Child Care Provider/Director/Employee. Support Center Employee." &&
                  this.props.selected_reason !=
                    "TCSG - Technical College Student or Employee Providing Direct Child Care") ? (
                  <View>
                    <Field
                      label="Requesting Agency ID"
                      name="requestAgencyID"
                      placeholder="If different from Reviewing Agency ID"
                      component={SmartInput}
                      auto_fill={side_bar_switch ? "GAP234655" : null}
                      validate={
                        this.state.paymentType === "Agency"
                          ? [required, minLength1]
                          : []
                      }
                    />

                    <Field
                      label="Billing Code"
                      name="billingCode"
                      component={SmartInput}
                      type={"text"} // create one more userinput
                      auto_fill={side_bar_switch ? "R1023C1608" : null}
                      placeholderText="(Mandatory if payment is Agency)"
                      validate={required}
                    />

                    <Field
                      label="Billing Password"
                      name="billingPassword"
                      component={SmartInput}
                      type={"text"}
                      auto_fill={side_bar_switch ? "R1023C1608" : null}
                      placeholderText="(Mandatory if payment is Agency)"
                      validate={required}
                    />
                  </View>
                ) : null}
              </Form>

              <View style={styles.checkbox_container}>
                <SmartCheckBox
                  checked={
                    this.state.paymentType === "Agency"
                      ? false
                      : this.state.boxTicked
                  }
                  onPress={this.toggleTick}
                  text={"Fingerprint Card User"}
                />
                {this.state.paymentType === "Agency" ? (
                  <Text
                    style={{
                      textAlign: "center",
                      paddingBottom: 20,
                      color: "#7A7A7A",
                      fontFamily: "Segoeui",
                      fontSize: 14
                    }}
                  >
                    Fingerprint Cards are not available for agency pay.
                  </Text>
                ) : null}
              </View>

              <View style={styles.checkbox_text_container}>
                <Text style={styles.checkBoxText}>
                  By checking this box, you are agreeing to submit ink cards to
                  Gemalto Cogent. see here for details
                </Text>
              </View>

              <View style={styles.submit_container}>
                <View>
                  <SmartButton
                    buttonBorderStyle={{ width: width / 3 }}
                    text={"Reset"}
                    onPress={reset}
                  />
                </View>
                <View>
                  <SmartButton
                    buttonBorderStyle={{ width: width / 3 }}
                    text={"Continue"}
                    onPress={() =>
                      this._handle_submit(handleSubmit(this.submit))
                    }
                  />
                </View>
              </View>
            </KeyboardAwareScrollView>
          ) : (
            <View style={{ flex: 1, height: 300, marginHorizontal: 20 }}>
              {agency_pay_result.success ? (
                handleSubmit(this.submit)
              ) : (
                <View style={styles.modalWrapper}>
                  <Text style={styles.modalHeaderText}>Oops!</Text>
                  <Text style={styles.modalText}>
                    {agency_pay_result.message}
                  </Text>
                  <SmartButton
                    text={"Let's start over.."}
                    onPress={this._reset_agency_pay}
                  />
                </View>
              )}
            </View>
          )
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={this._go_back}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  initialValues: {
    reviewAgencyID: state.userInput.formData.subgroup.ori
      ? state.userInput.formData.subgroup.ori
      : // ^^^ IF SUBGROUP HAS ORI - NO AGENCIES > USE IT -> ^^^
      state.userInput.formData.subgroup.agency.length
      ? // ^^^ ELSE IF
        state.userInput.formData.subgroup.agency.length === 1
        ? state.userInput.formData.subgroup.agency[0].ori
        : // ^^^ IF ONLY ONE ITEM IN AGENCIES ARRAY -> USE IT ^^^
          `` // << ELSE LEAVE IT BLANK
      : `Please enter a valid ORI`,
    // IF NO SUBGROUP ORI NOR AGENCIES -> ENTER AN ORI
    payment: ""
  },
  reason: state.userInput.reasons,
  departments: state.userInput.datas,
  departmentIndex: state.userInput.departmentIndex,
  UserInput: state.userInput.formData,
  checkBox: state.userInput.checkBox,

  grp_id: state.userInput.grp_id,
  subgrp_id: state.userInput.subgrp_id,
  ori: state.userInput.ori,
  side_bar_switch: state.dev_reducer.options.side_bar_switch,
  agency_pay_result: state.userInput.agency_pay_result
});
//
// connect the redux form
// s1: a unique identifier for this form
// bootstrap navigation
//
const notConnected = reduxForm({ form: "s1" })(withNavigation(FormScreen_1));
const selector = formValueSelector("s1");

const connected = connect(state => {
  const payment = selector(state, "payment");
  const reviewing_agency_id = selector(state, "reviewAgencyID");
  const selected_reason = selector(state, "reason");
  const requesting_agency_id = selector(state, "requestAgencyID");
  const billing_code = selector(state, "billingCode");
  const billing_password = selector(state, "billingPassword");

  return {
    reviewing_agency_id,
    requesting_agency_id,
    selected_reason,
    payment
  };
})(notConnected);

// map the redux' state to the components props
export default connect(mapStateToProps)(connected);
