import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Dimensions, Modal } from "react-native";
import { Button, Text, Form } from "native-base";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import Image from "react-native-remote-svg";
import { withNavigation } from "react-navigation";
import { fetchDemographics } from "../../redux/actions/actions";

import { Field, reduxForm, formValueSelector } from "redux-form";
import SmartInput from "../../components/SmartInput";
import SmartButton from "../../components/SmartButton";
import BodyWrapper from "../../components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import Storage from "../../components/Storage";

import styles from "../../assets/styles/forms";

const width = Dimensions.get("window").width;

import {
  required,
  maxLength30,
  maxLength25,
  maxLength9,
  maxLength8,
  maxLength3,
  number,
  SSNMatch,
  birthDate,
  alphaNumeric,
  alphaNumericPlusHyphen
} from "../../validation/validations";
import BackButton from "../../components/BackButton";

class FormScreenTwo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boxTicked: false,
      pob: [],
      isModalVisible: false
    };
  }

  toggleTick = () => {
    this.setState({ boxTicked: !this.state.boxTicked });
  };

  submit = () => {
    if (this.props.SSN === undefined) {
      this.toggleModal();
    } else {
      this.props.navigation.navigate("FormScreen_3");
    }
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  _go_back = () => {
    this.props.navigation.goBack();
  };

  renderModal = () => {
    return (
      <Modal
        backdropColor={"rgba(255, 255, 255, 1)"}
        isVisible={this.state.isModalVisible}
      >
        <View style={{ flex: 1, justifyContent: "center", padding: 20 }}>
          <Text style={styles.modal_headline}>Notice</Text>
          <Text style={styles.modal_text}>
            You did not enter a SSN, therefore you must take your registration
            receipt with you to the fingerprint site and use the Registration ID
            to be fingerprinted.
          </Text>

          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <View style={{ padding: 20 }}>
              <SmartButton
                buttonBorderStyle={styles.modal_button_width}
                buttonTextStyle={styles.modal_button_color}
                text={"Go Back"}
                onPress={() => {
                  this.toggleModal();
                }}
              />
            </View>
            <View style={{ padding: 20 }}>
              <SmartButton
                buttonBorderStyle={{ width: width / 3 }}
                buttonTextStyle={{ color: "white" }}
                text={"Next"}
                onPress={() => {
                  this.toggleModal();
                  this.props.navigation.navigate("FormScreen_3");
                }}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    const {
      demographics,
      handleSubmit,
      reset,
      first_name,
      last_name,
      dob,
      state_licence_number,
      side_bar_switch
    } = this.props;

    return !this.state.isModalVisible ? (
      <BodyWrapper
        {...this.props}
        body={
          demographics ? (
            <KeyboardAwareScrollView>
              <DepartmentHeader />

              <Storage
                last_name={last_name}
                first_name={first_name}
                dob={dob}
              />

              <View>
                <Text style={styles.newRegistrationStyle}>
                  New Registration: Please Enter Your Information
                </Text>
              </View>

              <View style={styles.form_progress_container}>
                <Image
                  style={styles.progressBar}
                  source={require("../../assets/forms/form_progress_2.svg")}
                />
              </View>

              <View style={styles.sectionStyle}>
                <Text style={styles.sectionTextStyle}>
                  Personal Information
                </Text>
                <Text style={styles.blue_line} />
              </View>

              <Form>
                <Field
                  label="First Name *"
                  name="firstName"
                  component={SmartInput}
                  type={"text"}
                  auto_fill={side_bar_switch ? "TEST" : null}
                  validate={
                    side_bar_switch
                      ? null
                      : [required, maxLength30, alphaNumeric]
                  }
                  length={30}
                />

                <Field
                  label="Middle Name"
                  name="middleName"
                  component={SmartInput}
                  type={"text"}
                  validate={
                    side_bar_switch ? null : [maxLength30, alphaNumeric]
                  }
                  length={30}
                />

                <Field
                  label="Last Name *"
                  name="lastName"
                  component={SmartInput}
                  type={"text"}
                  auto_fill={side_bar_switch ? "TEST" : null}
                  validate={
                    side_bar_switch
                      ? null
                      : [required, maxLength30, alphaNumericPlusHyphen]
                  }
                  length={30}
                />

                <Field
                  label="Maiden Name"
                  name="maiden_name"
                  component={SmartInput}
                  type={"text"}
                />

                <Field
                  label="Suffix"
                  name="suffix"
                  component={SmartInput}
                  type={"suffix"}
                />

                <Field
                  label="Social Security Number"
                  name="SSN"
                  component={SmartInput}
                  type={"text"}
                  auto_fill={side_bar_switch ? null : null}
                  // auto_fill={side_bar_switch ? "550051010" : null}
                  validate={[maxLength9, number]}
                />

                <Field
                  label="Re-enter Social Security Number"
                  name="confirmSSN"
                  component={SmartInput}
                  type={"text"}
                  auto_fill={side_bar_switch ? null : null}
                  // auto_fill={side_bar_switch ? "550051010" : null}
                  validate={[maxLength9, number, SSNMatch]}
                />

                <Field
                  label="Date of Birth in MMDDYYYY format *"
                  warning={"Need to be in format MMDDYYYY"}
                  name="birthDate"
                  component={SmartInput}
                  type={"text"}
                  auto_fill={side_bar_switch ? "01011990" : null}
                  validate={[required, birthDate, maxLength8]}
                />

                <Field
                  label="Place of Birth *"
                  name="pob"
                  component={SmartInput}
                  type={"demographics"}
                  auto_fill={side_bar_switch ? "UNITED STATES" : null}
                  validate={side_bar_switch ? null : [required]}
                />

                <Field
                  label="Sex *"
                  name="sex"
                  component={SmartInput}
                  type={"demographics"}
                  auto_fill={side_bar_switch ? "Male" : null}
                  validate={side_bar_switch ? null : [required]}
                />

                <Field
                  label="Race *"
                  name="race"
                  component={SmartInput}
                  type={"demographics"}
                  auto_fill={side_bar_switch ? "Black" : null}
                  validate={side_bar_switch ? null : [required]}
                />

                <Field
                  label="Eye Color *"
                  name="eye_color"
                  type={"demographics"}
                  component={SmartInput}
                  auto_fill={side_bar_switch ? "Brown" : null}
                  validate={side_bar_switch ? null : [required]}
                />

                <Field
                  label="Hair Color *"
                  name="hair_color"
                  type={"demographics"}
                  component={SmartInput}
                  auto_fill={side_bar_switch ? "Black" : null}
                  validate={side_bar_switch ? null : [required]}
                />

                <Field
                  label="Height *"
                  name="height"
                  component={SmartInput}
                  type={"height"}
                  auto_fill={side_bar_switch ? "6'02" : null}
                  validate={side_bar_switch ? null : [required]}
                />

                <Field
                  label="Weight *"
                  name="weight"
                  component={SmartInput}
                  type={"text"}
                  auto_fill={side_bar_switch ? "165" : null}
                  validate={side_bar_switch ? null : [required, maxLength3]}
                  length={3}
                />

                <Field
                  type={"demographics"}
                  label="Country of Citizenship"
                  name="citizen"
                  component={SmartInput}
                  auto_fill={side_bar_switch ? "UNITED STATES" : null}
                  //validate={side_bar_switch ? null : [required]}
                />

                <Field
                  label="Driver License State"
                  name="stateLicenseNumber"
                  component={SmartInput}
                  type={"demographics"}
                  auto_fill={side_bar_switch ? "CALIFORNIA" : null}
                  // validate={side_bar_switch ? null : []}
                />

                <Field
                  label="Driver License Number"
                  name="driverLicenseNumber"
                  component={SmartInput}
                  type={"text"}
                  placeholder="Don't include 'GA'"
                  auto_fill={side_bar_switch ? "0129092109" : null}
                  validate={
                    side_bar_switch
                      ? null
                      : !state_licence_number === undefined
                      ? [required, maxLength25]
                      : null
                  }
                  length={25}
                />
              </Form>
              <View style={styles.submit_container}>
                <View>
                  <SmartButton
                    buttonBorderStyle={{ width: width / 3 }}
                    text={"Reset"}
                    onPress={reset}
                  />
                </View>
                <View>
                  <SmartButton
                    buttonBorderStyle={{ width: width / 3 }}
                    text={"Continue"}
                    onPress={handleSubmit(this.submit)}
                  />
                </View>
              </View>
            </KeyboardAwareScrollView>
          ) : (
            <View style={styles.activity_indicator_container}>
              {/* {this.props.dispatch(fetchDemographics())} */}
              <ActivityIndicator size="large" color="#E06C08" />
            </View>
          )
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={this._go_back}>
            <BackButton />
          </Button>
        }
      />
    ) : (
      <View style={styles.modal_padding}>{this.renderModal()}</View>
    );
  }
}

const notConnected = reduxForm({ form: "s2" })(withNavigation(FormScreenTwo));
const selector = formValueSelector("s2");

const connected = connect(state => {
  const SSN = selector(state, "SSN");
  const first_name = selector(state, "firstName");
  const last_name = selector(state, "lastName");
  const dob = selector(state, "birthDate");
  const state_licence_number = selector(state, "stateLicenseNumber");
  return { SSN, first_name, last_name, dob, state_licence_number };
})(notConnected);

const mapStateToProps = state => ({
  UserInput: state.userInput.formData,
  checkBox: state.userInput.checkBox,
  demographics: state.demographics.data,
  side_bar_switch: state.dev_reducer.options.side_bar_switch
});

export default connect(mapStateToProps)(connected);
