import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  ActivityIndicator
} from "react-native";
import { Button, Text } from "native-base";
import Image from "react-native-remote-svg";

import BodyWrapper from "../../components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";

import BackButton from "../../components/BackButton";
import HomeButton from "../../components/HomeButton";
import { company_email, company_phone_number } from "../../assets/utils";
const height = Dimensions.get("window").height;
class SuccessPage extends Component {
  render() {
    const { navigation, new_reg_data } = this.props;
    return (
      <BodyWrapper
        {...this.props}
        body={
          new_reg_data ? (
            <ScrollView>
              <DepartmentHeader />
              {new_reg_data === 1 ? (
                <View>
                  <View style={style.titleStyle}>
                    <Text style={style.textTitleStyle}>
                      New Applicant Registration Success
                    </Text>
                  </View>

                  <View style={style.imageWrapper}>
                    <Image
                      style={style.imageStyle}
                      source={require("../../assets/forms/success.svg")}
                    />
                  </View>

                  <View style={style.textWrapper}>
                    <Text style={style.textStyle}>
                      Your registration has been saved. Please check your
                      confirmation email and proceed with further instruction
                      included in the email.
                      {/* Your registration has been saved. Please check your input
                      email box to activate your email and continue the
                      registration process. */}
                    </Text>
                  </View>
                </View>
              ) : (
                <View>
                  <View style={style.titleStyle}>
                    <Text style={style.textTitleStyle}>
                      New Applicant Registration Failed
                    </Text>
                  </View>

                  <View style={style.imageWrapper}>
                    <Image
                      style={style.imageStyle}
                      source={require("../../assets/forms/failed.svg")}
                    />
                  </View>

                  <View style={style.textWrapper}>
                    <Text style={style.textStyle}>
                      Your registration failed. Please contact {company_email}{" "}
                      or call customer service at {company_phone_number}.
                    </Text>
                  </View>
                </View>
              )}
            </ScrollView>
          ) : (
            <View style={style.ternaryRender2}>
              <ActivityIndicator size="large" color="#E06C08" />
            </View>
          )
        }
        footerPanels={2}
        footer={
          <Button transparent onPress={() => navigation.goBack()}>
            <BackButton />
          </Button>
        }
        footer_2={
          <Button transparent onPress={() => navigation.navigate("Home")}>
            <HomeButton />
          </Button>
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  new_reg_data: state.userInput.new_reg_data
});

export default connect(mapStateToProps)(SuccessPage);

const style = StyleSheet.create({
  titleStyle: {
    padding: 20,
    justifyContent: "center",
    flex: 2,
    alignItems: "center"
  },
  textTitleStyle: {
    fontFamily: "Roboto-Medium",
    fontSize: 15,
    textAlign: "center",
    color: "#1F1F1D"
  },
  imageWrapper: {
    justifyContent: "center",
    alignItems: "center",
    padding: 20
  },
  imageStyle: {
    alignSelf: "center",
    width: 100,
    height: 100
  },
  textWrapper: {
    justifyContent: "center",
    alignItems: "center",
    padding: 20
  },
  textStyle: {
    textAlign: "center",
    fontFamily: "Segoeui",
    fontSize: 15,
    paddingBottom: 30
  },
  ternaryRender2: {
    height: height,
    justifyContent: "center",
    alignItems: "center"
  }
});
