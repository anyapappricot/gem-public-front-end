import React, { Component } from "react";
import { View, Dimensions } from "react-native";
import { Button, Text, Form } from "native-base";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import Image from "react-native-remote-svg";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";

import { Field, reduxForm } from "redux-form";

import SmartInput from "../../components/SmartInput";
import SmartButton from "../../components/SmartButton";

import { saveCheckBoxSameAddress } from "../../redux/actions/actions";
import styles from "../../assets/styles/forms";

const width = Dimensions.get("window").width;

import {
  required,
  minLength1,
  maxLength100,
  maxLength30,
  maxLength5,
  number
} from "../../validation/validations";

import BodyWrapper from "../../components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import BackButton from "../../components/BackButton";
import SmartCheckBox from "../../components/SmartCheckBox";

class FormScreenFour extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boxTicked: false,
      address: this.props.form.address,
      city: this.props.form.city,
      apt: this.props.form.apt,
      zipcode: this.props.form.zipcode,
      state: this.props.form.state
    };
  }

  toggleTick = async () => {
    await this.setState({
      boxTicked: !this.state.boxTicked
    });

    await this.props.dispatch(
      saveCheckBoxSameAddress(this.state.boxTicked ? true : false)
    );
  };

  componentWillUnmount = () => {};

  submit = () => {
    this.props.navigation.navigate("VerificationScreen");
  };

  _go_back = () => {
    this.props.navigation.goBack();
  };

  render() {
    const { handleSubmit, reset, side_bar_switch } = this.props;
    return (
      <BodyWrapper
        {...this.props}
        body={
          <KeyboardAwareScrollView>
            <DepartmentHeader />

            <View>
              <Text style={styles.newRegistrationStyle}>
                New Registration: Please Enter Your Information
              </Text>
            </View>

            <View style={styles.form_progress_container}>
              <Image
                style={styles.progressBar}
                source={require("../../assets/forms/form_progress_4.svg")}
              />
            </View>

            <View style={styles.column_centered}>
              <View style={styles.mail_address_container}>
                <View style={styles.mail_address_headline}>
                  <Text style={styles.sectionTextStyle}>
                    Mail Address Information
                  </Text>
                </View>

                <View style={styles.mail_address_cta}>
                  <SmartCheckBox
                    checked={this.state.boxTicked}
                    onPress={this.toggleTick}
                    text={"Use same address"}
                  />
                </View>
              </View>
              <Text style={styles.blue_line} />
            </View>

            {this.state.boxTicked ? (
              <Form>
                <Field
                  label="Address *"
                  name="address"
                  type={"text"}
                  component={SmartInput}
                  validate={[required, minLength1, maxLength100]}
                  disabledField={{ disabled: true }}
                />

                <Field
                  label="Apt Number"
                  name="apt"
                  type={"text"}
                  component={SmartInput}
                  validate={[maxLength5]}
                  disabledField={{ disabled: true }}
                />
                <Field
                  label="City *"
                  name="city"
                  type={"text"}
                  component={SmartInput}
                  validate={[required, minLength1, maxLength30]}
                  disabledField={{ disabled: true }}
                />
                <Field
                  label="State *"
                  name="state"
                  type={"text"}
                  component={SmartInput}
                  validate={[required]}
                  disabledField={{ disabled: true }}
                />
                <Field
                  label="ZIP code *"
                  name="zipcode"
                  type={"text"}
                  component={SmartInput}
                  validate={[required, maxLength5]}
                  disabledField={{ disabled: true }}
                />
              </Form>
            ) : (
              <Form>
                <Field
                  label="Address *"
                  name="address2"
                  type={"text"}
                  component={SmartInput}
                  auto_fill={side_bar_switch ? "5050 Cherry Ave" : null}
                  validate={
                    side_bar_switch
                      ? null
                      : [required, minLength1, maxLength100]
                  }
                />
                <Field
                  label="Apt Number"
                  name="apt2"
                  type={"text"}
                  component={SmartInput}
                  validate={[maxLength5]}
                  auto_fill={side_bar_switch ? "17" : null}
                  validate={side_bar_switch ? null : [maxLength5]}
                />
                <Field
                  label="City *"
                  name="city2"
                  type={"text"}
                  component={SmartInput}
                  auto_fill={side_bar_switch ? "Long Beach" : null}
                  validate={
                    side_bar_switch ? null : [required, minLength1, maxLength30]
                  }
                />
                <Field
                  label="State *"
                  name="state2"
                  type={"demographics"}
                  component={SmartInput}
                  auto_fill={side_bar_switch ? "CALIFORNIA" : null}
                  validate={side_bar_switch ? null : [required]}
                />
                <Field
                  label="ZIP code *"
                  name="zipcode2"
                  type={"text"}
                  component={SmartInput}
                  auto_fill={side_bar_switch ? "90802" : null}
                  validate={
                    side_bar_switch ? null : [required, number, maxLength5]
                  }
                />
              </Form>
            )}

            <View style={styles.submit_container}>
              <View>
                <SmartButton
                  buttonBorderStyle={{ width: width / 3 }}
                  text={"Reset"}
                  onPress={reset}
                />
              </View>
              <View>
                <SmartButton
                  buttonBorderStyle={{ width: width / 3 }}
                  text={"Continue"}
                  onPress={handleSubmit(this.submit)}
                />
              </View>
            </View>
          </KeyboardAwareScrollView>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={this._go_back}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  initialValues: {
    address: state.form.s3.values.address,
    city: state.form.s3.values.city,
    apt: state.form.s3.values.apt,
    zipcode: state.form.s3.values.zipcode,
    state: state.form.s3.values.state
  },
  side_bar_switch: state.dev_reducer.options.side_bar_switch
});

export default connect(mapStateToProps)(
  reduxForm({ form: "s4" })(withNavigation(FormScreenFour))
);
