import React, { Component } from "react";
import { View, StyleSheet, Platform, Dimensions } from "react-native";
import { Content, Button, Text, Item } from "native-base";
import Image from "react-native-remote-svg";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";
import { postNewApplicant } from "../../redux/actions/actions";
import BodyWrapper from "../../components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import SmartButton from "../../components/SmartButton";
import Storage from "../../components/Storage";

import { getRegistrationID } from "../../redux/actions/actions";
import publicIP from "react-native-public-ip";
import BackButton from "../../components/BackButton";
import { _U } from "../../assets/utils";
const width = Dimensions.get("window").width;

const randomiZer = _ => Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000;

class VerificationPageNotConnected extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      remote_addr: null
    };
  }

  componentDidMount() {
    this.props.dispatch(getRegistrationID());
    publicIP()
      .then(ip => {
        this.setState({
          remote_addr: ip
        });
      })
      .catch(error => {
        console.warn(error);
        // 'Unable to get IP address.'
      });
  }

  render() {
    const data2Send = {
      reg_id: _U((this.props.reg_id += randomiZer())),
      last_name: _U(this.props.lastName),
      first_name: _U(this.props.firstName),
      middle_name: _U(this.props.middleName),
      suffix_name: _U(this.props.suffix),
      dob: this.props.birthDate,
      sex: _U(this.props.sex),
      ssn: _U(this.props.ssn),
      rac: this.props.race,
      ctz: this.props.citizen, // DOUBLE CHECK
      pob: this.props.pob,
      eye: this.props.eye_color,
      hai: this.props.hair_color,
      hgt: this.props.height,
      wgt: this.props.weight,
      app_street: _U(this.props.address),
      app_street2: this.props.apt,
      app_city: _U(this.props.city),
      app_state: this.props.state,
      app_zip: _U(this.props.zip),
      app_phone: _U(this.props.phoneNumber),
      app_email: _U(this.props.app_email),
      trans_type: this.props.reason,
      ori: "GAP234655",
      oca: "R1023C1608",
      pay_flag: this.props.payment,
      dl: null,
      dl_state: null,
      pay_status: 0,
      second_ori: _U(this.props.reviewAgencyID),
      remote_addr: "136.229.24.12",
      reg_user: "LS008",
      card_user: this.props.checkBox,
      position_applied: _U(this.props.positionAppliedFor),
      mail_street: _U(this.props.address2),
      mail_apt: _U(this.props.apt2),
      mail_city: _U(this.props.city2),
      mail_state: _U(this.props.state2),
      mail_zip: _U(this.props.zipcode2),
      grp_id: "CS",
      subgrp_id: "GASC",
      maiden_name: _U(this.props.maiden_name)
    };

    /*
    // NOTES TO SELF
     requesting is ori
     reviewing agency is second_ori
     reg_id: state.userInput.reg_id,
    */

    // console.warn(this.props.remote_addr); // ISSUES
    // console.warn(`checkBox`, this.props.checkBox);
    // console.warn(_U((this.props.reg_id += randomiZer())));
    // console.warn(`remote_addr`, this.props.remote_addr);

    // console.warn(`reviewAgencyID`, this.props.reviewAgencyID);
    // console.warn(`grp_id`, this.props.grp_id);
    // console.warn(`subgrp_id`, this.props.subgrp_id);

    // TODO: FIGURE OUT HOW TO GET THE GRP_ID
    const { reg_id, form } = this.props;

    //  console.warn(JSON.stringify(data2Send, null, 2));
    console.warn(this.props.city2);
    console.warn(this.props.city2.length);
    return this.props.reg_id ? (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />
            <Storage reg_id={reg_id} />

            {/* Transaction Information */}

            <View
              style={{
                alignSelf: "center",
                paddingBottom: 20
              }}
            >
              <View style={styles.sectionStyle}>
                <Text style={styles.sectionTextStyle}>
                  Transaction Information
                </Text>
                <Text style={styles.blue_line} />
              </View>

              <View style={[styles.subSection]}>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Reviewing Agency ID</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.reviewAgencyID}
                    </Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Reason</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.reason}
                    </Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Requesting Agency ID</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.requestAgencyID
                        ? this.props.requestAgencyID
                        : "n/a"}
                    </Text>
                  </Item>

                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Position apllied for</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.positionAppliedFor}
                    </Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Payment</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.payment}
                    </Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>
                      Electronic Fingerprinting
                    </Text>
                    <Text style={styles.receivedInput}>
                      {this.props.checkBox}
                    </Text>
                  </Item>
                </View>
              </View>
            </View>

            {/* Personal Information */}

            <View
              style={{
                alignSelf: "center",
                // top: 10,
                paddingBottom: 20
              }}
            >
              <View style={styles.sectionStyle}>
                <Text style={styles.sectionTextStyle}>
                  Personal Information
                </Text>
                <Text style={styles.blue_line} />
              </View>

              <View style={[styles.subSection]}>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Last Name</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.lastName}
                    </Text>
                  </Item>

                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>First Name</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.firstName}
                    </Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Middle Name</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.middleName ? this.props.middleName : "n/a"}
                    </Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>
                      Social Security Number
                    </Text>
                    <Text style={styles.receivedInput}>{this.props.ssn}</Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Date of Birth</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.birthDate}
                    </Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Place of Birth</Text>
                    <Text style={styles.receivedInput}>{this.props.pob}</Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Sex</Text>
                    <Text style={styles.receivedInput}>{this.props.sex}</Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Race</Text>
                    <Text style={styles.receivedInput}>{this.props.race}</Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Eye Color</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.eye_color}
                    </Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Hair Color</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.hair_color}
                    </Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Height</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.height}
                    </Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Weight</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.weight}
                    </Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>
                      Country of citizenship
                    </Text>
                    <Text style={styles.receivedInput}>{this.props.pob}</Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>
                      State Driver's License
                    </Text>
                    <Text style={styles.receivedInput}>
                      {this.props.stateLicenseNumber}
                    </Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>
                      Driver's License Number
                    </Text>
                    <Text style={styles.receivedInput}>
                      {this.props.driverLicenseNumber}
                    </Text>
                  </Item>
                </View>
              </View>
            </View>
            {/* Address Information */}
            <View
              style={{
                alignSelf: "center",
                paddingBottom: 20
              }}
            >
              <View style={styles.sectionStyle}>
                <Text style={styles.sectionTextStyle}>Address Information</Text>
                <Text style={styles.blue_line} />
              </View>

              <View style={styles.subSection}>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Address</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.address}
                    </Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>City</Text>
                    <Text style={styles.receivedInput}>{this.props.city}</Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>State</Text>
                    <Text style={styles.receivedInput}>{this.props.state}</Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Apt</Text>
                    <Text style={styles.receivedInput}>{this.props.apt}</Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Zip Code</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.zipcode}
                    </Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Phone Number</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.phoneNumber}
                    </Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Email</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.app_email}
                    </Text>
                  </Item>
                </View>
              </View>
            </View>
            {/* Mail Address Information */}
            <View
              style={{
                alignSelf: "center",
                paddingBottom: 20
              }}
            >
              <View style={styles.sectionStyle}>
                <Text style={styles.sectionTextStyle}>
                  Mail Address Information
                </Text>
                <Text style={styles.blue_line} />
              </View>

              <View style={styles.subSection}>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Address</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.address2}
                    </Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>City</Text>
                    <Text style={styles.receivedInput}>{this.props.city2}</Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>State</Text>
                    <Text style={styles.receivedInput}>
                      {this.props.state2}
                    </Text>
                  </Item>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Apt</Text>
                    <Text style={styles.receivedInput}>{this.props.apt2}</Text>
                  </Item>
                </View>
                <View style={styles.verificationCard}>
                  <Item stackedLabel style={styles.stackedLabelStyle}>
                    <Text style={styles.labelStyle}>Zip Code</Text>
                    <Text style={{ top: 10 }}>{this.props.zipcode2}</Text>
                  </Item>
                </View>
              </View>

              <View
                style={{
                  marginBottom: 30,
                  // marginTop: 50,
                  alignItems: "center",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              >
                <SmartButton
                  buttonBorderStyle={{ width: width / 3 }}
                  text={"Submit"}
                  onPress={() => {
                    // console.warn(JSON.stringify(data2Send, null, 2));
                    this.props.dispatch(postNewApplicant(data2Send));
                    this.props.navigation.navigate("SuccessScreen");

                    // if (this.props.new_reg_data) {
                    //   this.props.navigation.navigate("SuccessScreen");
                    // }
                  }}
                />
              </View>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    ) : (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <Text>NO REG ID</Text>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("HomeScreen")}
          >
            <BackButton />
          </Button>
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  reviewAgencyID: state.form.s1.values.reviewAgencyID,
  reason: state.form.s1.values.reason,
  requestAgencyID: state.form.s1.values.requestAgencyID,
  positionAppliedFor: state.form.s1.values.positionAppliedFor,
  payment: state.form.s1.values.payment,

  lastName: state.form.s2.values.lastName,
  maiden_name: state.form.s2.values.maiden_name,
  firstName: state.form.s2.values.firstName,
  middleName: state.form.s2.values.middleName,
  suffix: state.form.s2.values.suffix,
  ssn: state.form.s2.values.SSN,
  confirmSSN: state.form.s2.values.confirmSSN,
  birthDate: state.form.s2.values.birthDate,
  pob: state.form.s2.values.pob,
  sex: state.form.s2.values.sex,
  race: state.form.s2.values.race,
  eye_color: state.form.s2.values.eye_color,
  hair_color: state.form.s2.values.hair_color,
  height: state.form.s2.values.height,
  weight: state.form.s2.values.weight,
  citizen: state.form.s2.values.citizen,
  stateLicenseNumber: state.form.s2.values.stateLicenseNumber,
  driverLicenseNumber: state.form.s2.values.driverLicenseNumber,

  phoneNumber: state.form.s3.values.phoneNumber,
  app_email: state.form.s3.values.app_email,

  address: state.form.s3.values.address,
  city: state.form.s3.values.city,
  apt: state.form.s3.values.apt,
  zipcode: state.form.s3.values.zipcode,
  state: state.form.s3.values.state,

  address2: state.form.s4.values.address2 || state.form.s3.values.address,
  city2: state.form.s4.values.city2 || state.form.s3.values.city,
  apt2: state.form.s4.values.apt2 || state.form.s3.values.apt,
  zipcode2: state.form.s4.values.zipcode2 || state.form.s3.values.zipcode,
  state2: state.form.s4.values.state2 || state.form.s3.values.state,

  // address2: state.form.s4.values.address2
  //   ? state.form.s4.values.address2
  //   : state.form.s3.values.address,
  // city2: state.form.s4.values.city2
  //   ? state.form.s4.values.city2
  //   : state.form.s3.values.city,
  // apt2: state.form.s4.values.apt2
  //   ? state.form.s4.values.apt2
  //   : state.form.s3.values.apt,
  // zipcode2: state.form.s4.values.zipcode2
  //   ? state.form.s4.values.zipcode2
  //   : state.form.s3.values.zipcode,
  // state2: state.form.s4.values.state2
  //   ? state.form.s4.values.state2
  //   : state.form.s3.values.state,

  // address2: state.form.s4.values.address2,
  // city2: state.form.s4.values.city2,
  // apt2: state.form.s4.values.apt2,
  // zipcode2: state.form.s4.values.zipcode2,
  // state2: state.form.s4.values.state2,

  UserInput: state.userInput.formData,

  grp_id: state.userInput.grp_id,
  subgrp_id: state.userInput.subgrp_id,
  ori: state.userInput.ori,
  checkBox: state.userInput.checkBox,

  // BRENDAN WAS HERE
  reg_id: state.userInput.reg_id,
  demographics: state.demographics.data,
  new_reg_data: state.userInput.new_reg_data,
  form: {
    ...state.form.s1.values,
    ...state.form.s2.values,
    ...state.form.s3.values,
    ...state.form.s4.values
  }
});

const VerificationPage = connect(mapStateToProps)(VerificationPageNotConnected);

export default withNavigation(VerificationPage);

const styles = StyleSheet.create({
  verificationCard:
    width > 10000000 // USED TO 400
      ? {
          flexDirection: "column",
          height: 150,
          justifyContent: "center",
          alignItems: "center"
        }
      : {
          flexDirection: "row"
        },
  subSection: {
    padding: 20
  },
  blue_line: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: "#01366C",
    height: 0.5,
    width: width - 40
  },
  sectionStyle: {
    paddingTop: 0,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  sectionTextStyle: {
    fontSize: 15,
    fontFamily: "Segoeui",
    color: "#01366C"
  },
  stackedLabelStyle: {
    backgroundColor: "rgba(0,0,0, .00000025)",
    justifyContent: "center",
    alignItems: "flex-start",
    borderColor: "transparent",
    paddingBottom: 10,
    flex: 0.5,
    marginRight: 20,
    marginLeft: 20
  },
  receivedInput: {
    top: 10,
    fontFamily: "Segoeui-Semibold"
  },
  labelStyle: {
    color: "#1F1F1D",
    top: 10,
    fontFamily: "Segoeui"
  }
});
