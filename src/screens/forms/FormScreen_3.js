import React from "react";
import { View, Dimensions } from "react-native";
import { Button, Text, Form } from "native-base";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import Image from "react-native-remote-svg";
import { withNavigation } from "react-navigation";
import ValidationComponent from "react-native-form-validator";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import SmartInput from "../../components/SmartInput";
import SmartButton from "../../components/SmartButton";
import styles from "../../assets/styles/forms";

const width = Dimensions.get("window").width;

import {
  required,
  minLength1,
  maxLength100,
  maxLength50,
  maxLength30,
  maxLength5,
  number,
  phoneNumber,
  email
} from "../../validation/validations";

import BodyWrapper from "../../components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import BackButton from "../../components/BackButton";

class FormScreenThree extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      address: ""
    };
    this.submit = this.submit.bind(this);
  }

  submit = () => {
    this.props.navigation.navigate("FormScreen_4");
  };

  render() {
    const { side_bar_switch, handleSubmit, reset } = this.props;
    return (
      <BodyWrapper
        {...this.props}
        body={
          <KeyboardAwareScrollView>
            {/* <KeyboardAvoidingView> */}
            <DepartmentHeader />
            <View>
              <Text style={styles.newRegistrationStyle}>
                New Registration: Please Enter Your Information
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                padding: 20
              }}
            >
              <Image
                style={styles.progressBar}
                source={require("../../assets/forms/form_progress_3.svg")}
              />
            </View>

            <View style={styles.sectionStyle}>
              <Text style={styles.sectionTextStyle}>Address Information</Text>
              <Text style={styles.blue_line} />
            </View>

            <Form>
              <Field
                label="Address *"
                name="address"
                type={"text"}
                component={SmartInput}
                auto_fill={side_bar_switch ? "182 Elm" : null}
                validate={
                  side_bar_switch ? null : [required, minLength1, maxLength100]
                }
                length={100}
              />
              <Field
                label="City *"
                name="city"
                type={"text"}
                component={SmartInput}
                auto_fill={side_bar_switch ? "Los Angeles" : null}
                validate={
                  side_bar_switch ? null : [required, minLength1, maxLength30]
                }
                length={30}
              />
              <Field
                label="Apt Number"
                name="apt"
                type={"text"}
                component={SmartInput}
                auto_fill={side_bar_switch ? "5" : null}
                validate={side_bar_switch ? null : [maxLength5]}
                length={5}
              />
              <Field
                label="ZIP code *"
                name="zipcode"
                type={"text"}
                component={SmartInput}
                auto_fill={side_bar_switch ? "90210" : null}
                validate={
                  side_bar_switch ? null : [required, number, maxLength5]
                }
                length={5}
              />
              <Field
                label="State *"
                type={"demographics"}
                name="state"
                component={SmartInput}
                auto_fill={side_bar_switch ? "CALIFORNIA" : null}
                validate={side_bar_switch ? null : [required]}
              />
              <Field
                label="Phone Number *"
                name="phoneNumber"
                type={"text"}
                component={SmartInput}
                auto_fill={side_bar_switch ? "2816853143" : null}
                validate={side_bar_switch ? null : [required, phoneNumber]}
              />
              <Field
                label="Email *"
                name="app_email"
                type={"text"}
                component={SmartInput}
                auto_fill={side_bar_switch ? "threadpool.io@gmail.com" : null}
                validate={
                  side_bar_switch ? null : [required, email, maxLength50]
                }
                length={50}
              />
            </Form>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                marginTop: 25,
                marginBottom: 50
              }}
            >
              <View>
                <SmartButton
                  buttonBorderStyle={{ width: width / 3 }}
                  text={"Reset"}
                  onPress={reset}
                />
              </View>
              <View>
                <SmartButton
                  buttonBorderStyle={{ width: width / 3 }}
                  text={"Continue"}
                  onPress={handleSubmit(this.submit)}
                />
              </View>
            </View>
            {/* </KeyboardAvoidingView> */}
          </KeyboardAwareScrollView>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  UserInput: state.userInput.formData,
  side_bar_switch: state.dev_reducer.options.side_bar_switch
});

export default connect(mapStateToProps)(
  reduxForm({ form: "s3" })(withNavigation(FormScreenThree))
);
