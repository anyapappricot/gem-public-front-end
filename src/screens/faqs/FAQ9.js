import React, { Component } from "react";
import { StyleSheet, Platform, View } from "react-native";
import Image from "react-native-remote-svg";
import { Container, Button, Text, Right, Content } from "native-base";
import { withNavigation } from "react-navigation";
import ScreenHeader from "../../components/ScreenHeader";
import BodyWrapper from "../../components/BodyWrapper";
import BackButton from "../../components/BackButton";

class FAQ9 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={{ marginHorizontal: 20, top: 20 }}>
              <Text style={styles.textStyle}>
                A 2005 amendment to Georgia law authorizes the exchange of
                national criminal history record information on providers of
                care to children, the elderly and persons with disabilities,
                including but not limited to, volunteers with youth sports
                organizations and other youth activities.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                This amendment authorizes and facilitates, but does not require,
                the exchange of national criminal history record information
                with authorized agencies on behalf of qualified entities as
                authorized under federal law. An “authorized agency” means any
                local government agency designated to report, receive or
                disseminate information under the NCPA or VCA. A “qualified
                entity” means a business or organization whether public,
                private, for profit, not for profit, or volunteer that provides
                care or care placement services, including a business or
                organization that licenses or certifies others to provide care
                or care placement services.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                An authorized agency is responsible for designating qualified
                entities within its local jurisdiction and submitting national
                criminal history record checks as authorized under the NCPA and
                VCA. An authorized agency, other than a criminal justice agency
                as defined in O.C.G.A. § 35-3-30, must request an ORI from the
                FBI for the express purpose of submitting national criminal
                history record checks under this code section. Requests for an
                ORI must be in writing to the FBI through GCIC.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                Requests for national criminal history record checks must be
                fingerprint based and submitted to the GCIC for a state and FBI
                records check.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                A complete state-only criminal history response may be provided
                directly to the qualified entity; however, federal law and
                regulations DO NOT permit the qualified entity access to the
                national criminal history record. Therefore, the authorized
                agency is responsible for reviewing the national criminal
                history record to make a fitness determination of the applicant
                or volunteer based on the national record. Only the final
                fitness determination decision, and not the national criminal
                history record, may be provided to the qualified entity.
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

export default withNavigation(FAQ9);

const styles = StyleSheet.create({
  FAQ: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    color: "#C4C4C4"
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  },
  textStyle: {
    fontFamily: "OpenSans-Light"
  }
});
