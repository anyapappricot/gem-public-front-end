import React, { Component } from "react";
import { StyleSheet, Platform, View } from "react-native";
import Image from "react-native-remote-svg";
import { Container, Button, Text, Right, Content } from "native-base";
import { withNavigation } from "react-navigation";
import BodyWrapper from "../../components/BodyWrapper";
import BackButton from "../../components/BackButton";

class FAQ12 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={{ marginHorizontal: 20, top: 20 }}>
              <Text style={styles.textStyle}>
                If the results of a criminal history record check (state or
                national) cause an adverse employment or licensing decision the
                individual, business or agency making the adverse decision must
                inform the person of all information pertinent to that decision.
                This disclosure must include information that a criminal history
                record check was conducted, the specific contents of the record
                and the affect the record had on the decision. Failure to
                provide all such information to the person subject to the
                adverse employment or licensing decision is a misdemeanor
                offense under Georgia law.
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

export default withNavigation(FAQ12);

const styles = StyleSheet.create({
  FAQ: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    color: "#C4C4C4"
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  },
  textStyle: {
    fontFamily: "OpenSans-Light"
  }
});
