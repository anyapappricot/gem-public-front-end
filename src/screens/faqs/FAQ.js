import React, { Component } from "react";
import { StyleSheet, Platform, View } from "react-native";
import { Content, Text, List, ListItem, Button } from "native-base";
import Image from "react-native-remote-svg";
import BodyWrapper from "../../components/BodyWrapper";

const datas = [
  {
    route: "FAQ1",
    text:
      "- What forms of identification do I need to bring with me to the fingerprint location?"
  },
  {
    route: "FAQ2",
    text:
      "- What Georgia criminal history record information is available for employment or licensing purposes?"
  },
  {
    route: "FAQ3",
    text:
      "- What is required to obtain Georgia criminal history record information for employment or licensing purposes?"
  },
  {
    route: "FAQ4",
    text:
      "- Is a separate signed consent form required when submitting fingerprints for employment or licensing purposes?"
  },
  {
    route: "FAQ5",
    text:
      "- Can Georgia criminal history record information be obtained for employment or licensing purposes without submitting fingerprints?"
  },
  {
    route: "FAQ6",
    text:
      "- How is Georgia criminal history record information obtained from GCIC for employment or licensing purposes?"
  },
  {
    route: "FAQ7",
    text:
      "- What national (FBI) criminal history record information is available for employment or licensing purposes?"
  },
  {
    route: "FAQ8",
    text:
      "- Can private agencies or entities receive national (FBI) criminal history record information for employment or licensing purposes?"
  },
  {
    route: "FAQ9",
    text:
      "- What information is available under the National Child Protection Act (NCPA) and Volunteers for Children Act (VCA)?"
  },
  {
    route: "FAQ10",
    text:
      "- Can local law enforcement agencies provide national (FBI) criminal history record information for employment or licensing purposes?"
  },
  {
    route: "FAQ11",
    text:
      "- Can national criminal history record information be obtained directly from the FBI for employment or licensing purposes?"
  },
  {
    route: "FAQ12",
    text:
      "- What must occur if the results of a criminal history record check cause an adverse employment or licensing decision?"
  },
  {
    route: "FAQ13",
    text:
      "- Can an individual obtain a copy of his or her Georgia or national (FBI) criminal history record?"
  },
  {
    route: "FAQ14",
    text:
      "- What should an individual do if his or her Georgia or national (FBI) criminal history record contains an error or needs updating?"
  }
];

class FAQ extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={style.content}>
              <Text style={style.FAQ}>FAQs</Text>
              <Text style={style.line}>
                __________________________________________
              </Text>
              <Text style={style.explanation}>
                Obtaining Criminal History Record Information for
                Employment/Licensing FAQs
              </Text>

              <List
                style={style.listWrapper}
                dataArray={datas}
                renderRow={data => (
                  <ListItem
                    button
                    onPress={() => this.props.navigation.navigate(data.route)}
                  >
                    <Text style={style.listItemText}>{data.text}</Text>
                  </ListItem>
                )}
              />
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("HomeScreen")}
          >
            <Image
              source={{
                uri: `data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" fill="#000000" fill-opacity="0.5" width="30" height="30" viewBox="5 3 17 17"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/></svg>								`
              }}
            />
          </Button>
        }
      />
    );
  }
}

export default FAQ;

const style = StyleSheet.create({
  content: { marginHorizontal: 15 },
  listWrapper: { marginBottom: 50 },
  listItemText: { fontFamily: "OpenSans-Light" },
  FAQ: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    color: "#C4C4C4"
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  }
});
