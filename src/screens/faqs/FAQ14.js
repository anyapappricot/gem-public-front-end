import React, { Component } from "react";
import { StyleSheet, Platform, View } from "react-native";
import Image from "react-native-remote-svg";
import { Container, Button, Text, Right, Content } from "native-base";
import { withNavigation } from "react-navigation";
import ScreenHeader from "../../components/ScreenHeader";
import BodyWrapper from "../../components/BodyWrapper";
import BackButton from "../../components/BackButton";

class FAQ14 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={{ marginHorizontal: 20, top: 20 }}>
              <Text style={styles.textStyle}>
                The GCIC does not have authority to modify a state criminal
                history record without appropriate documentation and/or
                authorization of the submitting criminal justice agency. If an
                individual wishes to challenge the accuracy of his or her
                Georgia criminal history record, they should contact GCIC at
                404-270-2639, Option 2 for requirements and assistance.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                An individual may challenge the information contained in the FBI
                criminal history record by contacting the original agency that
                submitted the information to the FBI or the state central
                repository in the state where the arrest occurred. These
                agencies will be able to furnish guidelines for correcting the
                record. The FBI does not have authority to modify the record
                without written notification from the appropriate criminal
                justice agency.
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

export default withNavigation(FAQ14);

const styles = StyleSheet.create({
  textStyle: {
    fontFamily: "OpenSans-Light"
  }
});
