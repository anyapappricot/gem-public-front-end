import React, { Component } from "react";
import { StyleSheet, Platform, View } from "react-native";
import Image from "react-native-remote-svg";
import { Container, Button, Text, Right, Content } from "native-base";
import { withNavigation } from "react-navigation";
import ScreenHeader from "../../components/ScreenHeader";
import BodyWrapper from "../../components/BodyWrapper";
import BackButton from "../../components/BackButton";

class FAQ11 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={{ marginHorizontal: 20, top: 20 }}>
              <Text style={styles.textStyle}>
                The United States Department of Justice Order 556-73 establishes
                rules and regulations for the subject of a FBI criminal history
                record to obtain a copy of his or her own record for personal
                review, or to challenge information on the record. An individual
                may also request a copy of his or her FBI criminal history
                record to satisfy a requirement to live or work in a foreign
                country, or for an international adoption.{" "}
                <Text style={{ textDecorationLine: "underline" }}>
                  This process is not for an employment or licensing background
                  check.{" "}
                </Text>
                The FBI has authority to conduct a criminal history record check
                for non-criminal justice purposes based on Public Law 92-544.
                More information on the United States Department of Justice
                Order 556-73 is available at{" "}
                <Text style={{ fontStyle: "italic" }}>
                  http://www.fbi.gov/hq/cjisd/fprequest.html
                </Text>
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

export default withNavigation(FAQ11);

const styles = StyleSheet.create({
  FAQ: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    color: "#C4C4C4"
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  },
  textStyle: {
    fontFamily: "OpenSans-Light"
  }
});
