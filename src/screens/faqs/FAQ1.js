import React, { Component } from "react";
import { StyleSheet, Platform, View } from "react-native";
import Image from "react-native-remote-svg";
import { Container, Button, Text, Right, Content } from "native-base";
import { withNavigation } from "react-navigation";
import BodyWrapper from "../../components/BodyWrapper";
import BackButton from "../../components/BackButton";

class FAQ1 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={{ marginHorizontal: 20, top: 20 }}>
              <Text style={styles.textStyle}>
                Cogent Systems requires current, valid and unexpired picture
                identification documents. As a primary form of picture
                identification one of the following will be accepted at the GAPS
                Print Locations.
              </Text>
              <Text />
              <Text style={styles.textStyle}>Primary Documents include:</Text>
              <Text style={styles.textStyle}>
                - State Issued Driver’s License with Photograph
              </Text>
              <Text style={styles.textStyle}>
                - State Issued Identification Card with Photograph
              </Text>
              <Text style={styles.textStyle}>
                - US Passport with Photograph
              </Text>
              <Text style={styles.textStyle}>
                - US Active Duty/Retiree/Reservist Military ID Card (000 10-2)
                with Photograph
              </Text>
              <Text style={styles.textStyle}>
                - Government Issued Employee Identification Card with Photograph
                (includes Federal, State, County, City, etc.)
              </Text>
              <Text style={styles.textStyle}>
                - Tribal Identification Card with Photograph
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                However, in the absence of one of the above Primary
                identifications, applicants may provide one or more of the
                following Secondary Documents, along with two of the supporting
                documents listed below:
              </Text>
              <Text />
              <Text style={styles.textStyle}>Secondary Documents:</Text>
              <Text />
              <Text style={styles.textStyle}>
                - State Government Issued Certificate of Birth
              </Text>
              <Text style={styles.textStyle}>- Social Security Card</Text>
              <Text style={styles.textStyle}>
                - Certificate of Citizenship (N560)
              </Text>
              <Text style={styles.textStyle}>
                - Certificate of Naturalization (N550)
              </Text>
              <Text style={styles.textStyle}>
                - INS I-551 Resident Alien Card Issued since 1997
              </Text>
              <Text style={styles.textStyle}>
                - NS 1-688 Temporary Resident Identification Card
              </Text>
              <Text style={styles.textStyle}>
                - INS I-688B, I-766 Employment Authorization Card
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                Secondary Documentation must be supported by at least two of the
                following:
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                - Utility Bill (Including street address)
              </Text>
              <Text style={styles.textStyle}>- Voter Registration Card</Text>
              <Text style={styles.textStyle}>
                - Vehicle Registration Card/Title
              </Text>
              <Text style={styles.textStyle}>
                - Certificate of Naturalization (N550)
              </Text>
              <Text style={styles.textStyle}>
                - Current Paycheck Stub with Name and Address
              </Text>
              <Text style={styles.textStyle}>
                - Cancelled Check or Bank Statement
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

export default withNavigation(FAQ1);

const styles = StyleSheet.create({
  FAQ: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    color: "#C4C4C4"
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  },
  textStyle: {
    fontFamily: "OpenSans-Light"
  }
});
