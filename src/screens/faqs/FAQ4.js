import React, { Component } from "react";
import { StyleSheet, Platform, View } from "react-native";
import Image from "react-native-remote-svg";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Body,
  Text,
  Right,
  Content
} from "native-base";
import { withNavigation } from "react-navigation";
import ScreenHeader from "../../components/ScreenHeader";
import BodyWrapper from "../../components/BodyWrapper";
import BackButton from "../../components/BackButton";

class FAQ4 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={{ marginHorizontal: 20, top: 20 }}>
              <Text style={styles.textStyle}>
                The Georgia Crime Information Center (GCIC) does not require a
                separate signed consent form when submitting fingerprints for
                employment or licensing purposes. However, an
                employing/licensing agency may require signed consent from the
                individual for the agency’s own retention and purposes. GCIC
                recommends employing/licensing agencies have individuals sign an
                Acknowledgement Form to ensure they understand that fingerprints
                are necessary for the purpose of a criminal history record check
                and the results will help determine suitability for employment
                or licensing. Refer to the GAPS Applicant Waiver under
                Registration on the website’s main menu.
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

export default withNavigation(FAQ4);

const styles = StyleSheet.create({
  FAQ: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    color: "#C4C4C4"
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  },
  textStyle: {
    fontFamily: "OpenSans-Light"
  }
});
