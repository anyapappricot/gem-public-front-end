import React, { Component } from "react";
import { StyleSheet, Platform, View } from "react-native";
import Image from "react-native-remote-svg";
import { Container, Button, Text, Right, Content } from "native-base";
import { withNavigation } from "react-navigation";
import ScreenHeader from "../../components/ScreenHeader";
import BodyWrapper from "../../components/BodyWrapper";
import BackButton from "../../components/BackButton";

class FAQ2 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={{ marginHorizontal: 20, top: 20 }}>
              <Text style={styles.textStyle}>
                Georgia criminal history record information, except for sealed
                records, is available for employment or licensing purposes.
                Sealed records include juvenile records, expunged records, and
                most sentences successfully completed under the First Offender
                Act. Georgia law authorizes release of completed first offender
                records for specific offenses when conducting pre-employment
                criminal history record checks for particular job fields, e.g.
                working with children, persons who are mentally disabled or the
                elderly.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                The Georgia criminal history record includes the subject's
                identification data (name, date of birth, social security
                number, sex, race, height, weight, etc.), arrest data (including
                arresting agency, date of arrest and charges), final judicial
                disposition data submitted by a court, prosecutor or other
                criminal justice agency and custodial information, if the
                offender was incarcerated in a Georgia correctional facility.
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

export default withNavigation(FAQ2);

const styles = StyleSheet.create({
  FAQ: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    color: "#C4C4C4"
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  },
  textStyle: {
    fontFamily: "OpenSans-Light"
  }
});
