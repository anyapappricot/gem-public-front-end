import React, { Component } from "react";
import { StyleSheet, Platform, View } from "react-native";
import Image from "react-native-remote-svg";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Body,
  Text,
  Right,
  Content
} from "native-base";
import { withNavigation } from "react-navigation";
import ScreenHeader from "../../components/ScreenHeader";
import BodyWrapper from "../../components/BodyWrapper";
import BackButton from "../../components/BackButton";

class FAQ7 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={{ marginHorizontal: 20, top: 20 }}>
              <Text style={styles.textStyle}>
                Under federal regulations and Georgia law, certain businesses,
                organizations, licensing boards and state and local governments
                have the authority to obtain criminal history record information
                maintained by the FBI for employment or licensing purposes.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                Before the FBI will provide criminal history record information,
                the following standards known as “Public Law 92-544 statutes”
                must be in effect:
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                Before the FBI will provide criminal history record information,
                the following standards known as “Public Law 92-544 statutes”
                must be in effect:
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                - There must be a state statute authorizing access of the FBI
                criminal history file
              </Text>
              <Text style={styles.textStyle}>
                - The state statute must specifically call for the use of FBI
                records
              </Text>
              <Text style={styles.textStyle}>
                - The request must not violate public policy and comply with all
                state requirements
              </Text>
              <Text style={styles.textStyle}>
                - The request can not be overly broad in scope, but must
                identify the specific category of employment or licensing
              </Text>
              <Text style={styles.textStyle}>
                - The applicant must be fingerprinted{" "}
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                Access to FBI criminal history record information is subject to
                numerous restrictive laws and regulations. Dissemination of
                criminal history record information to entities outside the
                requesting organization or agency is prohibited and a violation
                of law.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                Authorized FBI criminal history record checks are conducted by
                submitting the applicant’s fingerprints to GCIC for both a state
                and FBI record check.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                Refer to the FBI Approved FP List for the current Georgia
                “92-544” statutes. The list can be found under General
                Information on the website’s main menu.
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

export default withNavigation(FAQ7);

const styles = StyleSheet.create({
  FAQ: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    color: "#C4C4C4"
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  },
  textStyle: {
    fontFamily: "OpenSans-Light"
  }
});
