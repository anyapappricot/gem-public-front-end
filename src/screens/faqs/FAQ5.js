import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Image from "react-native-remote-svg";
import { Container, Button, Text, Right, Content } from "native-base";
import { withNavigation } from "react-navigation";
import BodyWrapper from "../../components/BodyWrapper";
import BackButton from "../../components/BackButton";

class FAQ5 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={{ marginHorizontal: 20, top: 20 }}>
              <Text style={styles.textStyle}>
                Georgia criminal history record information, based on a search
                of name and descriptive data only, is obtainable from local law
                enforcement agencies providing this service. Please contact
                local law enforcement agencies directly for information on
                agency specific requirements and fees.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                Agencies should understand that when obtaining criminal history
                record information using the individual’s identification data
                (name, sex, race and date of birth), it is possible that the
                information returned belongs to a different person with the
                same, or similar, identifiers.
              </Text>
              <Text />
              <Text style={styles.textStyle}>
                GCIC will only provide criminal history record information based
                on an individual’s fingerprints because when conducting a
                fingerprint search the information returned does in fact, belong
                to the individual.
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

export default withNavigation(FAQ5);

const styles = StyleSheet.create({
  FAQ: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    color: "#C4C4C4"
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10
  },
  textStyle: {
    fontFamily: "OpenSans-Light"
  }
});
