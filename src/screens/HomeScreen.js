import React, { PureComponent } from "react";

import { View, StyleSheet, Dimensions } from "react-native";

import { Content, Text } from "native-base";
import Image from "react-native-remote-svg";
import BodyWrapper from "../components/BodyWrapper";

import ScreenHeader from "../components/ScreenHeader";
import SmartButton from "../components/SmartButton";

const width = Dimensions.get("window").width;

class HomeScreen extends PureComponent {
  render() {
    const { ...props } = this.props;
    return (
      <BodyWrapper
        {...props}
        body={
          <Content
            contentContainerStyle={styles.contentContainerStyleStyleSheet}
          >
            <View style={styles.logoMap}>
              <Image
                source={require("../assets/home_screen/LogoMap.svg")}
                style={styles.image_style}
              />
            </View>

            <View style={styles.groupOfButtons}>
              <SmartButton
                buttonBorderStyle={styles.buttonBorderStyle}
                text={"New Registration"}
                onPress={() => {
                  this.props.navigation.navigate("NewRegistrationScreen");
                }}
                image={
                  <Image
                    source={require("../assets/home_screen/fingerprint_white.svg")}
                    style={styles.smart_button_svg}
                  />
                }
              />
              <SmartButton
                buttonBorderStyle={styles.buttonBorderStyle}
                onPress={() => {
                  this.props.navigation.navigate("LogInToViewStatus");
                }}
                text={"View Registration Status"}
                image={
                  <Image
                    source={require("../assets/home_screen/search_white.svg")}
                    style={styles.smart_button_svg}
                  />
                }
              />
            </View>

            <View style={styles.footer}>
              <Image
                // source={require("../assets/home_screen/new_gemalto.png")}
                source={require("../assets/home_screen/logo-gemalto-thales.gif")}
                style={styles.logo_image}
              />
              <View>
                <Text style={styles.copyright}>
                  © 2019 Copyright Gemalto , Inc. All rights reserved.
                </Text>
              </View>
            </View>
          </Content>
        }
      />
    );
  }
}
const image_width =
  width < 375 ? width / 1.5 : width > 700 ? width / 2 : width / 1.5;

const styles = StyleSheet.create({
  contentContainerStyleStyleSheet: {
    flexGrow: 1
    // backgroundColor: "white"
    // flex: 1,
    // justifyContent: "center",
    // alignItems: "center"
  },
  logoMap: {
    flex: width < 375 ? 1 : 2,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonBorderStyle: {
    width: width / 1.2
  },
  smart_button_svg: {
    height: width <= 375 ? 35 : 50,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.1,
    shadowRadius: 5
  },
  image_style: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: image_width * 1.29310344828,
    height: image_width
  },
  groupOfButtons: {
    flex: 1,
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  footer: {
    alignItems: "center",
    marginBottom: 10
  },
  logo_image: {
    alignItems: "center",
    height: 34,
    width: 100
  },
  copyright: {
    fontFamily: "Montserrat-Light",
    fontSize: 8,
    alignItems: "center"
  }
});

export default HomeScreen;
// height: 34,
// width: 100
// source={require("../assets/home_screen/logo-gemalto-thales.gif")}
