import React, { Component } from "react";

import { View, StyleSheet, Platform, Dimensions } from "react-native";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Button, Text, Form } from "native-base";
import RadioForm from "react-native-simple-radio-button";

import { connect } from "react-redux";
import { Field, reduxForm, formValueSelector } from "redux-form";
import { withNavigation } from "react-navigation";
import SmartInput from "../../components/SmartInput";

import {
  getRegistrationStatus,
  resetRegistrationStatus
} from "../../redux/actions/actions";

import BodyWrapper from "../../components/BodyWrapper";
import SmartButton from "../../components/SmartButton";
import HomeButton from "../../components/HomeButton";
import Storage from "../../components/Storage";

import { _U } from "../../assets/utils";

import {
  required,
  minLength1,
  required_ga
} from "../../validation/validations";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

const radio_props = [
  { label: `Search by \nFirstName, LastName, Date of Birth`, value: 0 },
  { label: `Search by \nRegistrationID, Date Of Birth`, value: 1 }
];

class LogInToViewStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      option: false,
      reg_id: "",
      first_name: "",
      last_name: "",
      dob: ""
    };
  }

  componentDidMount = async () => {
    await this.props.dispatch(resetRegistrationStatus({}));
  };

  _toggle_switch = async () => {
    await this.setState({
      option: !this.state.option
    });
  };

  _set_data_from_storage = async (key, value) => {
    await this.setState({ [key]: _U(value) });
  };

  render() {
    const list = ["reg_id", "first_name", "last_name", "dob"];
    const { first_name, last_name, dob, reg_id, side_bar_switch } = this.props;

    // console.warn(first_name);
    // first_name ? console.warn(first_name.length) : null;

    return (
      <BodyWrapper
        {...this.props}
        body={
          <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <Storage list={list} callback={this._set_data_from_storage} />

            <View style={{ flex: 1 }}>
              <Text style={styles.screenTitle}>View Registration Status</Text>
              <Text style={styles.line} />
            </View>

            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                paddingTop: 20
              }}
            >
              <View
                style={{
                  flex: 1,
                  // width: width / 2,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <RadioForm
                  radio_props={radio_props}
                  initial={0}
                  labelColor={"#000100"}
                  buttonColor={"#6699cc"}
                  animation={false}
                  // animation={true}
                  onPress={value => {
                    this.setState({ value: value });
                  }}
                />
              </View>
            </View>

            <View
              style={{
                minHeight: 300,
                flex: 1,
                justifyContent: "center"
              }}
            >
              <Text />
              <View style={styles.card_container}>
                <Form>
                  {this.state.value ? (
                    // {this.state.option ? (
                    <Field
                      type={"text"}
                      label="Registration ID *"
                      name="registrationID"
                      // auto_fill={this.state.reg_id ? this.state.reg_id : null}
                      // auto_fill={side_bar_switch ? "Insurance Licensees" : null}
                      component={SmartInput}
                    />
                  ) : (
                    <View>
                      <Field
                        type={"text"}
                        label="First Name *"
                        name="first_name"
                        component={SmartInput}
                        auto_fill={side_bar_switch ? "Tiffany" : null}
                        // auto_fill={side_bar_switch ? "Test" : "Tiffany"}
                        // validate={[required, maxLength50, minLength1]}
                      />
                      <Field
                        type={"text"}
                        label="Last Name *"
                        name="last_name"
                        component={SmartInput}
                        auto_fill={side_bar_switch ? "Ip" : null}
                        // auto_fill={side_bar_switch ? "Test" : "Ip"}
                        // validate={[required, maxLength50, minLength1]}
                      />
                    </View>
                  )}
                  <Field
                    type={"text"}
                    label="Date of Birth in MMDDYYYY format *"
                    name="birthDate"
                    component={SmartInput}
                    auto_fill={side_bar_switch ? "01011990" : null}
                    // auto_fill={side_bar_switch ? "01011990" : "01011990"}
                    // validate={[required, birthDate]}
                  />
                </Form>
              </View>
            </View>
            <View
              style={{
                flex: 4,
                alignSelf: "center",
                paddingBottom: 20
              }}
            >
              <SmartButton
                text={"Continue"}
                buttonBorderStyle={{ width: width / 2 }}
                style={{ alignSelf: "center" }}
                onPress={async () => {
                  await this.props.navigation.navigate("ViewStatus");
                  await this.props.dispatch(
                    getRegistrationStatus(
                      this.state.option
                        ? { reg_id: _U(reg_id), dob: _U(dob) }
                        : {
                            last_name: _U(last_name),
                            first_name: _U(first_name),
                            dob: _U(dob)
                          }
                    )
                  );
                }}
                style={{
                  alignSelf: "center",
                  borderRadius: 0,
                  backgroundColor: "#b8bdc4",
                  padding: 40
                }}
              />
            </View>
          </KeyboardAwareScrollView>
        }
        footerPanels={1}
        footer={
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("HomeScreen")}
          >
            <HomeButton />
          </Button>
        }
      />
    );
  }
}

const notConnected = reduxForm({ form: "logIn" })(
  withNavigation(LogInToViewStatus)
);

const selector = formValueSelector("logIn");

const LogInToViewStatusConnected = connect(state => {
  const first_name = selector(state, "first_name");
  const last_name = selector(state, "last_name");
  const dob = selector(state, "birthDate");
  const reg_id = selector(state, "registrationID");

  return {
    first_name,
    last_name,
    dob,
    reg_id
  };
})(notConnected);
const mapStateToProps = state => ({
  side_bar_switch: state.dev_reducer.options.side_bar_switch
});

export default connect(mapStateToProps)(LogInToViewStatusConnected);

// https://redux-form.com/6.0.2/examples/selectingformvalues/
// just in case for you to revise redux form

const styles = StyleSheet.create({
  card_container: {
    marginBottom: 40
  },
  disabledButton: {
    justifyContent: "center",
    alignItems: "center",
    top: 20,
    marginHorizontal: Platform.OS === "ios" ? 50 : 80,
    marginBottom: 170,
    width: 259,
    height: 32,
    borderRadius: 0
  },
  buttonText: {
    color: "#000100",
    fontFamily: "Segoeui",
    fontSize: 15,
    textAlign: "center",
    paddingTop: Platform.OS === "ios" ? 5 : 0,
    paddingBottom: Platform.OS === "ios" ? 25 : 0,
    paddingRight: Platform.OS === "ios" ? 15 : 5,
    width: 270
  },
  screenTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    borderColor: "#C4C4C4",
    height: 1,
    borderWidth: 0.5,
    marginRight: 20,
    marginLeft: 20,
    marginTop: 20,
    marginBottom: 20
  },
  explanation: {
    fontFamily: "OpenSans-Light",
    fontSize: 12,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingBottom: 10
  }
});
