import React, { PureComponent } from "react";

import {
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  ActivityIndicator
} from "react-native";

import { Button, Text } from "native-base";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";

import Barcode from "react-native-barcode-builder";
import ViewStatusListItem from "../../components/ViewStatusListItem";
import BodyWrapper from "../../components/BodyWrapper";
import SmartButton from "../../components/SmartButton";
import BackButton from "../../components/BackButton";

import { resetRegistrationStatus } from "../../redux/actions/actions";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

const base_data = {
  reg_id: "",
  last_name: "",
  first_name: "",
  app_email: "",
  dob: "",
  approve_date: "",
  registrationDate: "",
  emailConfirmDate: ""
};

class ViewStatusNotConnected extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      nested_modal_visible: false,
      no_data_found_boolean: false,
      renderResult: base_data
    };
  }

  componentWillUnmount = () => {
    clearTimeout(this._ref_activity_indicator_container);
  };

  _reset = async data => {
    await this.props.dispatch(resetRegistrationStatus(data));
  };

  _toggle_nested_modal = () => {
    this.setState({ nested_modal_visible: !this.state.nested_modal_visible });
  };

  _navigate_to_log_in_to_view_status = async () => {
    await this.setState({ no_data_found_boolean: false });
    await this.props.navigation.navigate("LogInToViewStatus");
  };

  _ref_activity_indicator_container = setTimeout(() => {
    if (this.props.registration_data.length === undefined) {
      this.setState({
        no_data_found_boolean: true
      });
    }
  }, 10000);

  _user_not_found = () => {
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Text style={styles.warning_message}>
          No user found at this time...
        </Text>
        <SmartButton
          buttonBorderStyle={{ width: width / 3 }}
          text={"Go Back"}
          onPress={this._navigate_to_log_in_to_view_status}
        />
      </View>
    );
  };

  _render_activity_indicator = () => {
    return <ActivityIndicator size="large" color="#E06C08" />;
  };

  render() {
    const { no_data_found_boolean, nested_modal_visible } = this.state;
    const { registration_data } = this.props;
    const reg_data = registration_data[0];

    return (
      <BodyWrapper
        {...this.props}
        body={
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ flex: 1 }}>
              <Text style={styles.subTitle}>
                {!nested_modal_visible
                  ? "View Registration Status"
                  : "Registration Selection"}
              </Text>
              <Text style={styles.line} />
            </View>

            <View
              style={{
                flex: 9,
                justifyContent: "center"
              }}
            >
              {this.props.registration_data.length !== undefined ? (
                <View>
                  {registration_data.length > 1 ? (
                    this.state.renderResult.last_name === "" ? (
                      <ScrollView
                        contentContainerStyle={{
                          justifyContent: "center",
                          alignItems: "center",
                          padding: 25
                        }}
                      >
                        {!nested_modal_visible ? (
                          <View>
                            <Text style={styles.warning_message}>
                              We've found multiple registrations with the same
                              name and date of birth.
                            </Text>
                            <View
                              style={{
                                alignItems: "center",
                                paddingTop: 25,
                                paddingBottom: 25
                              }}
                            >
                              <SmartButton
                                buttonBorderStyle={{ width: width / 2 }}
                                buttonTextStyle={{ color: "white" }}
                                text={"CHOOSE ONE."}
                                onPress={this._toggle_nested_modal}
                              />
                            </View>
                          </View>
                        ) : (
                          <View>
                            {this.props.registration_data.map((data, index) => {
                              // console.warn(JSON.stringify(data, null, 2));
                              return (
                                <View
                                  key={index}
                                  style={{
                                    flexDirection: "column",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    textAlign: "center"
                                  }}
                                >
                                  <ViewStatusListItem
                                    nearly_identical={true}
                                    reg_id={data.reg_id}
                                    last_name={data.last_name}
                                    first_name={data.first_name}
                                    dob={data.dob}
                                    pay_flag={data.pay_flag}
                                    app_email={data.app_email}
                                    user_fee={data.user_fee}
                                    email_confirmed={data.email_confirmed}
                                  />

                                  <View style={{ padding: 20 }}>
                                    <SmartButton
                                      buttonBorderStyle={{ width: width / 2 }}
                                      text={"Select"}
                                      onPress={() => {
                                        this._toggle_nested_modal();
                                        this.setState({
                                          renderResult: data
                                        });
                                      }}
                                    />
                                  </View>
                                  <View>
                                    <View style={styles.blue_line} />
                                    <View style={styles.line} />
                                  </View>
                                </View>
                              );
                            })}
                          </View>
                        )}
                      </ScrollView>
                    ) : (
                      <ScrollView>
                        <View style={{ paddingBottom: 20 }}>
                          <Barcode
                            width={1.5}
                            height={50}
                            value={`${this.state.renderResult.reg_id}${
                              this.state.renderResult.dob
                            }`}
                            format="CODE128"
                          />
                        </View>

                        <View style={styles.list_container}>
                          <ViewStatusListItem
                            reg_id={this.state.renderResult.reg_id}
                            last_name={this.state.renderResult.last_name}
                            first_name={this.state.renderResult.first_name}
                            dob={this.state.renderResult.dob}
                            app_email={this.state.renderResult.app_email}
                            user_fee={this.state.renderResult.user_fee}
                            email_confirmed={
                              this.state.renderResult.email_confirmed
                            }
                            approve_date={this.state.renderResult.approve_date}
                            reject_date={this.state.renderResult.reject_date}
                            pay_flag={this.state.renderResult.pay_flag}
                            pay_date={this.state.renderResult.pay_date}
                            received={this.state.renderResult.received}
                            used={this.state.renderResult.used}
                            in_queue={this.state.renderResult.in_queue}
                          />
                        </View>

                        <View
                          style={{
                            alignItems: "center",
                            paddingTop: 25,
                            paddingBottom: 25
                          }}
                        >
                          <SmartButton
                            buttonBorderStyle={{ width: width / 2 }}
                            buttonTextStyle={{ color: "white" }}
                            text={"Pick Another"}
                            // onPress={this._toggle_nested_modal}
                            onPress={() => {
                              this._toggle_nested_modal();
                              this.setState({
                                renderResult: base_data
                              });
                            }}
                          />
                        </View>
                        <View
                          style={{
                            marginTop: 20,
                            alignSelf: "center",
                            width: 325
                          }}
                        >
                          <Text
                            style={{
                              justifyContent: "center",
                              textAlign: "center",
                              fontSize: 15,
                              color: "#3E4649",
                              paddingBottom: 25
                            }}
                          >
                            This registration will expire after 90 days from the
                            registration date. If the applicant has not been
                            fingerprinted within 90 days, the registration will
                            be cancelled and any payment made will be refunded.
                            Please print this information if needed and present
                            it to the fingerprint site.
                          </Text>
                        </View>
                      </ScrollView>
                    )
                  ) : (
                    <ScrollView>
                      <View style={{ paddingBottom: 20 }}>
                        <Barcode
                          width={1.5}
                          height={50}
                          value={`${this.props.registration_data.reg_id}${
                            this.props.registration_data.dob
                          }`}
                          format="CODE128"
                        />
                      </View>
                      <View style={styles.list_container}>
                        <ViewStatusListItem
                          reg_id={reg_data.reg_id}
                          last_name={reg_data.last_name}
                          first_name={reg_data.first_name}
                          app_email={reg_data.app_email}
                          user_fee={reg_data.user_fee}
                          dob={reg_data.dob}
                          email_confirmed={reg_data.email_confirmed}
                          approve_date={reg_data.approve_date}
                          reject_date={reg_data.reject_date}
                          pay_flag={reg_data.pay_flag}
                          pay_date={reg_data.pay_date}
                          received={reg_data.received}
                          used={reg_data.used}
                          in_queue={reg_data.in_queue}
                        />
                      </View>
                      <View
                        style={{
                          marginTop: 20,
                          alignSelf: "center",
                          width: 325
                        }}
                      >
                        <Text
                          style={{
                            justifyContent: "center",
                            textAlign: "center",
                            fontSize: 15,
                            color: "#3E4649",
                            paddingBottom: 25
                          }}
                        >
                          This registration will expire after 90 days from the
                          registration date. If the applicant has not been
                          fingerprinted within 90 days, the registration will be
                          cancelled and any payment made will be refunded.
                          Please print this information if needed and present it
                          to the fingerprint site.
                        </Text>
                      </View>
                    </ScrollView>
                  )}
                </View>
              ) : (
                // <NotRegistered />
                // DO NOT FORGET TO SET
                // REG-DATA AS {} WHEN THIS PAGE EXISTS

                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    backdropColor: "red"
                  }}
                  ref={this._ref_activity_indicator_container}
                >
                  {no_data_found_boolean
                    ? this._user_not_found()
                    : this._render_activity_indicator()}
                </View>
              )}
            </View>
          </ScrollView>
        }
        footerPanels={1}
        footer={
          <Button
            transparent
            onPress={() => {
              this._reset(this.state.renderResult);
              this.props.navigation.goBack();
            }}
          >
            <BackButton />
          </Button>
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  registration_data: state.userInput.registration_data
});

const ViewStatus = connect(mapStateToProps)(ViewStatusNotConnected);

export default withNavigation(ViewStatus);

const styles = StyleSheet.create({
  warning_message: {
    fontSize: 27,
    textAlign: "center",
    fontFamily: "Montserrat-Regular",
    marginBottom: 25
  },
  subTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    borderColor: "#C4C4C4",
    height: 1,
    borderWidth: 0.5,
    marginRight: 20,
    marginLeft: 20,
    marginTop: 20,
    marginBottom: 20
  },

  sub_sub_title: {
    fontFamily: "Montserrat-Regular",
    fontSize: 19,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25,
    paddingBottom: 100
  },
  list_container: {
    justifyContent: "center",
    alignSelf: "center"
  },
  blue_line: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: "#01366C",
    height: 0.5
  }
});
