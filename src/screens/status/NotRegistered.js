import React, { Component } from "react";
import { View } from "react-native";
import { Text } from "native-base";

export default class NotRegistered extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Please go to New registration page first.</Text>
      </View>
    );
  }
}
