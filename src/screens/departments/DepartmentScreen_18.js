import React, { Component } from "react";
import { View } from "react-native";
import { Content, Button, Left, Text } from "native-base";
import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_18 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              The Georgia Department of Defense coordinates and supervises all
              agencies and functions of the Georgia National Guard, including
              the Georgia Army National Guard, the Georgia Air National Guard,
              and the Georgia State Defense Force.
            </Text>
            <Text style={styleGlobal.explanation}>
              The department provides ready and relevant military forces to the
              Combatant Commanders and, with the consent of the Governor,
              provides command and control capabilities to support Homeland
              Defense and Defense Support of Civil Authorities.
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>DOD Website</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>1000 Halsey Ave</Text>
                <Text style={styles.contactInfo}>Marietta, GA 30060</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
