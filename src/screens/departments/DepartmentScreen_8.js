import React, { Component } from "react";

import { View } from "react-native";

import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_8 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              The Georgia Secretary of State registers voters, tracks annual
              corporate filings, grants professional licenses and oversees the
              state's securities' market.
            </Text>
            <Text style={styleGlobal.explanation}>
              To register for a background check, please select one of the
              options below
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>http://sos.ga.gov</Text>
                <Text style={styles.website}>
                  Private Detectives & Security Agencies
                </Text>
                <Text style={styles.contactInfo}>or</Text>
                <Text style={styles.website}>Download applications</Text>
                <Text style={styles.website}>
                  Used Motor Vehicle & Vehicle Parts Dealers
                </Text>
                <Text style={styles.contactInfo}>or</Text>
                <Text style={styles.website}>Download applications</Text>
                <Text style={styles.website}>
                  Board of Nursing or Download applications
                </Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>214 State Capitol</Text>
                <Text style={styles.contactInfo}>Atlanta, GA 30334</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfoTitleGrey}>Call Center</Text>
                <Text style={styles.contactInfo}>(478) 207-2440</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
