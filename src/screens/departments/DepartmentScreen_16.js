import React, { Component } from "react";
import { View } from "react-native";
import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_16 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              We are the state agency that regulates and examines Georgia
              state-chartered banks, state-chartered credit unions, and
              state-chartered trust companies. We also have responsibility for
              the supervision, regulation, and examination of Merchant Acquirer
              Limited Purpose Banks (MALPBs) chartered in Georgia.
            </Text>
            <Text style={styleGlobal.explanation}>
              Please be aware that the DBF does not regulate all financial
              institutions doing business in Georgia. Specifically, we do not
              regulate national banks (examples: Bank of America, Wells Fargo,
              Chase, Citibank, PNC) or federal credit unions, whether operating
              in Georgia or elsewhere.
            </Text>
            <Text style={styleGlobal.explanation}>
              In addition to Georgia state-chartered financial institutions, the
              DBF has regulatory and/or licensing authority over mortgage
              brokers, lenders and processors, mortgage loan originators, check
              cashers, sellers-issuers of payment instruments, money
              transmitters, international banking organizations, and bank
              holding companies conducting business in Georgia.
            </Text>
            <Text style={styleGlobal.explanation}>
              The DBF is accredited through the Conference of State Bank
              Supervisors (CSBS) and the National Association of State Credit
              Union Supervisors (NASCUS). Both of these accreditation programs
              provide an opportunity and a system for state regulatory agencies
              to identify opportunities for efficiencies as well as best
              practices from our state banking and credit union department
              colleagues across the country.
            </Text>
            <Text style={styleGlobal.explanation}>
              The DBF remains committed to targeting mortgage fraud and
              unlicensed activity. We will continue to work with federal, state,
              and local law enforcement and regulatory agencies, the Georgia
              Real Estate Fraud Prevention & Awareness Coalition, community task
              forces, and the industry to fight mortgage fraud and unlicensed
              activity.
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>
                  Department of Banking and Finance
                </Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
