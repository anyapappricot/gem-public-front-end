import React, { Component } from "react";
import { View } from "react-native";
import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_2 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              The Georgia Department of Behavioral Health and Developmental
              Disabilities provides treatment and support services to people
              with behavioral health challenges and addictive diseases, and
              assists individuals who live with developmental disabilities. The
              agency's mission is to provide high-quality health care
              opportunities for individuals with developmental disabilities or
              behavioral health challenges close to their homes, so they can
              live a life of independence and recovery and create a sustainable,
              self-sufficient and resilient life in their community.
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>DBHDD Website</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfo}>
                  Jaronda Turner | Jaronda.Turner@dbhdd.ga.gov
                </Text>
                <Text style={styles.contactInfo}>(404) 232-1541</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button
            transparent
            onPress={() =>
              this.props.navigation.navigate("NewRegistrationScreen")
            }
          >
            <BackButton />
          </Button>
        }
      />
    );
  }
}
