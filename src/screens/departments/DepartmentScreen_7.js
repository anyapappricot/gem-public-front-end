import React, { Component } from "react";

import { View } from "react-native";

import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_7 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />
            <Text style={styleGlobal.explanation}>
              Bright from the Start administers the nationally recognized
              Georgia's Pre-K Program, licenses child care centers and
              home-based child care, administers federal nutrition programs, and
              manages voluntary quality enhancement programs. The criminal
              records section processes criminal background checks on directors
              of child care learning centers, family day care homes, group day
              care homes and informal providers. Providers and Directors are
              required to have a final fingerprint clearance letter prior to the
              Department issuing permission to operate the facility. Employees
              and adults age 18 and older living in the home or who have routine
              contact with the children in care are required to have background
              checks through the Georgia Crime Information Center (GCIC)
              obtained through the local law enforcement agency.
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>DECAL's Website</Text>
                <Text style={styles.website}>Fingerprinting Instructions</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>
                  2 Martin Luther King Jr. Dr. SE
                </Text>
                <Text style={styles.contactInfo}>754 East Tower</Text>
                <Text style={styles.contactInfo}>Atlanta, GA 30334</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfoTitleGrey}>
                  Criminal Recoeds Unit Helpdesk
                </Text>
                <Text style={styles.contactInfo}>CRC@decal.ga.gov</Text>
                <Text style={styles.contactInfo}>(855) 884-7444</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
