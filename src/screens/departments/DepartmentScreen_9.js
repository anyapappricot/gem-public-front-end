import React, { Component } from "react";

import { View } from "react-native";

import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_9 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              Mission: To foster the prosperity of our citizens and businesses
              by facilitating the safe transportation of people and products on
              Georgia's roads while safeguarding the integrity of our services
              and instilling values that demonstrate "We C.A.R.E"
            </Text>
            <Text style={styleGlobal.explanation}>"We C.A.R.E."</Text>
            <Text style={styleGlobal.explanation}>
              Communication - Provide information in a consistent, courteous
              manner.
            </Text>
            <Text style={styleGlobal.explanation}>
              Accountability - Empower employees and measure our performance.
            </Text>
            <Text style={styleGlobal.explanation}>
              Respect - Listen attentively and assist customers in a timely and
              professional manner.
            </Text>
            <Text style={styleGlobal.explanation}>
              Ethics - Demonstrate integrity and honesty in our actions and
              decisions.
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>http://www.dds.ga.gov/</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>2206 East View Parkway</Text>
                <Text style={styles.contactInfo}>P.O. Box 80447</Text>
                <Text style={styles.contactInfo}>Conyers, GA 30013</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfoTitleGrey}>Call Center</Text>
                <Text style={styles.contactInfo}>(678) 413-8745</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
