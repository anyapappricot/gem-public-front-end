import React, { Component } from "react";

import { View } from "react-native";

import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_10 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              Our agency's mission is to ensure professional competency among
              real estate licensees and appraisers and to promote a fair and
              honest market environment for practitioners and their customers
              and clients in real estate transactions in Georgia. The Georgia
              Real Estate Commission licenses real estate salespersons, brokers,
              community association managers, and firms. The Georgia Real Estate
              Appraisers Board licenses appraisers and appraisal management
              companies.
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>grec.state.ga.us</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>229 Peachtree St. NE</Text>
                <Text style={styles.contactInfo}>
                  International Tower Suite 1000
                </Text>
                <Text style={styles.contactInfo}>Atlanta, GA 30303</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfo}>(404) 656-3916</Text>
                <Text style={styles.contactInfo}>(404) 656-6650</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
