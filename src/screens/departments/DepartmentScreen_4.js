import React, { Component } from "react";

import { View } from "react-native";

import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_4 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              The Georgia Department of Public Health (DPH) is the lead agency
              in preventing disease, injury and disability; promoting health and
              well-being; and preparing for and responding to disasters from a
              health perspective. In 2011, the General Assembly restored DPH to
              its own state agency after more than 30 years of consolidation
              with other departments. At the state level, DPH functions through
              numerous divisions, sections, programs and offices.
            </Text>
            <Text style={styleGlobal.explanation}>
              Locally, DPH funds and collaborates with Georgia's 159 county
              health departments and 18 public health districts. Through the
              changes, the mission has remained constant; to protect the lives
              of all Georgians. Today, DPH's main functions include: Health
              Promotion and Disease Prevention, Maternal and Child Health,
              Infectious Disease and Immunization, Environmental Health,
              Epidemiology, Emergency Preparedness and Response, Emergency
              Medical Services, Pharmacy, Nursing, Volunteer Health Care, the
              Office of Health Equity, Vital Records, and the State Public
              Health Laboratory.
            </Text>
            <Text />
            <Text style={styleGlobal.explanation}>
              To register for a background check, please select one of the
              options below
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>DPH Homepage</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfoTitleGrey}>
                  Emergency Medical Services:
                </Text>
                <Text style={styles.contactInfo}>
                  Georgia Office of EMS and Trauma
                </Text>
                <Text style={styles.contactInfo}>
                  1680 Phoenix Blvd, Ste 200
                </Text>
                <Text style={styles.contactInfo}>
                  Atlanta, Georgia 30349-5576
                </Text>
                <Text />
                <Text style={styles.contactInfo}>Email EMS</Text>
                <Text style={styles.contactInfo}>(770) 996-3133</Text>
                <Text />
                <Text style={styles.contactInfoTitleGrey}>OIG</Text>
                <Text style={styles.contactInfo}>Stephanie Lockridge</Text>
                <Text style={styles.contactInfo}>(404) 232-7809</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
