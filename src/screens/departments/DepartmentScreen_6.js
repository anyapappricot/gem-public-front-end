import React, { Component } from "react";

import { View } from "react-native";

import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentSceen_6 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />
            <View>
              <Text style={styleGlobal.explanation}>
                The Georgia Department of Juvenile Justice is a multi-faceted
                agency that serves the state`s youthful offenders up to the age
                of 21. Each day, at 26 facilities and 92 court services offices
                through out the state, more than 4,000 DJJ employees work
                diligently to effect justice as well redirect and shape the
                young lives in the agency`s care so they can take responsibility
                for their delinquent conduct as well as become contributing
                members of society. At the same time, DJJ seeks to protect the
                victims of crimes so that they can rebuild their lives.
              </Text>
              <Text style={styleGlobal.explanation}>
                While holding youthful offenders accountable for their actions
                through probation supervision and secure detention, DJJ provides
                youth with medical and psychological treatment, as well as
                specialized programs designed to equip them with the social,
                intellectual and emotional tools they will need as adults. DJJ
                also places a premium on education. As Georgia`s 181st school
                district, we offer youth in our custody the opportunity to earn
                a high school diploma from a system accredited by both the
                Southern Association of Colleges and Schools (SACS) and the
                Correctional Education Association (CEA). Some 52,000 youths are
                served annually, including those who are placed on probation,
                sentenced to short-term incarceration, or committed to the
                Department`s custody by Juvenile Courts. Below, read more about
                our Mission, Vision, Values and Goals. Learn more about the
                Commissioner by reading his biography or his message.
              </Text>
              <ListOfSubGroups navigation={this.props.navigation} />
            </View>
            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>DJJ Homepage</Text>

                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>3408 Covington Hwy</Text>
                <Text style={styles.contactInfo}>Decatur, GA 30032r</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfoTitleGrey}>
                  Yewanda Kendricks
                </Text>
                <Text style={styles.contactInfo}>(404) 508-6568</Text>
                <Text style={styles.contactInfoTitleGrey}>Kathy Sidwell</Text>
                <Text style={styles.contactInfo}>(404) 508-5097</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
