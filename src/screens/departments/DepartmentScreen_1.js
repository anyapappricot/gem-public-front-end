import React, { Component } from "react";
import { Content, Button, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import styleGlobal from "../../assets/styles/global";
import DepartmentHeader from "../../../src/components/departments/DepartmentHeader";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_1 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              To register for a background check, please select one of the
              options below.
            </Text>
            <ListOfSubGroups navigation={this.props.navigation} />
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
