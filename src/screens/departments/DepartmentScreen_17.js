import React, { Component } from "react";
import { View } from "react-native";
import { Container, Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_17 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              An independent agency that supports the state's criminal justice
              system, Georgia Bureau of Investigation assists criminal
              investigations, provides forensic laboratory services, and updates
              computerized criminal justice information. The Mission of the
              Georgia Bureau of Investigation is to provide the highest quality
              investigative, scientific, and information services and resources
              to the criminal justice community and others as authorized by law,
              for the purpose of maintaining law and order and protecting life
              and property. The Mission will be achieved by a team of skilled
              and dedicated employees, utilizing innovative programs and state
              of the art technology.
            </Text>
            <Text style={styleGlobal.explanation}>
              To register for a background check, please select one of the
              options below
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>gbi.georgia.gov/</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>3121 Panthersville Rd.</Text>
                <Text style={styles.contactInfo}>Decatur, GA 30034</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfo}>(404) 244-2600</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
