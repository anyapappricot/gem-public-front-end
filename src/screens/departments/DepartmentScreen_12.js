import React, { Component } from "react";
import { View } from "react-native";
import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_12 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              The Georgia Vocational Rehabilitation Agency operates five
              integrated and interdependent statutory programs that share a
              primary goal - to help people with disabilities to become fully
              productive members of society by achieving independence and
              meaningful employment. The largest of the programs are Vocational
              Rehabilitation (VR) Program, Disability Adjudication Services, and
              the Roosevelt Warm Springs Institute for Rehabilitation.
            </Text>
            <Text style={styleGlobal.explanation}>
              Two other unique programs serve consumers with visual impairments:
              the Business Enterprise Program and Georgia Industries for the
              Blind. We are committed to the principle that people with
              disabilities are assets, and our efforts emphasize eliminating
              attitudinal barriers as well as physical ones.
            </Text>
            <Text style={styleGlobal.explanation}>
              We are a strategic partner in helping to build a diverse,
              qualified and productive workforce. Our aim is to pursue a
              comprehensive, coordinated employment agenda.
            </Text>
            <Text style={styleGlobal.explanation}>
              Our programs impact both employers and job seekers. Our bottom
              line is to tailor our services to fit your needs. And our promise
              to you is consistent professional service with responsive and
              responsible follow-up.
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>GVRA Website</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>
                  2 Peachtree St NE Suite #6
                </Text>
                <Text style={styles.contactInfo}>Atlanta, GA 30303</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfoTitleGrey}>Customer Care</Text>
                <Text style={styles.contactInfo}>(404) 232-1998</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
