import React, { Component } from "react";

import { Content, Button, Text } from "native-base";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_15 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              To register for a background check, please select one of the
              options below
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
