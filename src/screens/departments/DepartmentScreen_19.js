import React, { Component } from "react";

import { View } from "react-native";

import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_19 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              The Department of Community Supervision (DCS) was created by HB310
              in the 2015 Legislative Session. As part of the executive branch
              of Georgia’s government, the Department of Community Supervision
              (DCS) is responsible for the supervision of approximately 180,000
              adult felony offenders and Class A and Class B designated juvenile
              offenders. It is our mission to protect and serve all Georgia
              citizens through effective and efficient offender supervision in
              our communities, while providing opportunities for successful
              outcomes.
            </Text>
            <Text style={styleGlobal.explanation}>
              For the latest news and information regarding the Georgia
              Department of Community Supervision visit our official website at
              www.dcs.georgia.gov. You can also follow us on Facebook, Twitter,
              Google+, YouTube and Linkedin.
            </Text>
            <Text style={styleGlobal.explanation}>
              External stakeholders can still submit questions regarding the
              Department by emailing our webmaster.
            </Text>
            <Text style={styleGlobal.explanation}>
              To register for a background check, please select one of the
              options below
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>www.dcs.georgia.gov</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>
                  2 Martin Luther King, Jr. Drive SE
                </Text>
                <Text style={styles.contactInfo}>Atlanta, GA 30334</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfoSubTitle}>DCS Employment:</Text>
                <Text style={styles.contactInfoTitleGrey}>Tiffany Mosby</Text>
                <Text style={styles.contactInfo}>(404) 656-6144</Text>
                <Text />
                <Text style={styles.contactInfoSubTitle}>
                  Misdemeanor Probation Oversight Contact:
                </Text>
                <Text style={styles.contactInfoTitleGrey}>Aura Russell</Text>
                <Text style={styles.contactInfo}>(770) 634-8442</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
