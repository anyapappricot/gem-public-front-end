import React, { Component } from "react";
import { View } from "react-native";
import { Content, Button, Left, Text } from "native-base";

import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import ListOfSubGroups from "../../../src/components/ListOfSubGroups";

import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_11 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              To promote stronger families, the Georgia Department of Human
              Services investigates child and elderly abuse, facilitates
              adoption and foster parenting, accepts applications for Medicaid,
              assists single parents in collecting child support, helps senior
              citizens remain self-sufficient, and runs the Food Stamp,
              Temporary Assistance to Needy Families (TANF), and Childcare and
              Parent Services (CAPS) programs.
            </Text>
            <Text style={styleGlobal.explanation}>
              To register for a background check, please select one of the
              options below
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>DHS Website</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>2 Peachtree St. NW</Text>
                <Text style={styles.contactInfo}>Suite 30.493</Text>
                <Text style={styles.contactInfo}>Atlanta, GA 30303</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfoTitleGrey}>Emily Smith</Text>
                <Text style={styles.contactInfo}>Emily.Smith@dhs.ga.gov</Text>
                <Text style={styles.contactInfo}>(404) 463-0771</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
