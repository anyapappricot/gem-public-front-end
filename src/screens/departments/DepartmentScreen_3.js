import React, { Component } from "react";

import { View } from "react-native";

import { Content, Button, Left, Text } from "native-base";

import { Linking } from "react-native";
import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_3 extends Component {
  handleLinking = () => {
    Linking.openURL("https://www.dch.gchexs.ga.gov/Auth?ReturnUrl=%2f");
  };

  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <Text style={styleGlobal.explanation}>
              EFFECTIVE SEPTEMBER 28, 2017
            </Text>
            <Text style={styleGlobal.explanation}>
              Fingerprint-based criminal background check registrations for
              Personal Care Homes, Community Living Arrangements, Private Home
              Care Providers, Assisted Living Communities applicants and
              Voluntary Background Check Program participants must now register
              for the fingerprint-based criminal background check in the GCHEXS
              system before being fingerprinted through Cogent-GAPS.
            </Text>
            <Text style={styleGlobal.explanation}>
              Applicants must now start the registration for the
              fingerprint-based criminal background check in the GCHEXS system
              before being fingerprinted through Cogent- GAPS. Clink on the link
              below to log into the GCHEXS system.
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>
                  https://dch.georgia.gov/georgia-criminal-background-check-system-gchexse
                </Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>Two Peachtree St. NW</Text>
                <Text style={styles.contactInfo}>Atlanta, GA 30303</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfo}>
                  Chris Bennett | cbennett@dch.ga.gov
                </Text>
                <Text style={styles.contactInfo}>(404) 656-0464</Text>
                <Text />
                <Text style={styles.contactInfo}>
                  Denise Matthews | DMatthews@dch.ga.gov
                </Text>
                <Text style={styles.contactInfo}>(404) 463-0115</Text>
                <Text />
                <Text style={styles.contactInfo}>
                  Sakinah Johnson | sakinah.johnson@dch.ga.gov
                </Text>
                <Text style={styles.contactInfo}>(404) 463-7154</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
