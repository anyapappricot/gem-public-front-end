import React, { Component } from "react";

import { View } from "react-native";

import { Content, Button, Left, Text } from "native-base";

import ListOfSubGroups from "../../../src/components/ListOfSubGroups";
import BodyWrapper from "../../../src/components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import styles from "../../assets/styles/departments";
import styleGlobal from "../../assets/styles/global";
import BackButton from "../../components/BackButton";

export default class DepartmentScreen_5 extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />
            <Text style={styleGlobal.explanation}>
              The Office of Insurance and Safety Fire Commissioner licenses and
              regulates insurance companies; ensures that insurance rates, rules
              and forms comply with state law; investigates suspicions of
              insurance fraud; and conducts inspections of buildings and houses
              to prevent fire outbreak. Our vision is to have a well-trained
              workforce, utilizing state of the art technology to facilitate
              regulation, coordination and uniformity among state regulators and
              provide public access to services and fire safety information that
              results in a consumer friendly and competitive market place.
            </Text>

            <ListOfSubGroups navigation={this.props.navigation} />

            <View style={styles.contacts}>
              <Left>
                <Text style={styles.contactInfoTitle}>Website</Text>
                <Text style={styles.website}>
                  www.oci.ga.gov/Agents/Home.aspx
                </Text>
                <Text style={styles.contactInfoTitleGrey}>GAPS</Text>
                <Text style={styles.website}>
                  How To Be Fingerprinted for the Office of Insurance and Safety
                  Fire Commissioner
                </Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Address</Text>
                <Text style={styles.contactInfo}>
                  2 Martin Luther King Jr. Dr. SE
                </Text>
                <Text style={styles.contactInfo}>754 East Tower</Text>
                <Text style={styles.contactInfo}>Atlanta, GA 30334</Text>
                <Text />
                <Text style={styles.contactInfoTitle}>Contact</Text>
                <Text style={styles.contactInfoTitleGrey}>Tammy Clemens</Text>
                <Text style={styles.contactInfo}>tclemens@oci.ga.gov</Text>
                <Text style={styles.contactInfo}>(404) 656-2101</Text>
                <Text style={styles.contactInfoTitleGrey}>Michael Carr</Text>
                <Text style={styles.contactInfo}> mcarr@oci.ga.gov</Text>
                <Text style={styles.contactInfo}>(404) 656-2101</Text>
              </Left>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}
