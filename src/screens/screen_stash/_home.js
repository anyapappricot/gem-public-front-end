import React, { Component } from "react";

import {
  View,
  Platform,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  Dimensions
} from "react-native";

import { Container, Content, Text } from "native-base";
import Image from "react-native-remote-svg";
import ScreenHeader from "../../src/components/ScreenHeader";
import sortByDistance from "sort-by-distance";
import Barcode from "react-native-barcode-builder";

const width = Dimensions.get("window").width;

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      barcode: "GA18CLA54542306/t01011990"
    };
  }

  // componentWillMount() {
  //   BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  // }

  // componentWillUnmount() {
  //   BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  // }

  render() {
    return (
      <Container style={style.wholeScreen}>
        <ScreenHeader navigation={this.props.navigation} />
        <Content
          contentContainerStyle={{
            // NEW STYLE
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
            // OLD STYLE
            // flex: 1,
            // justifyContent: "center",
            // alignItems: "center"
          }}
        >
          {/* NEW IMAGE */}
          <Image
            source={require("../assets/home_screen/LogoMap.svg")}
            style={style.welcomeToThe}
          />

          {/* OLD IMAGE */}
          {/* <Image
            source={require("../assets/home_screen/Welcome.png")}
            style={style.welcomeToThe}
          /> */}

          <View style={style.groupOfButtons}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("NewRegistrationScreen");
              }}
            >
              <Image
                source={require("../assets/home_screen/Button1.png")}
                style={style.button1}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("LogInToViewStatus")
              }
            >
              <Image
                source={require("../assets/home_screen/Button2.png")}
                style={style.button2}
              />
            </TouchableOpacity>
          </View>

          <View style={style.footer}>
            <Image
              source={require("../assets/home_screen/new_gemalto.png")}
              style={style.byGemalto}
            />
            <View>
              <Text style={style.copyright}>
                © 2018 Copyright Gemalto , Inc. All rights reserved.
              </Text>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const style = StyleSheet.create({
  wholeScreen: {
    // backgroundColor: "#f5f5f5"
  },
  headerStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "auto",
    height: 90,
    backgroundColor: "#3e4649",
    shadowRadius: 6,
    shadowOpacity: 1,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#707070"
  },
  welcomeToThe: {
    flexDirection: "row",
    alignItems: "center",
    width: 300,
    height: 232,
    marginTop: Platform.OS === "ios" ? 20 : 70
  },
  groupOfButtons: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginTop: Platform.OS === "ios" ? 25 : 0,
    marginBottom: Platform.OS === "ios" ? 25 : 45
  },
  button1: {
    // width: Platform.OS === "ios" ? 131 : 131,
    // height: Platform.OS === "ios" ? 131 : 131,
    // width: Platform.OS === "ios" ? 131 : 100,
    // height: Platform.OS === "ios" ? 131 : 100,
    width: width < 361 ? 100 : 131,
    height: width < 361 ? 100 : 131,
    marginLeft: 5,
    marginRight: 7,
    borderColor: "#2A50A6",
    borderWidth: 0.5
  },
  button2: {
    // width: Platform.OS === "ios" ? 131 : 100,
    // height: Platform.OS === "ios" ? 131 : 100,
    width: width < 361 ? 100 : 131,
    height: width < 361 ? 100 : 131,
    marginLeft: 7,
    marginRight: 5,
    borderColor: "#2A50A6",
    borderWidth: 0.5
  },
  footer: {
    alignItems: "center",
    marginBottom: 10
  },
  byGemalto: {
    alignItems: "center"
  },
  copyright: {
    fontFamily: "Montserrat-Light",
    fontSize: 8,
    alignItems: "center"
  }
});

export default HomeScreen;
