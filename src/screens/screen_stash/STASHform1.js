import React, { Component } from "react";
import { View, Dimensions, Animated } from "react-native";

import Image from "react-native-remote-svg";
import {
  Content,
  Button,
  CheckBox,
  Text,
  Form,
  Row,
  CardSwiper
} from "native-base";

import { withNavigation } from "react-navigation";

import { Field, reduxForm, formValueSelector } from "redux-form";
import { connect } from "react-redux";

import {
  fetchReasons,
  getRegistrationID,
  saveCheckBox
} from "../../redux/actions/actions";

import {
  required,
  minLength1,
  required_ga
} from "../../validation/validations";

import BodyWrapper from "../../components/BodyWrapper";
import DepartmentHeader from "../../components/departments/DepartmentHeader";
import SmartInput from "../../components/SmartInput";
import SmartButton from "../../components/SmartButton";
import BackButton from "../../components/BackButton";
import styles from "../../assets/styles/forms";
const width = Dimensions.get("window").width;
import fake_post_data from "../../assets/json/fake_post_data";

class FormScreen_1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boxTicked: false,
      paymentType: null,
      fadeAnim: new Animated.Value(0) // Initial value for opacity: 0
    };
    this.setPaymentType = this.setPaymentType.bind(this);
    this.toggleTick = this.toggleTick.bind(this);
    this.toggleImage = this.toggleImage.bind(this);
  }

  _fade_in_view = () => {
    Animated.timing(
      // Animate over time
      this.state.fadeAnim, // The animated value to drive
      {
        toValue: 1, // Animate to opacity: 1 (opaque)
        duration: 1000 // Make it take a while
      }
    ).start(); // Starts the animation
  };

  _fade_out_view = () => {
    Animated.timing(
      // Animate over time
      this.state.fadeAnim, // The animated value to drive
      {
        toValue: 0, // Animate to opacity: 1 (opaque)
        duration: 1000 // Make it take a while
      }
    ).start(); // Startszz the animation
  };

  setPaymentType = paymentType => {
    this.setState({
      paymentType
    });
  };

  toggleTick = async () => {
    await this.setState({
      boxTicked: !this.state.boxTicked
    });

    this.props.dispatch(saveCheckBox(this.state.boxTicked ? "Y" : "N"));
  };

  toggleImage() {
    const ticked = require("../../assets/departments/checked.png");
    const notTicked = require("../../assets/departments/uncheck.png");
    return this.state.boxTicked ? ticked : notTicked;
  }

  submit = () => {
    this.props.navigation.navigate("FormScreen_2");
  };

  renderReasons() {
    const grp_id = this.props.departments[this.props.departmentIndex].grp_id;
    const subgrp_id = this.props.UserInput.subgroup.subgrp_id;
    const ori = this.props.reviewing_agency_id;

    this.props.dispatch(fetchReasons(grp_id, subgrp_id, ori));
    this.props.dispatch(getRegistrationID());
  }

  renderReviewingAgencyORIField = group => {
    const { side_bar_switch } = this.state;
    if (group.subgroup.agency.length === 0 && group.subgroup.ori === null) {
      // CASE 1: Subgroup with no list of agencies, with NO ORI associated to it
      // court service > superior court
      return (
        <Field
          label="Reviewing Agency ID *"
          name="reviewAgencyID"
          component={SmartInput}
          type={"text"} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
          auto_fill={side_bar_switch ? "GA044015Y" : null}
          validate={[required, minLength1, required_ga]}
        />
      );
    }

    if (group.subgroup.agency.length === 0 && group.subgroup.ori !== null) {
      // CASE 2: Subgroup with no list of agencies, with ONE ORI associated to it
      // court service > supreme court
      return (
        <Field
          label="Reviewing Agency ID *"
          name="reviewAgencyID"
          component={SmartInput}
          type={"text"} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
          auto_fill={side_bar_switch ? "GA044015Y" : null}
          validate={[required, minLength1, required_ga]}
        />
      );
    }
    if (group.subgroup.agency.length > 1 && group.subgroup.ori === null) {
      // CASE 3: There are a list of agencies associated with the subgroup
      // education > private school
      // if (group.subgroup.grp_id === "PU")
      //   return (
      //     <Field
      //       type={"ori"}
      //       label="Reviewing Agency ID *"
      //       name="reviewAgencyID"
      //       component={SmartInput} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
      //       auto_fill={side_bar_switch ? "GA044015Y" : null}
      //       validate={[required, minLength1, required_ga]}
      //     />
      //   );
      // else {
      return (
        <Field
          type={"ori"}
          label="Reviewing Agency ID *"
          name="reviewAgencyID"
          component={SmartInput} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
          auto_fill={side_bar_switch ? "GA044015Y" : null}
          validate={[required, minLength1, required_ga]}
        />
      );
      // }
    }
    if (group.subgroup.agency.length === 1 && group.subgroup.ori === null) {
      // CASE 4: There is only ONE agency associated with the subgroup
      // education > private school
      return (
        <Field
          label="Reviewing Agency ID *"
          name="reviewAgencyID"
          component={SmartInput}
          type={"text"} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
          auto_fill={this.props.side_bar_switch ? "GA044015Y" : null}
          validate={[required, minLength1, required_ga]}
        />
      );
    }

    // if (group.subgroup.name === "MEDICAID HIGH RISK PROVIDERS") {
    //   // CASE 6: DCH
    //   // I COULD ADD
    //   // || group.subgroup.name === "GCHEXS REGISTRATIONS"
    //   // ASK TIFF

    //   // THIS COULD ALSO BE NESTED IN CASE 2
    //   return (
    //     <Field
    //       label="Reviewing Agency ID *"
    //       name="reviewAgencyID"
    //       component={SmartInput}
    //       type={"text"} // if agency length = 0 => populate with subgroup ori, if length > 0 drop down, if blank =>  null
    //       validate={[required, minLength1]}
    //       grp_id={this.props.departments[this.props.departmentIndex].grp_id} // has to be 'DCH'
    //     />
    //   );
    // }
  };

  renderPayment = side_bar_switch => {
    return (
      <Field
        type={"payment"}
        label="Payment *"
        name="payment"
        component={SmartInput}
        setPaymentType={this.setPaymentType}
        auto_fill={side_bar_switch ? "Credit Card" : null}
        validate={side_bar_switch ? null : [required, minLength1]}
      />
    );

    //   if (
    //     this.props.DECAL_reasons === "DECAL- Daycare Director/ Employee" ||
    //     this.props.DECAL_reasons ===
    //       "TCSG - Technical College Student Providing Direct Child Care"
    //   ) {
    //     return (
    //       <Field
    //         type={"text"}
    //         label="Payment *"
    //         name="decal_payment"
    //         component={SmartInput}
    //         setPaymentType={this.setPaymentType}
    //         auto_fill={side_bar_switch ? "Agency" : null}
    //         validate={side_bar_switch ? null : [required, minLength1]}
    //         disabledField={{ disabled: true }}
    //       />
    //     );
    //   } else {
    //     return (
    //       <Field
    //         type={"payment"}
    //         label="Payment *"
    //         name="payment"
    //         component={SmartInput}
    //         setPaymentType={this.setPaymentType}
    //         auto_fill={side_bar_switch ? "Credit Card" : null}
    //         validate={side_bar_switch ? null : [required, minLength1]}
    //       />
    //     );
    //   }
  };

  render() {
    const { fadeAnim } = this.state;
    const {
      handleSubmit,
      reset,
      reviewing_agency_id,
      side_bar_switch
    } = this.props;

    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <DepartmentHeader />

            <View>
              {/* <View style={{ padding: 10 }}> */}
              <Text style={styles.newRegistrationStyle}>
                New Registration: Please Enter Your Information
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                padding: 20
              }}
            >
              <Image
                style={styles.progressBar}
                // style={{
                //   width: 355 / 1.25,
                //   height: 40 / 1.25
                // }}
                source={require("../../assets/forms/form_progress_1.svg")}
              />
            </View>

            <View style={styles.sectionStyle}>
              <Text style={styles.sectionTextStyle}>
                Transaction Information
              </Text>
              <Text style={styles.blue_line} />
            </View>

            <Form>
              {this.renderReviewingAgencyORIField(this.props.UserInput)}

              {reviewing_agency_id !== undefined ? (
                reviewing_agency_id[0] === "G" &&
                reviewing_agency_id[1] === "A" ? (
                  // && reviewing_agency_id.length === 9
                  <Field
                    label="Reasons *"
                    name="reason"
                    component={SmartInput}
                    type={"reason"}
                    auto_fill={side_bar_switch ? "Insurance Licensees" : null}
                    validate={side_bar_switch ? null : [required, minLength1]}
                  >
                    {this.renderReasons()}
                  </Field>
                ) : null
              ) : null}

              <Field
                label="Position applied for"
                name="positionAppliedFor"
                component={SmartInput}
                type={"text"}
                auto_fill={side_bar_switch ? "Teacher" : null}
              />
              {this.renderPayment(side_bar_switch)}

              {this.state.paymentType === "Agency" ||
              this.props.DECAL_reasons ===
                "DECAL- Employment for State/Contracted Employees of DECAL Only" ? (
                <View>
                  <Field
                    label="Requesting Agency ID"
                    name="requestAgencyID"
                    placeholder="If different from Reviewing Agency ID"
                    component={SmartInput}
                    type={"text"}
                    auto_fill={side_bar_switch ? "GAP234655" : null}
                    validate={
                      this.state.paymentType === "Agency"
                        ? [required, minLength1]
                        : []
                    }
                  />

                  <Field
                    label="Billing Code"
                    name="billingCode"
                    component={SmartInput}
                    type={"text"} // create one more userinput
                    auto_fill={side_bar_switch ? "R1023C1608" : null}
                    placeholderText="(Mandatory if payment is Agency)"
                    validate={required}
                  />
                  <Field
                    label="Billing Password"
                    name="billingPassword"
                    component={SmartInput}
                    type={"text"}
                    auto_fill={side_bar_switch ? "R1023C1608" : null}
                    placeholderText="(Mandatory if payment is Agency)"
                    validate={required}
                  />
                </View>
              ) : null}
            </Form>

            <View
              style={{
                flex: 1,
                backgroundColor: "#ffffff",
                alignItems: "center",
                justifyContent: "center",
                margin: 20
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <CheckBox
                  checked={this.state.boxTicked}
                  style={styles.tickBoxAndroid}
                  boxTicked={this.state.boxTicked}
                  onPress={() => this.toggleTick()}
                />
                <Text style={styles.tickText}>Fingerprint Card User</Text>
              </View>
            </View>

            {/* <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                padding: 20
              }}
              button
            >
              {Platform.OS === "android" ? (
                <CheckBox
                  checked={this.state.boxTicked}
                  style={styles.tickBoxAndroid}
                  boxTicked={this.state.boxTicked}
                  onPress={() => {
                    this.toggleTick();
                  }}
                />
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    this.toggleTick();
                  }}
                >
                  <Image
                    style={styles.tickBoxiOS}
                    boxTicked={this.state.boxTicked}
                    source={this.toggleImage()}
                  />
                </TouchableOpacity>
              )}
              <Text style={styles.tickText}>Fingerprint Card User</Text>
            </View> */}
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                paddingLeft: 40,
                paddingRight: 40
                // padding: 25
                // backgroundColor: "red"
              }}
            >
              <Text style={styles.checkBoxText}>
                By checking this box, you are agreeing to submit ink cards to
                Gemalto Cogent. see here for details
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                marginTop: 25,
                marginBottom: 50
              }}
            >
              <View>
                <SmartButton
                  buttonBorderStyle={{ width: width / 3 }}
                  text={"Reset"}
                  onPress={reset}
                />
              </View>
              <View>
                <SmartButton
                  buttonBorderStyle={{ width: width / 3 }}
                  text={"Continue"}
                  onPress={handleSubmit(this.submit)}
                />
              </View>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  initialValues: {
    reviewAgencyID: state.userInput.formData.subgroup.ori
      ? state.userInput.formData.subgroup.ori
      : // ^^^ IF SUBGROUP HAS ORI - NO AGENCIES >  USE IT -> ^^^
      state.userInput.formData.subgroup.agency.length
      ? // ^^^ ELSE IF
        state.userInput.formData.subgroup.agency.length === 1
        ? state.userInput.formData.subgroup.agency[0].ori
        : // ^^^ IF ONLY ONE ITEM IN AGENCIES ARRAY -> USE IT ^^^
          `` // << ELSE LEAVE IT BLANK
      : `Please enter a valid ORI`,
    // IF NO SUBGROUP ORI NOR AGENCIES -> ENTER AN ORI
    decal_payment: "Agency"
  },
  reason: state.userInput.reasons,
  departments: state.userInput.datas,
  departmentIndex: state.userInput.departmentIndex,
  UserInput: state.userInput.formData,
  checkBox: state.userInput.checkBox,

  grp_id: state.userInput.grp_id,
  subgrp_id: state.userInput.subgrp_id,
  ori: state.userInput.ori,
  // test: state.userInput.test,
  side_bar_switch: state.dev_reducer.options.side_bar_switch
});
//
// connect the redux form
// s1: a unique identifier for this form
// bootstrap navigation
const notConnected = reduxForm({ form: "s1" })(withNavigation(FormScreen_1));
const selector = formValueSelector("s1");

const connected = connect(state => {
  const reviewing_agency_id = selector(state, "reviewAgencyID");
  const DECAL_reasons = selector(state, "reason");
  return {
    reviewing_agency_id,
    DECAL_reasons
  };
})(notConnected);

// map the redux' state to the components props
export default connect(mapStateToProps)(connected);
