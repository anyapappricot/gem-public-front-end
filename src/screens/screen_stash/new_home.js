import React, { Component } from "react";

import {
  View,
  Platform,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  Dimensions
} from "react-native";

import { Container, Content, Text } from "native-base";
import Image from "react-native-remote-svg";
import ScreenHeader from "../../components/ScreenHeader";
import SmartButton from "../../components/SmartButton";
import Barcode from "react-native-barcode-builder";
import sortByDistance from "sort-by-distance";

const width = Dimensions.get("window").width;

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      barcode: "GA18CLA54542306/t01011990"
    };
  }
  // componentWillMount() {
  //   BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  // }

  // componentWillUnmount() {
  //   BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  // }

  render() {
    return (
      <Container>
        <ScreenHeader navigation={this.props.navigation} />
        <Content
          contentContainerStyle={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <View
            style={{
              flex: width < 375 ? 1 : 2,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            {/* NEW IMAGE */}
            <Image
              source={require("../assets/home_screen/LogoMap.svg")}
              style={style.image_style}
            />

            {/* OLD IMAGE */}
            {/* 
            <Image
            source={require("../assets/home_screen/Welcome.png")}
            style={style.image_style}
             /> 
             */}

            <Text>{this.state.reg_id}</Text>
          </View>
          {/* <Barcode value={this.state.barcode} format="CODE128" /> */}

          <View style={style.groupOfButtons}>
            <SmartButton
              buttonBorderStyle={{ width: width / 1.2 }}
              text={"New Registration"}
              onPress={() => {
                this.props.navigation.navigate("NewRegistrationScreen");
              }}
              image={
                <Image
                  // source={require("../assets/home_screen/fingerprint.svg")}
                  source={require("../assets/home_screen/fingerprint_white.svg")}
                  style={style.smart_button_svg}
                />
              }
            />
            <SmartButton
              buttonBorderStyle={{ width: width / 1.2 }}
              onPress={() => {
                this.props.navigation.navigate("LogInToViewStatus");
              }}
              text={"View Registration Status"}
              image={
                <Image
                  // source={require("../assets/home_screen/search.svg")}
                  source={require("../assets/home_screen/search_white.svg")}
                  style={style.smart_button_svg}
                />
              }
            />
          </View>

          <View style={style.footer}>
            <Image
              source={require("../assets/home_screen/new_gemalto.png")}
              style={style.byGemalto}
            />
            <View>
              <Text style={style.copyright}>
                © 2018 Copyright Gemalto , Inc. All rights reserved.
              </Text>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
const image_width =
  width < 375 ? width / 1.5 : width > 700 ? width / 2 : width / 1.5;

const style = StyleSheet.create({
  smart_button_svg: {
    height: width <= 375 ? 35 : 50,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.1,
    shadowRadius: 5
  },
  image_style: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: image_width * 1.29310344828,
    height: image_width
  },
  groupOfButtons: {
    flex: 1,
    // flexDirection: "column",
    justifyContent: "space-evenly",
    alignItems: "center"
    // backgroundColor: "red"
    // marginBottom: 50
    // marginBottom: Platform.OS === "ios" ? 25 : 45
  },
  footer: {
    alignItems: "center",
    marginBottom: 10
  },
  byGemalto: {
    alignItems: "center"
  },
  copyright: {
    fontFamily: "Montserrat-Light",
    fontSize: 8,
    alignItems: "center"
  }
});

export default HomeScreen;
