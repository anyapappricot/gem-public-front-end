import React, { PureComponent } from "react";

import { Provider } from "react-redux";
import { createDrawerNavigator, DrawerItems } from "react-navigation";

import {
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  Image,
  Platform
} from "react-native";

import Home from "../stacks/HomeStack";
import NewRegistration from "../stacks/NewRegistrationStack";
import FAQs from "../stacks/FAQStack";
import ViewRegistrationStatus from "../stacks/ViewRegistrationStatusStack";
import FingerprintSiteLocations from "../stacks/FingerprintSiteLocationsStack";
import About from "../stacks/AboutStack";
import SmartSwitch from "../../src/components/SmartSwitch";
import store from "../../src/redux/store";

export default class App extends PureComponent {
  render() {
    return (
      <Provider store={store}>
        <AppDrawerNavigator />
      </Provider>
    );
  }
}

const CustomeDrawerComponent = props => {
  const { items, ...rest } = props;
  const filteredItems = items.filter(item => item.key !== "AppNavigator");
  return (
    <SafeAreaView style={styles.drawer_nav}>
      <ScrollView>
        <DrawerItems
          style={{ marginTop: Platform.OS === "ios" ? 0 : 200 }}
          items={filteredItems}
          {...rest}
        />
      </ScrollView>
      <View style={styles.smart_switch_container}>
        <SmartSwitch left_text={"Test Data"} right_text={"Real Data"} />
      </View>
      <View style={styles.view}>
        <Image
          source={require("../assets/home_screen/logo-gemalto-white.png")}
          style={styles.logo}
        />
      </View>
    </SafeAreaView>
  );
};

const AppDrawerNavigator = createDrawerNavigator(
  {
    Home,
    "New Registration": NewRegistration,
    "View Registration Status": ViewRegistrationStatus,
    "Fingerprint Site Locations": FingerprintSiteLocations,
    FAQs,
    About
  },
  {
    contentComponent: CustomeDrawerComponent,
    contentOptions: {
      inactiveTintColor: "#ffffff",
      itemsContainerStyle: {
        marginVertical: 0
      },
      iconContainerStyle: {
        opacity: 1
      }
    },
    initialRouteName: "Home"
  }
);

const styles = StyleSheet.create({
  drawer_nav: {
    flex: 1,
    backgroundColor: "#3e4649",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#707070"
  },
  view: {
    alignItems: "center",
    justifyContent: "center"
  },
  logo: {
    width: 81,
    height: 27,
    marginBottom: 20
  },
  smart_switch_container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  }
});
