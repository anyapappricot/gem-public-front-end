import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Platform,
  Dimensions
} from "react-native";
import { Content, Button, Text, CheckBox } from "native-base";
import { withNavigation } from "react-navigation";
import DepartmentHeader from "../components/departments/DepartmentHeader";
import SmartButton from "../components/SmartButton";
import BodyWrapper from "../components/BodyWrapper";
import BackButton from "../components/BackButton";
import SmartCheckBox from "../components/SmartCheckBox";

import styleGlobal from "../assets/styles/global";

const width = Dimensions.get("window").width;

class PreRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boxTicked: false
    };
  }

  toggleTick = () => {
    this.setState({
      boxTicked: !this.state.boxTicked
    });
  };

  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content contentContainerStyle={style.contentContainerStyle}>
            <DepartmentHeader />

            <Text style={style.subTitle}>
              Non-Criminal Justice Applicant's Privacy Rights
            </Text>
            <View style={style.textBox}>
              <Text style={styleGlobal.explanation}>
                As an applicant that is the subject of a Georgia only or a
                Georgia and Federal Bureau of Investigation (FBI) national
                fingerprint/biometric-based criminal history record check for a
                non-criminal justice purpose (such as an application for a job
                or license, immigration or naturalization, security clearance,
                or adoption), you have certain rights which are discussed below.{" "}
              </Text>

              <Text style={styleGlobal.explanation}>
                • You must be provided written notification that your
                fingerprints/biometrics will be used to check the criminal
                history records maintained by the Georgia Crime Information
                Center (GCIC) and the FBI, when a federal record check is so
                authorized.
              </Text>
              <Text style={styleGlobal.explanation}>
                • If your fingerprints/biometrics are used to conduct a FBI
                national criminal history check, you are provided a copy of the
                Privacy Act Statement that would normally appear on the FBI
                fingerprint card.
              </Text>
              <Text style={styleGlobal.explanation}>
                • The agency must advise you of the procedures for changing,
                correcting, or updating your criminal history record as set
                forth in Title 28, Code of Federal Regulations (CFR), Section
                16.34.{" "}
              </Text>
              <Text style={styleGlobal.explanation}>
                • If you have a Georgia or FBI criminal history record, you
                should be afforded a reasonable amount of time to correct or
                complete the record (or decline to do so) before the agency
                denies you the job, license or other benefit based on
                information in the criminal history record.
              </Text>
              <Text style={styleGlobal.explanation}>
                • In the event an adverse employment or licensing decision is
                made, you must be informed of all information pertinent to that
                decision to include the contents of the record and the effect
                the record had upon the decision. Failure to provide all such
                information to the person subject to the adverse decision shall
                be a misdemeanor [O.C.G.A.§35-3-34(b) and §35-3- 35(b)].
              </Text>
              <Text style={styleGlobal.explanation}>
                You have the right to expect the agency receiving the results of
                the criminal history record check will use it only for
                authorized purposes and will not retain or disseminate it in
                violation of state and/or federal statute, regulation or
                executive order, or rule, procedure or standard established by
                the National Crime Prevention and Privacy Compact Council.
              </Text>
              <Text style={styleGlobal.explanation}>
                If the employment/licensing agency policy permits, the agency
                may provide you with a copy of your Georgia or FBI criminal
                history record for review and possible challenge. If agency
                policy does not permit it to provide you a copy of the record,
                information regarding how to obtain a copy of your Georgia, FBI
                or other state criminal history may be obtained at the GBI
                website
                (http://gbi.georgia.gov/obtaining-criminal-history-record-information).
              </Text>
              <Text style={styleGlobal.explanation}>
                If you decide to challenge the accuracy or completeness of your
                Georgia or FBI criminal history record, you should send your
                challenge to the agency that contributed the questioned
                information. Alternatively, you may send your challenge directly
                to GCIC provided the disputed arrest occurred in Georgia.
                Instructions to dispute the accuracy of your criminal history
                can be obtained at the GBI website
                (http://gbi.georgia.gov/obtaining-criminal-history-record-information).
              </Text>
            </View>
            <Text style={style.subTitle}>Privacy Act Statement</Text>
            <View style={style.textBox}>
              <Text style={styleGlobal.explanation}>
                Authority: The FBI's acquisition, preservation, and exchange of
                fingerprints and associated information is generally authorized
                under 28 U.S.C. 534. Depending on the nature of your
                application, supplemental authorities include Federal statutes,
                State statutes pursuant to Pub. L. 92-544, Presidential
                Executive Orders, and federal regulations. Providing your
                fingerprints and associated information is voluntary; however,
                failure to do so may affect completion or approval of your
                application.{" "}
              </Text>
              <Text style={styleGlobal.explanation}>
                Principal Purpose: Certain determinations, such as employment,
                licensing, and security clearances, may be predicated on
                fingerprint-based background checks. Your fingerprints and
                associated information/biometrics may be provided to the
                employing, investigating, or otherwise responsible agency,
                and/or the FBI for the purpose of comparing your fingerprints to
                other fingerprints in the FBI's Next Generation Identification
                (NGI) system or its successor systems (including civil,
                criminal, and latent fingerprint repositories) or other
                available records of the employing, investigating, or otherwise
                responsible agency. The FBI may retain your fingerprints and
                associated information/biometrics in NGI after the completion of
                this application and, while retained, your fingerprints may
                continue to be compared against other fingerprints submitted to
                or retained by NGI.{" "}
              </Text>
              <Text style={styleGlobal.explanation}>
                Routine Uses: During the processing of this application and for
                as long thereafter as your fingerprints and associated
                information/biometrics are retained in NGI, your information may
                be disclosed pursuant to your consent, and may be disclosed
                without your consent as permitted by the Privacy Act of 1974 and
                all applicable Routine Uses as may be published at any time in
                the Federal Register, including the Routine Uses for the NGI
                system and the FBI's Blanket Routine Uses. Routine uses include,
                but are not limited to, disclosures to: employing, governmental
                or authorized non-governmental agencies responsible for
                employment, contracting, licensing, security clearances, and
                other suitability determinations; local, state, tribal, or
                federal law enforcement agencies; criminal justice agencies; and
                agencies responsible for national security or public safety.
              </Text>
            </View>

            <SmartCheckBox
              checked={this.state.boxTicked}
              onPress={() => this.toggleTick()}
              text={"I have read and accept these terms"}
            />

            <View
              style={{
                padding: 20,
                alignSelf: "stretch"
              }}
            >
              <SmartButton
                disabled={this.state.boxTicked ? false : true}
                style={!this.state.boxTicked ? style.disabled : null}
                text={"Continue"}
                onPress={() => this.props.navigation.navigate("FormScreen_1")}
              />
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button transparent onPress={() => this.props.navigation.goBack()}>
            <BackButton />
          </Button>
        }
      />
    );
  }
}

export default withNavigation(PreRegistration);

const style = StyleSheet.create({
  contentContainerStyle: {
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "center",
    flexGrow: 1
  },
  disabled: {
    backgroundColor: "rgba(220,220,220, 0.4)"
  },
  subTitle: {
    fontFamily: "OpenSans-Semibold",
    fontWeight: "400",
    fontSize: 12,
    textAlign: "center",
    top: 15
  },
  textBox: {
    width: width - 40,
    marginTop: 35,
    marginBottom: 40,
    paddingBottom: 20,
    borderRadius: 3,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#c7c7c7"
  }
});
