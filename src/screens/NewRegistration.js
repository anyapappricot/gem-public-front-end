import React, { Component } from "react";

import { View, StyleSheet, ActivityIndicator, Dimensions } from "react-native";
import { Content, Button, Text } from "native-base";

import Image from "react-native-remote-svg";
import Modal from "react-native-modal";
import BodyWrapper from "../components/BodyWrapper";
import SmartButton from "../components/SmartButton";
import SmartCard from "../components/SmartCard";
import BackButton from "../components/BackButton";

import { connect } from "react-redux";
import { fetchDepartments, setDepartmentIndex } from "../redux/actions/actions";

import styles from "../assets/styles/newRegistration";
import department_data from "../assets/json/departments";

const width = Dimensions.get("window").width;
class NewRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false
    };
  }

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  renderModal = () => {
    return (
      <Modal
        backdropColor={"rgba(255, 255, 255, 1)"}
        isVisible={this.state.isModalVisible}
      >
        <View style={styles.modalWrapper}>
          <Text style={styles.modalHeaderText}>Oops!</Text>
          <Text style={styles.modalText}>The has been an error..</Text>
          <SmartButton
            text={"Let's start over.."}
            onPress={this._go_to_home_screen}
          />
        </View>
      </Modal>
    );
  };

  _go_to_home_screen = () => {
    this.state.isModalVisible ? this._toggleModal() : null;
    this.props.navigation.navigate("HomeScreen");
  };

  componentDidMount() {
    !this.props.departments.length
      ? this.props.dispatch(fetchDepartments())
      : // null
        this.props.dispatch(fetchDepartments());

    setTimeout(() => {
      if (this.props.departments.length === 0) {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      }
    }, Math.floor(10000));
  }

  render() {
    const { navigation } = this.props;
    const { ...props } = this.props;

    return this.props.departments.length > 0 && !this.state.isModalVisible ? (
      <BodyWrapper
        {...props}
        body={
          <Content>
            <Text style={styles.screenTitle}>New Registration</Text>
            <Text style={styles.line} />

            <Text style={styles.explanation}>
              To register for a background check, please select one of the
              options below
            </Text>

            <View>
              {this.renderModal()}
              {this.props.departments ? (
                <View>
                  {this.props.departments.map((item, index) => (
                    <SmartCard
                      key={item.id}
                      onPress={() => {
                        props.dispatch(
                          setDepartmentIndex("departmentIndex", index)
                        );

                        switch (item.grp_id) {
                          case "CS":
                            navigation.navigate("DepartmentScreen_1");
                            break;
                          case "DBHDD":
                            navigation.navigate("DepartmentScreen_2");
                            break;
                          case "DCH":
                            navigation.navigate("DepartmentScreen_3");
                            break;
                          case "DPH":
                            navigation.navigate("DepartmentScreen_4");
                            break;
                          case "OIC":
                            navigation.navigate("DepartmentScreen_5");
                            break;
                          case "DJJ":
                            navigation.navigate("DepartmentScreen_6");
                            break;
                          case "DECAL":
                            navigation.navigate("DepartmentScreen_7");
                            break;
                          case "SOS":
                            navigation.navigate("DepartmentScreen_8");
                            break;
                          case "DDS":
                            navigation.navigate("DepartmentScreen_9");
                            break;
                          case "RECAB":
                            navigation.navigate("DepartmentScreen_10");
                            break;
                          case "DHS":
                            navigation.navigate("DepartmentScreen_11");
                            break;
                          case "GVRA":
                            navigation.navigate("DepartmentScreen_12");
                            break;
                          case "EA":
                            navigation.navigate("DepartmentScreen_13");
                            break;
                          case "GABC": // not present in server
                            navigation.navigate("DepartmentScreen_14");
                            break;
                          case "CCGC":
                            navigation.navigate("DepartmentScreen_15");
                            break;
                          case "DBF":
                            navigation.navigate("DepartmentScreen_16");
                            break;
                          case "GBI":
                            navigation.navigate("DepartmentScreen_17");
                            break;
                          case "DOD":
                            navigation.navigate("DepartmentScreen_18");
                            break;
                          case "DCS":
                            navigation.navigate("DepartmentScreen_19");
                            break;
                          //  not in mock
                          case "DCH_ORI":
                            navigation.navigate("DepartmentDefault");
                            break;
                          case "DR":
                            navigation.navigate("DepartmentDefault");
                            break;

                          default:
                            navigation.navigate("DepartmentDefault");
                            break;
                        }
                      }}
                      body={
                        <View style={styles.bodyStyleWrapper}>
                          <View style={styles.departmentLogo}>
                            <Image
                              source={department_data[item.grp_id].image_url}
                              style={styles.bodyDepartmentLogo}
                            />
                          </View>
                          <View style={styles.bodyTextWrapper1}>
                            <Text style={styles.bodyTextWrapper2}>
                              {item.name}
                            </Text>
                            <Text style={styles.bodyTextWrapper3}>
                              ({item.grp_id})
                            </Text>
                          </View>
                        </View>
                      }
                    />
                  ))}
                </View>
              ) : (
                <Text>no userInput</Text>
              )}
            </View>
          </Content>
        }
        footerPanels={1}
        footerNavigation={"Home"}
        footer={
          <Button transparent onPress={this._go_to_home_screen}>
            <BackButton />
          </Button>
        }
      />
    ) : (
      <View style={[styles.container]}>
        {this.renderModal()}
        <ActivityIndicator size="large" color="#E06C08" />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  departments: state.userInput.datas
});

export default connect(mapStateToProps)(NewRegistration);
