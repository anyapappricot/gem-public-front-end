// // Not using this screen
// import React, { Component } from "react";
// import { View, StyleSheet, Platform } from "react-native";
// import {
//   Container,
//   Content,
//   Button,
//   Icon,
//   Right,
//   Text,
//   Item,
//   Label,
//   Form,
//   Picker,
//   Input
// } from "native-base";
// import Image from "react-native-remote-svg";
// import { withNavigation } from "react-navigation";
// import { connect } from "react-redux";
// import { Field, reduxForm } from "redux-form";
// import UserInput from "../components/UserInput";
// import UserInputOptions from "../components/OptionUserInput/Payment";
// import ScreenHeader from "../components/ScreenHeader";

// class PaymentPageNotConnected extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       fontLoaded: false,
//       boxTicked: false,
//       selected2: undefined,
//       text: ""
//     };
//   }

//   render() {
//     return (
//       <Container>
//         <ScreenHeader navigation={this.props.navigation} />
//         {this.props.reviewAgencyID ||
//         this.props.reason ||
//         this.props.lastName ||
//         this.props.firstName ? (
//           <Content
//             style={{
//               flex: 1,
//               width: Platform.OS === "android" ? 500 : "auto",
//               backgroundColor: "#ffffff"
//             }}
//           >
//             <View style={style.departmentBg}>
//               Text{" "}
//               <Image
//                 source={require("../../src/assets/view_status/preregistration.png")}
//                 style={style.courtLogo}
//               />
//               <Text style={style.departmentTitle}>
//                 Georgia State-Only Background Checks (GABC)
//               </Text>
//             </View>
//             <Text style={style.newRegistrationStyle}>Credit Card Payment</Text>

//             {/* Appended info */}
//             <View style={{ left: 25 }}>
//               <Item style={{ borderColor: "transparent", left: 20, top: 10 }}>
//                 <Text
//                   style={{
//                     color: "#1F1F1D",
//                     top: 10,
//                     marginBottom: 20,
//                     fontFamily: "Segoeui"
//                   }}
//                 >
//                   Registration ID:{" "}
//                 </Text>
//                 <Text style={{ fontFamily: "Segoeui-Semibold" }}>Input</Text>
//               </Item>
//               <Item style={{ borderColor: "transparent", left: 20, top: 10 }}>
//                 <Text
//                   style={{
//                     color: "#1F1F1D",
//                     top: 10,
//                     marginBottom: 20,
//                     fontFamily: "Segoeui"
//                   }}
//                 >
//                   Name:{" "}
//                 </Text>
//                 <Text style={{ fontFamily: "Segoeui-Semibold" }}>
//                   {this.props.firstName} {this.props.lastName}
//                 </Text>
//               </Item>
//               <Item style={{ borderColor: "transparent", left: 20, top: 10 }}>
//                 <Text
//                   style={{
//                     color: "#1F1F1D",
//                     top: 10,
//                     marginBottom: 20,
//                     fontFamily: "Segoeui"
//                   }}
//                 >
//                   Transaction Type:{" "}
//                 </Text>
//                 <Text style={{ fontFamily: "Segoeui-Semibold" }}>
//                   {this.props.reason}
//                 </Text>
//               </Item>
//               <Item style={{ borderColor: "transparent", left: 20, top: 10 }}>
//                 <Text
//                   style={{
//                     color: "#1F1F1D",
//                     top: 10,
//                     marginBottom: 20,
//                     fontFamily: "Segoeui"
//                   }}
//                 >
//                   Transaction Fee:{" "}
//                 </Text>
//                 <Text style={{ fontFamily: "Segoeui-Semibold" }}>$8.50</Text>
//               </Item>
//             </View>

//             {/* Payment input */}
//             <Form handleSubmit={() => this.handleSubmit()}>
//               <View style={style.sectionStyle}>
//                 <Text style={style.sectionTextStyle}>
//                   Credit Card Information
//                 </Text>
//                 <Text style={style.sectionTextStyle}>
//                   ________________________________________________
//                 </Text>
//               </View>
//               <Field
//                 label="Credit Card Type *"
//                 name="creditCardType"
//                 component={UserInputOptions}
//               />
//               <Field
//                 label="Card Number *"
//                 name="cardNumber"
//                 component={UserInput}
//                 //validate={[required, phoneNumber]}
//               />
//               <Item
//                 stackedLabel
//                 style={{ borderColor: "transparent", bottom: 15, left: 25 }}
//               >
//                 <View
//                   style={{
//                     flexDirection: "row",
//                     right: Platform.OS === "ios" ? 55 : 105
//                   }}
//                 >
//                   <Label
//                     style={{
//                       color: "#1F1F1D",
//                       top: 50,
//                       left: Platform.OS === "ios" ? -2 : 10,
//                       top: 68
//                     }}
//                   >
//                     Card Security Code (CSC) *
//                   </Label>
//                   {/* <Image
//                     style={{ top: 50, left: 50, width: 90, height: 90 }}
//                     source={require("../assets/AssetPayment/CSC.png")}
//                   /> */}
//                 </View>
//                 <Input style={style.inputCSC} />
//               </Item>
//               <Item
//                 stackedLabel
//                 style={{ borderColor: "transparent", left: 5, top: 10 }}
//               >
//                 <Label style={{ color: "#1F1F1D", left: 20, top: 10 }}>
//                   Expiration Date *
//                 </Label>
//                 <View
//                   style={{
//                     flexDirection: "row",
//                     justifyContent: "space-between"
//                   }}
//                 >
//                   <View style={style.inputBorderOptionStyle2Row1}>
//                     <Picker
//                       mode="dropdown"
//                       iosIcon={
//                         <Icon
//                           name="ios-arrow-dropdown"
//                           style={{
//                             color: "#707070",
//                             right: Platform.OS === "ios" ? 20 : 5
//                           }}
//                         />
//                       }
//                       style={style.pickerStyleRow}
//                       placeholder="Select Month"
//                       placeholderStyle={{
//                         color: "#090808",
//                         opacity: 0.68,
//                         fontFamily: "Segoeui"
//                       }}
//                       placeholderIconColor="#007aff"
//                       selectedValue={this.state.selected2}
//                       onValueChange={this.onValueChange2.bind(this)}
//                     >
//                       <Item label="1" value="key0" />
//                       <Item label="2" value="key1" />
//                       <Item label="3" value="key2" />
//                       <Item label="4" value="key3" />
//                       <Item label="5" value="key4" />
//                       <Item label="6" value="key5" />
//                       <Item label="7" value="key6" />
//                       <Item label="8" value="key7" />
//                       <Item label="9" value="key8" />
//                       <Item label="10" value="key9" />
//                       <Item label="11" value="key10" />
//                       <Item label="12" value="key11" />
//                     </Picker>
//                   </View>
//                   <View style={style.inputBorderOptionStyle2Row2}>
//                     <Picker
//                       mode="dropdown"
//                       iosIcon={
//                         <Icon
//                           name="ios-arrow-dropdown"
//                           style={{ color: "#707070" }}
//                         />
//                       }
//                       style={style.pickerStyleRow}
//                       placeholder="Select Year"
//                       placeholderStyle={{
//                         color: "#090808",
//                         opacity: 0.68,
//                         fontFamily: "Segoeui"
//                       }}
//                       placeholderIconColor="#007aff"
//                       selectedValue={this.state.selected2}
//                       onValueChange={this.onValueChange2.bind(this)}
//                     >
//                       <Item label="2019" value="key0" />
//                       <Item label="2020" value="key1" />
//                       <Item label="2021" value="key2" />
//                       <Item label="2022" value="key3" />
//                       <Item label="2023" value="key4" />
//                       <Item label="2024" value="key5" />
//                       <Item label="2025" value="key6" />
//                       <Item label="2026" value="key7" />
//                       <Item label="2027" value="key8" />
//                       <Item label="2028" value="key9" />
//                       <Item label="2029" value="key10" />
//                       <Item label="2030" value="key11" />
//                       <Item label="2031" value="key12" />
//                       <Item label="2032" value="key13" />
//                       <Item label="2033" value="key14" />
//                       <Item label="2034" value="key15" />
//                     </Picker>
//                   </View>
//                 </View>

//                 <Text
//                   style={{
//                     paddingTop: 20,
//                     right: Platform.OS === "ios" ? 15 : 50,
//                     width: Platform.OS === "android" ? 380 : 330
//                   }}
//                 >
//                   No unemployment cards, child support cards or gift cards are
//                   accepted.
//                 </Text>
//               </Item>
//               <Item
//                 stackedLabel
//                 style={{
//                   borderColor: "transparent",
//                   left: 25,
//                   top: 30,
//                   marginBottom: 30
//                 }}
//               >
//                 <Label style={{ color: "#1F1F1D", top: 10 }}>
//                   Name as it appears on card
//                 </Label>
//                 <Input style={style.input} />
//               </Item>

//               {/* billing address */}
//               <View style={style.sectionStyle}>
//                 <Text style={style.sectionTextStyle}>Billing Address *</Text>
//                 <Text style={style.sectionTextStyle}>
//                   ________________________________________________
//                 </Text>
//               </View>
//               <Item
//                 stackedLabel
//                 style={{ borderColor: "transparent", left: 25, top: 10 }}
//               >
//                 <Label style={{ color: "#1F1F1D", top: 10 }}>
//                   Street Address *
//                 </Label>
//                 <Input style={style.input} />
//               </Item>
//               <Item
//                 stackedLabel
//                 style={{ borderColor: "transparent", left: 25, top: 10 }}
//               >
//                 <Label style={{ color: "#1F1F1D", top: 10 }}>City *</Label>
//                 <Input style={style.input} />
//               </Item>
//               <Field
//                 label="State *"
//                 name="stateLicenseNumber"
//                 component={UserInputOptions}
//               />
//               <Item
//                 stackedLabel
//                 style={{ borderColor: "transparent", left: 25, top: 10 }}
//               >
//                 <Label style={{ color: "#1F1F1D", top: 10 }}>Zip Code *</Label>
//                 <Input style={style.input} />
//               </Item>
//               <Item
//                 stackedLabel
//                 style={{ borderColor: "transparent", left: 25, top: 10 }}
//               >
//                 <Label style={{ color: "#1F1F1D", top: 10 }}>
//                   Day Phone Number *
//                 </Label>
//                 <Input style={style.input} />
//               </Item>
//               <Item
//                 stackedLabel
//                 style={{ borderColor: "transparent", left: 25, top: 10 }}
//               >
//                 <Label style={{ color: "#1F1F1D", top: 10 }}>
//                   Email Address *
//                 </Label>
//                 <Input style={style.input} />
//               </Item>
//             </Form>

//             <View style={style.buttons}>
//               <Button
//                 block
//                 style={style.button}
//                 button
//                 onPress={() => this.props.navigation.navigate("Complete")}
//               >
//                 <Text style={style.textStyle}>Pay</Text>
//               </Button>
//             </View>

//             <View style={style.backButton}>
//               <Right>
//                 <Button
//                   transparent
//                   onPress={() => this.props.navigation.goBack()}
//                 >
//                   <Image
//                     source={require("../../assets/newRegistration/backButton.png")}
//                   />
//                 </Button>
//               </Right>
//             </View>
//           </Content>
//         ) : (
//           <Text>Please go to New registration page first.</Text>
//         )}
//       </Container>
//     );
//   }
// }

// const mapStateToProps = state => ({
//   reviewAgencyID:
//     state.form && state.form.s1 && state.form.s1.values
//       ? state.form.s1.values.reviewAgencyID
//       : null,
//   reason:
//     state.form && state.form.s1 && state.form.s1.values
//       ? state.form.s1.values.reason
//       : null,
//   lastName:
//     state.form && state.form.s2 && state.form.s2.values
//       ? state.form.s2.values.lastName
//       : null,
//   firstName:
//     state.form && state.form.s2 && state.form.s2.values
//       ? state.form.s2.values.firstName
//       : null
// });

// const PaymentPage = connect(mapStateToProps)(PaymentPageNotConnected);

// export default reduxForm({ form: "PaymentPage" })(withNavigation(PaymentPage));

// const style = StyleSheet.create({
//   headerStyle: {
//     flexDirection: "row",
//     justifyContent: "center",
//     alignItems: "center",
//     width: "auto",
//     height: 90,
//     backgroundColor: "#3e4649",
//     shadowRadius: 6,
//     shadowOpacity: 1,
//     borderStyle: "solid",
//     borderWidth: 1,
//     borderColor: "#707070"
//   },
//   logo: {
//     color: "#ffffff",
//     fontSize: 30,
//     marginRight: Platform.OS === "ios" ? 0 : 35,
//     marginTop: Platform.OS === "ios" ? 0 : 20
//   },
//   headerTitle: {
//     fontFamily: "OpenSans-Semibold",
//     fontSize: 13,
//     textAlign: "center",
//     color: "#ffffff"
//   },
//   departmentBg: {
//     width: Platform.OS === "ios" ? 500 : 500,
//     height: 74.45703125,
//     backgroundColor: "#f1f4f8"
//   },
//   courtLogo: {
//     top: 8,
//     left: 8,
//     width: 52,
//     height: 52
//   },
//   departmentTitle: {
//     bottom: 25,
//     left: 70,
//     width: Platform.OS === "ios" ? 280 : 380,
//     height: 28.45703125,
//     overflow: "hidden",
//     fontFamily: "OpenSans-Regular",
//     fontSize: 13,
//     fontWeight: "bold",
//     textAlign: "left",
//     color: "#3e4649"
//   },
//   newRegistrationStyle: {
//     fontFamily: "Roboto-Medium",
//     fontSize: 15,
//     color: "#1F1F1D",
//     paddingTop: 15,
//     right: Platform.OS === "ios" ? 0 : 40,
//     textAlign: "center"
//   },
//   sectionStyle: {
//     paddingTop: 50,
//     paddingLeft: 20,
//     paddingRight: 20
//   },
//   sectionTextStyle: {
//     fontSize: 15,
//     fontFamily: "Segoeui",
//     color: "#01366C",
//     left: 25
//   },
//   listStyle: {
//     justifyContent: "center",
//     alignItems: "center",
//     width: 259,
//     height: 259.943359375,
//     overflow: "hidden",
//     color: "#000100",
//     top: 10,
//     left: 50,
//     right: 40,
//     bottom: 10
//   },
//   button1: {
//     justifyContent: "space-between",
//     marginBottom: 5,
//     width: 259,
//     height: 32,
//     backgroundColor: "#b8bdc4"
//   },
//   buttonText: {
//     fontFamily: "Segoeui",
//     fontSize: 15,
//     color: "#000100",
//     textAlign: "center",
//     paddingTop: 5,
//     paddingBottom: 25,
//     paddingRight: 15,
//     width: 270,
//     height: 32
//   },
//   backButton: {
//     bottom: Platform.OS === "ios" ? 100 : 80,
//     right: Platform.OS === "ios" ? 130 : 190
//   },
//   tickBox: {
//     top: 53,
//     left: 20,
//     borderRadius: 0,
//     borderColor: "#3e4649"
//   },
//   tickText: {
//     top: 35,
//     bottom: 25,
//     left: 60,
//     width: 225.42114257812,
//     height: 28.45703125,
//     overflow: "hidden",
//     fontFamily: "Segoeui",
//     fontSize: 15,
//     textAlign: "left",
//     color: "#000100"
//   },
//   progressBar: {
//     justifyContent: "center",
//     alignItems: "center",
//     width: 360,
//     left: Platform.OS === "ios" ? 10 : 20
//   },
//   input: {
//     top: Platform.OS === "ios" ? 10 : 20,
//     marginBottom: Platform.OS === "ios" ? 0 : 20,
//     width: Platform.OS === "android" ? 380 : 330,
//     borderWidth: 1,
//     borderColor: "#707070",
//     textAlign: "center"
//   },
//   inputCSC: {
//     width: 190,
//     height: 40,
//     borderWidth: 1,
//     borderColor: "#707070",
//     textAlign: "center",
//     fontFamily: "Segoeui-Semibold"
//   },
//   inputBorderStyle: {
//     borderColor: "transparent",
//     left: 5,
//     marginBottom: 20
//   },
//   inputBorderOptionStyle: {
//     top: Platform.OS === "ios" ? 10 : 20,
//     right: Platform.OS === "ios" ? 0 : 50,
//     marginBottom: Platform.OS === "ios" ? 0 : 20,
//     width: Platform.OS === "android" ? 380 : 330,
//     borderWidth: Platform.OS === "ios" ? 0 : 1,
//     borderColor: Platform.OS === "ios" ? 0 : "#707070"
//   },
//   inputBorderOptionStyle2Row1: {
//     top: Platform.OS === "ios" ? 10 : 20,
//     right: Platform.OS === "ios" ? 30 : 70,
//     marginBottom: Platform.OS === "ios" ? 0 : 20,
//     width: Platform.OS === "android" ? 170 : 150,
//     borderWidth: 1,
//     borderColor: "#707070"
//   },
//   inputBorderOptionStyle2Row2: {
//     top: Platform.OS === "ios" ? 10 : 20,
//     right: Platform.OS === "ios" ? 0 : 30,
//     marginBottom: Platform.OS === "ios" ? 0 : 20,
//     width: Platform.OS === "android" ? 170 : 150,
//     borderWidth: 1,
//     borderColor: "#707070"
//   },
//   pickerStyle: {
//     top: 10,
//     right: 15,
//     width: Platform.OS === "android" ? 380 : 330,
//     borderWidth: 1,
//     borderColor: "#707070",
//     borderRadius: 0,
//     marginBottom: Platform.OS === "ios" ? 20 : 0
//   },
//   buttons: {
//     flex: 1,
//     flexDirection: "row",
//     alignSelf: "center",
//     top: Platform.OS === "ios" ? 0 : 10,
//     bottom: Platform.OS === "ios" ? 0 : 50,
//     height: 200,
//     right: Platform.OS === "ios" ? 0 : 40
//   },
//   button: {
//     top: 50,
//     width: 150,
//     height: 32,
//     borderRadius: 0,
//     backgroundColor: "#b8bdc4"
//   },
//   textStyle: {
//     color: "#000100",
//     fontFamily: "Segoeui",
//     fontSize: 15
//   },
//   checkBoxText: {
//     top: 30,
//     left: 20,
//     width: 330,
//     color: "#7A7A7A",
//     fontFamily: "Segoeui",
//     fontSize: 14
//   },
//   unemploymentNote: {
//     top: 15,
//     marginBottom: Platform.OS === "ios" ? 0 : 20,
//     width: 330,
//     right: Platform.OS === "ios" ? 10 : 70,
//     color: "#7A7A7A",
//     fontFamily: "Segoeui",
//     fontSize: 14
//   },
//   placeholderStyle: {
//     color: "#090808",
//     opacity: 0.68,
//     fontFamily: "Segoeui"
//   }
// });
