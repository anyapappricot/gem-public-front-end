import React, { Component } from "react";
import { StyleSheet, Platform, View } from "react-native";
import { Content, Text, Button } from "native-base";
import Image from "react-native-remote-svg";
import BodyWrapper from "../components/BodyWrapper";

class About extends Component {
  render() {
    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content>
            <View style={{ marginHorizontal: 40 }}>
              <Text style={styles.FAQ}>About</Text>
              <Text style={styles.line}>
                __________________________________________
              </Text>
              <Text style={styles.someStyle}>
                Welcome to the Georgia Applicant Processing Service for
                fingerprint background requests.
              </Text>
              <Text />
              <Text style={styles.someO}>
                Electronic submission of fingerprint images will involve the use
                of a Gemalto Cogent Livescan machine. The Livescan captures
                fingerprint images and demographic data and submits this
                information to GBI. GBI conducts a search of its criminal
                history records using the fingerprint images. In some cases,
                these images are also forwarded to the FBI where a Federal
                Criminal History Record search is also conducted. Notifications
                of the search results are then forwarded from the GBI/FBI to
                Gemalto Cogent where these results are then electronically
                disseminated to the Georgia company or agency that requested the
                search to be completed.
              </Text>
            </View>
          </Content>
        }
        footerPanels={1}
        footer={
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("HomeScreen")}
          >
            <Image
              source={{
                uri: `data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" fill="#000000" fill-opacity="0.5" width="30" height="30" viewBox="5 3 17 17"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/></svg>								`
              }}
            />
          </Button>
        }
      />
    );
  }
}

export default About;

const styles = StyleSheet.create({
  someO: {
    fontFamily: "OpenSans-Light"
  },
  someStyle: {
    fontFamily: "OpenSans-Light",
    paddingTop: 20
  },
  FAQ: {
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  line: {
    textAlign: "center",
    color: "#C4C4C4"
  }
});
