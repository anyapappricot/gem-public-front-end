import React, { Component } from "react";
import { StyleSheet, View, Dimensions, ActivityIndicator } from "react-native";
import { Content, Button, Text, List, ListItem } from "native-base";
import Image from "react-native-remote-svg";
import { connect } from "react-redux";
import { fetchLocation } from "../../src/redux/actions/actions";
import BodyWrapper from "../components/BodyWrapper";
import SmartButton from "../components/SmartButton";

import MapModal from "../../src/components/MapModal";
import sortByDistance from "sort-by-distance";

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

class FingerprintSiteLocations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      modalVisible: false,
      dest_lat: null,
      dest_long: null,
      locations: null,
      error: null,
      latitude: null,
      longitude: null
      // latitude: 33.757934,
      // longitude: -84.394811
    };
  }

  componentDidMount() {
    this.props.dispatch(fetchLocation());

    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null
        });
      },
      error => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }

  componentDidUpdate = () => {
    if (this.state.locations === null) {
      const origin = {
        latitude: this.state.latitude,
        longitude: this.state.longitude
      };

      const opts = {
        yName: "latitude",
        xName: "longitude"
      };

      if (this.props.locations) {
        const points = this.props.locations.map(obj => {
          const newobj = {};
          let key,
            keys = Object.keys(obj);
          let n = keys.length;

          while (n--) {
            key = keys[n];
            newobj[key.toLowerCase()] = obj[key];
          }
          return newobj;
        });

        this.setState({ locations: sortByDistance(origin, points, opts) });
      }
    }
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  closeModal = () => {
    this.setState({ modalVisible: false });
  };

  _calc_distance = (lat1, lon1, lat2, lon2) => {
    const R = 3956; // 3956 for km || 3956 for mls
    const dLat = ((lat2 - lat1) * Math.PI) / 180;
    const dLon = ((lon2 - lon1) * Math.PI) / 180;
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos((lat1 * Math.PI) / 180) *
        Math.cos((lat2 * Math.PI) / 180) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;
    if (d > 1) return "Distance: " + Math.round(d) + " miles away";
    else if (d <= 1) return "Distance: " + Math.round(d * 1000) + " miles away";
    return d;
  };

  render() {
    const listPart = this.state.locations ? (
      <View>
        <MapModal
          latitude={this.state.latitude}
          longitude={this.state.longitude}
          closeModal={this.closeModal}
          modalVisible={this.state.modalVisible}
          dest_lat={this.state.dest_lat}
          dest_long={this.state.dest_long}
        />
        <View>
          <List>
            {this.state.locations.map(item => (
              <ListItem key={item.id} style={style.listItemStyle}>
                <Text style={style.ls_name}>{item.ls_name}</Text>
                <Text />
                <Text style={style.other_ls}>
                  {item.ls_address1} {item.ls_address2}
                </Text>
                <Text />
                <Text style={style.other_ls}>
                  {item.ls_city} {item.ls_state} {item.ls_zip}
                </Text>
                <Text />
                <Text style={style.other_ls}>{item.contact_phone}</Text>
                <Text />
                <Text style={style.other_ls}>
                  {this._calc_distance(
                    item.latitude,
                    item.longitude,
                    this.state.latitude,
                    this.state.longitude
                  )}
                </Text>

                <View style={style.smartButtonWrapper}>
                  <SmartButton
                    buttonBorderStyle={style.buttonBorderStyle}
                    text={"Open in Google Map"}
                    onPress={async () => {
                      await this.setState({
                        dest_lat: item.latitude,
                        dest_long: item.longitude
                      });
                      this.setModalVisible(true);
                    }}
                  />
                </View>
              </ListItem>
            ))}
          </List>
        </View>
      </View>
    ) : (
      <View style={style.ternaryRender2}>
        <ActivityIndicator size="large" color="#E06C08" />
      </View>
    );

    return (
      <BodyWrapper
        {...this.props}
        body={
          <Content contentContainerStyle={style.contentContainerStyle}>
            {listPart}
          </Content>
        }
        footerPanels={1}
        footer={
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("HomeScreen")}
          >
            <Image
              source={{
                uri: `data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" fill="#000000" fill-opacity="0.5" width="30" height="30" viewBox="5 3 17 17"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/></svg>								`
              }}
            />
          </Button>
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  locations: state.locations.datas.data
    ? state.locations.datas.data.locations
    : null
});

export default connect(mapStateToProps)(FingerprintSiteLocations);

const style = StyleSheet.create({
  listItemStyle: { flexDirection: "column" },
  ls_name: { fontFamily: "OpenSans-Semibold" },
  other_ls: { fontFamily: "OpenSans-regular" },
  smartButtonWrapper: { padding: 20 },
  buttonBorderStyle: { width: width / 2 },
  ternaryRender2: {
    height: height,
    justifyContent: "center",
    alignItems: "center"
  },
  contentContainerStyle: { flexGrow: 1 }
});
