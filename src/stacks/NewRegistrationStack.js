import { createStackNavigator } from "react-navigation";

// Import the screen(s) you'd like to have in the stack
import NewRegistrationScreen from "../../src/screens/NewRegistration";

import DepartmentScreen_1 from "../../src/screens/departments/DepartmentScreen_1";
import DepartmentScreen_2 from "../../src/screens/departments/DepartmentScreen_2";
import DepartmentScreen_3 from "../../src/screens/departments/DepartmentScreen_3";
import DepartmentScreen_4 from "../../src/screens/departments/DepartmentScreen_4";
import DepartmentScreen_5 from "../../src/screens/departments/DepartmentScreen_5";
import DepartmentScreen_6 from "../../src/screens/departments/DepartmentScreen_6";
import DepartmentScreen_7 from "../../src/screens/departments/DepartmentScreen_7";
import DepartmentScreen_8 from "../../src/screens/departments/DepartmentScreen_8";
import DepartmentScreen_9 from "../../src/screens/departments/DepartmentScreen_9";
import DepartmentScreen_10 from "../../src/screens/departments/DepartmentScreen_10";
import DepartmentScreen_11 from "../../src/screens/departments/DepartmentScreen_11";
import DepartmentScreen_12 from "../../src/screens/departments/DepartmentScreen_12";
import DepartmentScreen_13 from "../../src/screens/departments/DepartmentScreen_13";
import DepartmentScreen_14 from "../../src/screens/departments/DepartmentScreen_14";
import DepartmentScreen_15 from "../../src/screens/departments/DepartmentScreen_15";
import DepartmentScreen_16 from "../../src/screens/departments/DepartmentScreen_16";
import DepartmentScreen_17 from "../../src/screens/departments/DepartmentScreen_17";
import DepartmentScreen_18 from "../../src/screens/departments/DepartmentScreen_18";
import DepartmentScreen_19 from "../../src/screens/departments/DepartmentScreen_19";
import DepartmentDefault from "../../src/screens/departments/DepartmentDefault";
import PreRegistration from "../../src/screens/PreRegistration";
import FormScreen_1 from "../../src/screens/forms/FormScreen_1";
import FormScreen_2 from "../../src/screens/forms/FormScreen_2";
import FormScreen_3 from "../../src/screens/forms/FormScreen_3";
import FormScreen_4 from "../../src/screens/forms/FormScreen_4";
import SuccessScreen from "../../src/screens/forms/SuccessScreen";
import VerificationScreen from "../../src/screens/forms/VerificationScreen";
import FormCompleteScreen from "../../src/screens/FormCompleteScreen";

// deprecated
// import PaymentPage from "../../src/screens/PaymentPage";
// import CompleteScreen from "../../src/screens/Complete";

// List the screens you'd like in each stack
// Each stack comes with an initial route
// Optional: Override the `navigationOptions for this screen
// In this, case drawerLabel = null
export default (NewRegistrationStack = createStackNavigator(
  {
    NewRegistrationScreen,
    DepartmentScreen_1,
    DepartmentScreen_2,
    DepartmentScreen_3,
    DepartmentScreen_4,
    DepartmentScreen_5,
    DepartmentScreen_6,
    DepartmentScreen_7,
    DepartmentScreen_8,
    DepartmentScreen_9,
    DepartmentScreen_10,
    DepartmentScreen_11,
    DepartmentScreen_12,
    DepartmentScreen_13,
    DepartmentScreen_14,
    DepartmentScreen_15,
    DepartmentScreen_16,
    DepartmentScreen_17,
    DepartmentScreen_18,
    DepartmentScreen_19,
    DepartmentDefault,
    PreRegistration,
    FormScreen_1,
    FormScreen_2,
    FormScreen_3,
    FormScreen_4,
    VerificationScreen,
    SuccessScreen,
    FormCompleteScreen
  },
  { headerMode: "none", initialRouteName: "NewRegistrationScreen" },
  { navigationOptions: { drawerLabel: () => null } }
));
