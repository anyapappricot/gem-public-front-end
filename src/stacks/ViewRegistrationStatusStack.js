import { createStackNavigator } from "react-navigation";

// Import the screen(s) you'd like to have in the stack
// import PaymentPage from "../screens/PaymentPage";
import ViewStatus from "../../src/screens/status/ViewStatusScreen";
import LogInToViewStatus from "../../src/screens/status/LogInToViewStatus";

// List the screens you'd like in each stack
// Each stack comes with an initial route
// Optional: Override the `navigationOptions for this screen
// In this, case drawerLabel = null
export default (ViewRegistrationStatusStack = createStackNavigator(
  { ViewStatus, LogInToViewStatus },
  // { PaymentPage, ViewStatus, LogInToViewStatus },
  { headerMode: "none", initialRouteName: "LogInToViewStatus" },
  { navigationOptions: { drawerLabel: () => null } }
));
