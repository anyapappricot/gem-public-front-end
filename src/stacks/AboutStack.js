import { createStackNavigator } from "react-navigation";

// Import the screen(s) you'd like to have in the stack
import About from "../screens/About";
import HomeScreen from "../screens/HomeScreen";

// List the screens you'd like in each stack
// Each stack comes with an initial route
// Optional: Override the `navigationOptions for this screen
// In this, case drawerLabel = null
export default (AboutStack = createStackNavigator(
  { About, HomeScreen },
  { headerMode: "none", initialRouteName: "About" },
  { navigationOptions: { drawerLabel: () => null } }
));
