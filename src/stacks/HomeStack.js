import { createStackNavigator } from "react-navigation";

// Import the screen(s) you'd like to have in the stack
import HomeScreen from "../screens/HomeScreen";
import ViewStatus from "../screens/status/ViewStatusScreen";

// List the screens you'd like in each stack
// Each stack comes with an initial route
// Optional: Override the `navigationOptions for this screen
// In this, case drawerLabel = null
export default (HomeStack = createStackNavigator(
  { HomeScreen, ViewStatus },
  { headerMode: "none", initialRouteName: "HomeScreen" },
  { navigationOptions: { drawerLabel: () => null } }
));
