import { createStackNavigator } from "react-navigation";

// Import the screen(s) you'd like to have in the stack
import FAQ from "../screens/faqs/FAQ";

import FAQ1 from "../screens/faqs/FAQ1";
import FAQ2 from "../screens/faqs/FAQ2";
import FAQ3 from "../screens/faqs/FAQ3";
import FAQ4 from "../screens/faqs/FAQ4";
import FAQ5 from "../screens/faqs/FAQ5";
import FAQ6 from "../screens/faqs/FAQ6";
import FAQ7 from "../screens/faqs/FAQ7";
import FAQ8 from "../screens/faqs/FAQ8";
import FAQ9 from "../screens/faqs/FAQ9";
import FAQ10 from "../screens/faqs/FAQ10";
import FAQ11 from "../screens/faqs/FAQ11";
import FAQ12 from "../screens/faqs/FAQ12";
import FAQ13 from "../screens/faqs/FAQ13";
import FAQ14 from "../screens/faqs/FAQ14";

// List the screens you'd like in each stack
// Each stack comes with an initial route
// Optional: Override the `navigationOptions for this screen
// In this, case drawerLabel = null
export default (FAQStack = createStackNavigator(
  {
    FAQ,
    FAQ1,
    FAQ2,
    FAQ3,
    FAQ4,
    FAQ5,
    FAQ6,
    FAQ7,
    FAQ8,
    FAQ9,
    FAQ10,
    FAQ11,
    FAQ12,
    FAQ13,
    FAQ14
  },
  {
    headerMode: "none",
    initialRouteName: "FAQ"
  },
  {
    navigationOptions: {
      drawerLabel: () => null
    }
  }
));
