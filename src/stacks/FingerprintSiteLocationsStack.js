import { createStackNavigator } from "react-navigation";

// Import the screen(s) you'd like to have in the stack
import FingerprintSiteLocations from "../screens/FingerprintSiteLocations";

// List the screens you'd like in each stack
// Each stack comes with an initial route
// Optional: Override the `navigationOptions for this screen
// In this, case drawerLabel = null
export default (FingerprintSiteLocationsStack = createStackNavigator(
  { FingerprintSiteLocations },
  { headerMode: "none", initialRouteName: "FingerprintSiteLocations" },
  { navigationOptions: { drawerLabel: () => null } }
));
