import React, { Component } from "react";
import {
  Modal,
  Text,
  View,
  StyleSheet,
  Platform,
  Dimensions
} from "react-native";
import SmartButton from "../components/SmartButton";

import getDirections from "react-native-google-maps-directions";

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;
//
// NOTE TO SELF
// GET SOME SLEEP
//
class MapModal extends Component {
  handleGetDirections = () => {
    const data = {
      source: {
        latitude: this.props.latitude,
        longitude: this.props.longitude
      },
      destination: {
        latitude: this.props.dest_lat,
        longitude: this.props.dest_long
      },
      params: [
        {
          key: "travelmode",
          value: "driving" // may be "walking", "bicycling" or "transit" as well
        }
      ]
    };
    //
    getDirections(data);
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.modalVisible}
      >
        <View style={style.container}>
          <Text style={style.warning_message}>
            You are about to be redirected to Google Maps
          </Text>
          <View style={style.button_container}>
            <View style={style.buttonStyle}>
              <SmartButton
                buttonBorderStyle={style.button_border_style}
                text={"Go Back"}
                onPress={() => {
                  this.props.closeModal();
                }}
              />
            </View>
            <View style={style.buttonStyle}>
              <SmartButton
                buttonBorderStyle={style.button_border_style}
                text={"Open in Maps"}
                onPress={() => {
                  this.handleGetDirections();
                }}
              />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default MapModal;

const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 20
  },
  warning_message: {
    fontSize: 27,
    textAlign: "center",
    fontFamily: "Montserrat-Regular",
    marginBottom: 25
  },
  button_container: { flexDirection: "row", justifyContent: "center" },
  buttons: {
    alignItems: "center",
    justifyContent: "center",
    top: 20,
    marginHorizontal: Platform.OS === "ios" ? 50 : 80,
    marginBottom: 30,
    width: 100,
    height: 32,
    backgroundColor: "#b8bdc4",
    borderRadius: 0
  },
  buttonStyle: { padding: 20 },
  buttonText: {
    color: "white",
    fontSize: 15
  },
  button_border_style: { width: width / 3 }
});
