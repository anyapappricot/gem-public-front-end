import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Animated,
  TouchableOpacity,
  TouchableHighlight,
  Platform
} from "react-native";

const base_elevation = 3;

class SmartCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      elevation: new Animated.Value(base_elevation)
    };
  }
  _press_in = () => {
    // Animate over time
    Animated.timing(
      this.state.elevation, // The animated value to drive
      {
        toValue: 10, // Animate to elevation: 10
        // duration: 10 // Make it take a while
        duration: 100, // Make it take a while
        useNativeDriver: true
      }
    ).start(); // Starts the animation
  };

  _press_out = () => {
    // Animate over time
    Animated.timing(
      this.state.elevation, // The animated value to drive
      {
        toValue: base_elevation, // Animate to elevation: 5
        duration: 500, // Make it take a while
        useNativeDriver: true
      }
    ).start(); // Start the animation
  };

  render() {
    return (
      <Animated.View style={style.container} elevation={this.state.elevation}>
        <TouchableHighlight
          underlayColor="rgba(0, 0, 0, .0000)"
          onHideUnderlay={Platform.OS === "android" ? this._press_out : null}
          onShowUnderlay={Platform.OS === "android" ? this._press_in : null}
          onPress={this.props.onPress}
        >
          {this.props.body}
        </TouchableHighlight>
      </Animated.View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    margin: 10,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: "rgba(255, 255, 255, 1)",
    backgroundColor: "#f7f7f7"
  }
});

export default SmartCard;
