import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { Text, CheckBox } from "native-base";

export default class HomeButton extends Component {
  render() {
    return (
      <View style={style.container}>
        <CheckBox
          color="#6699CC"
          style={style.tickBoxAndroid}
          checked={this.props.checked}
          boxTicked={this.props.boxTicked}
          onPress={this.props.onPress}
        />
        <View style={style.tickTextWrapper}>
          <Text style={style.tickText}>{this.props.text}</Text>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginBottom: 20,

    alignItems: "center",
    justifyContent: "center"
  },
  tickBoxAndroid: {
    justifyContent: "center",
    alignItems: "center",
    margin: 10,
    width: 44,
    height: 44,
    borderRadius: 0,
    borderColor: "#c7c7c7"
  },
  tickTextWrapper: {
    justifyContent: "center",
    alignItems: "center",
    padding: 10
  },
  tickText: {
    margin: 10,
    overflow: "hidden",
    fontFamily: "OpenSans-Regular",
    fontSize: 15,
    color: "#3D454C"
  }
});
