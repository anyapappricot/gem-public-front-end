import React, { Component } from "react";

import {
  Text,
  View,
  StyleSheet,
  Animated,
  TouchableHighlight
} from "react-native";

import { connect } from "react-redux";
import { save_data_dev } from "../redux/actions/actions";

class SmartSwitch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      button_text: this.props.right_text,
      switch: "flex-start"
    };
  }

  componentDidMount = () => {
    this._handleClick();
  };

  _slide_left = () => {
    Animated.timing(
      // Animate over time
      this.state.fadeAnim, // The animated value to drive
      {
        toValue: 1, // Animate to opacity: 1 (opaque)
        duration: 1000 // Make it take a while
      }
    ).start(); // Starts the animation
  };

  _slide_right = () => {
    Animated.timing(
      // Animate over time
      this.state.fadeAnim, // The animated value to drive
      {
        toValue: 0, // Animate to opacity: 1 (opaque)
        duration: 1000 // Make it take a while
      }
    ).start(); // Starts the animation
  };

  _handleClick = async () => {
    if (this.state.switch === "flex-end") {
      await this.setState({
        button_text: this.props.left_text,
        switch: "flex-start"
      });
      this.props.dispatch(save_data_dev("side_bar_switch", true));
    } else {
      await this.setState({
        button_text: this.props.right_text,
        switch: "flex-end"
      });
      this.props.dispatch(save_data_dev("side_bar_switch", false));
    }
  };

  render() {
    return (
      <View
        style={[
          {
            justifyContent: "center",
            borderWidth: 1,
            backgroundColor: "rgba(0, 0, 0, 0.025)",
            backgroundColor: "rgba(102, 153, 204, 0.025)",
            borderColor: "#c7c7c7"
          },
          this.state.active ? { backgroundColor: "white" } : null,
          styles.border,

          [
            { backgroundColor: "rgba(0, 0, 0, 0.5)", width: 200 },
            {
              borderWidth: 1,
              borderRadius: 2,
              borderColor: "#rgba(0, 0, 0, .00025)",
              shadowColor: "#000",
              shadowOffset: { width: 0, height: 5 },
              shadowOpacity: 0.5,
              shadowRadius: 5,
              elevation: 0
            }
          ]
        ]}
      >
        <Animated.View
          style={{
            justifyContent: this.state.switch,
            alignItems: "center",
            backgroundColor: "black",
            backgroundColor: "rgba(0, 0, 0, 0)",
            flexDirection: "row",
            alignSelf: "stretch"
          }}
        >
          <TouchableHighlight
            style={{
              width: 100,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "rgba(102, 153, 204, 1)",
              padding: 20
            }}
            onPressIn={this._handleClick}
          >
            <Text style={{ color: "white" }}>{this.state.button_text}</Text>
          </TouchableHighlight>
        </Animated.View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  side_bar_switch_test: state.dev_reducer.options.side_bar_switch
});

export default connect(mapStateToProps)(SmartSwitch);

const styles = StyleSheet.create({
  border: {
    backgroundColor: "rgba(0, 0, 0, 0.025)",
    backgroundColor: "rgba(102, 153, 204, 0.999)",
    borderColor: "#c7c7c7",
    borderRadius: 100 / 2,
    borderRadius: 5
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 5,
    paddingBottom: 5
  },
  buttonText: {
    fontFamily: "segoeui",
    fontSize: 15,
    padding: 10,
    textAlign: "center",
    color: "black",
    color: "white"
  }
});
