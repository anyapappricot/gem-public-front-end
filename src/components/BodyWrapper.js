import React, { Component } from "react";
import { View, StyleSheet } from "react-native";

import ScreenHeader from "../components/ScreenHeader";

export default class BodyWrapper extends Component {
  render() {
    const { navigation, ...props } = this.props;
    return (
      <View style={styles.container}>
        {!props.modal_mode ? <ScreenHeader navigation={navigation} /> : null}

        <View style={styles.body}>{props.body}</View>

        {props.footerPanels === 0 ? null : props.footerPanels === 1 ? (
          <View style={styles.footer_container}>
            <View style={styles.footer_panel}>
              <View>{props.footer}</View>
            </View>
          </View>
        ) : props.footerPanels === 2 ? (
          <View style={styles.footer_container}>
            <View style={styles.footer_panel}>
              <View>{props.footer}</View>
            </View>
            <View style={styles.footer_panel}>
              <View>{props.footer_2}</View>
            </View>
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  body: {
    flex: 10
  },
  footer_container: {
    flex: 1,
    flexDirection: "row"
  },
  footer_panel: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  }
});
