import React, { Component } from "react";
import { Text, View, StyleSheet, Dimensions } from "react-native";
import { ListItem, List, Row } from "native-base";
import { saveData } from "../../src/redux/actions/actions";
import SmartButton from "../components/SmartButton";

import { connect } from "react-redux";
const width = Dimensions.get("window").width;
class ListOfSubGroups extends Component {
  render() {
    return (
      <View>
        {this.props.departments[this.props.departmentIndex].subgroup.map(
          item => (
            <List key={item.id} style={styles.listStyle}>
              <SmartButton
                disabled={false}
                key={item.id}
                text={item.name}
                onPress={() => {
                  const _item = {
                    department: this.props.departments[
                      this.props.departmentIndex
                    ].name,

                    _img_url: this.props.departments[this.props.departmentIndex]
                      .img_url,
                    img_url:
                      "https://via.placeholder.com/150/00a3e0/fff?text=FPO"
                  };

                  this.props.dispatch(
                    saveData("subgroup", { ...item, ..._item })
                  );

                  this.props.navigation.navigate("PreRegistration");
                }}
              />
            </List>
          )
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  departments: state.userInput.datas,
  departmentIndex: state.userInput.departmentIndex
});

export default connect(mapStateToProps)(ListOfSubGroups);

const styles = StyleSheet.create({
  listStyle: {
    padding: 20
  }
});
