import React, { Component } from "react";
import Image from "react-native-remote-svg";

export default class BackButton extends Component {
  render() {
    return (
      <Image
        source={{
          uri: `data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" fill="#3D454C" fill-opacity="1" width="30" height="30" viewBox="5 5 13 13"><path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"/></svg>								`
        }}
      />
    );
  }
}
