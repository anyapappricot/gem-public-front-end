import React, { Component } from "react";
import { Text, View } from "react-native";
import { Label } from "native-base";
export default class CustomListItem extends Component {
  render() {
    return (
      <View
        style={{
          justifyContent: "space-between",
          alignSelf: "stretch",
          paddingRight: 20,
          paddingLeft: 20,
          marginTop: 10,
          marginBottom: 10
        }}
      >
        <Label>{this.props.label}</Label>
        <Text style={{ fontWeight: "700" }}>{this.props.text}</Text>
      </View>
    );
  }
}
