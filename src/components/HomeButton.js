import React, { Component } from "react";
import Image from "react-native-remote-svg";

export default class HomeButton extends Component {
  render() {
    return (
      <Image
        source={{
          uri: `data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" fill="#3D454C" fill-opacity="1" width="30" height="30" viewBox="5 3 17 17"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/></svg>								`
        }}
      />
    );
  }
}
