import { Component } from "react";
import { AsyncStorage } from "react-native";

export default class Storage extends Component {
  _handle_data = () => {
    const { first_name, last_name, dob, reg_id, list, callback } = this.props;

    // THE CODE BELOW IS TO 'SET' DATA
    if (first_name) {
      this._storeData("first_name", first_name);
    }

    if (last_name) {
      this._storeData("last_name", last_name);
    }

    if (dob) {
      this._storeData("dob", dob);
    }

    if (this.props.reg_id) {
      this._storeData("reg_id", reg_id);
    }

    // THE CODE BELOW IS TO 'GET' DATA
    if (callback) {
      this._retrieveData(list, callback);
    }
  };

  shouldComponentUpdate = () => false;

  _storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (error) {
      // console.warn(`ERROR: ${error}`);
    }
  };

  _retrieveData = (list, callback) => {
    try {
      list.map(async key => {
        const data = await AsyncStorage.getItem(key);
        data !== null ? callback(key, data) : null;
      });
    } catch (error) {
      // console.warn(`ERROR: ${error}`);
    }
    return;
  };

  render = () => {
    this._handle_data();
    return null;
  };
}
