import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";
import Image from "react-native-remote-svg";

import { connect } from "react-redux";

import department_data from "../../assets/json/departments";

class DepartmentHeader extends Component {
  render() {
    const { grp_id, name } = this.props.departments[this.props.departmentIndex];
    return (
      <View style={styles.departmentTitle}>
        <Image source={department_data[grp_id].image_url} />
        <Text style={styles.departmentHeader}>
          {name} ({grp_id})
        </Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  departments: state.userInput.datas,
  departmentIndex: state.userInput.departmentIndex
});

export default connect(mapStateToProps)(DepartmentHeader);

const styles = StyleSheet.create({
  departmentTitle: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    padding: 25
    // backgroundColor: "green"
  },
  departmentHeader: {
    paddingTop: 25,
    fontSize: 40,
    // fontSize: 60,
    color: "rgba(0, 0, 0, .75)",
    fontFamily: "OpenSans-Regular",
    textAlign: "center"
  }
});
