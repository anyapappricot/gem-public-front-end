import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
  Animated,
  Platform,
  Dimensions
} from "react-native";
const width = Dimensions.get("window").width;

const base_elevation = 3;
class SmartButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,

      elevation: new Animated.Value(base_elevation)
    };
  }

  _press_in = () => {
    // Animate over time
    Animated.timing(
      this.state.elevation, // The animated value to drive
      {
        toValue: 10, // Animate to elevation: 10
        duration: 100, // Make it take a while,
        useNativeDriver: true
      }
    ).start(); // Starts the animation
  };

  _press_out = () => {
    // Animate over time
    Animated.timing(
      this.state.elevation, // The animated value to drive
      {
        toValue: base_elevation, // Animate to elevation: 5
        duration: 500, // Make it take a while
        useNativeDriver: true
      }
    ).start(); // Start the animation
  };

  render() {
    return (
      <Animated.View
        style={[
          {
            shadowOffset: { width: 0, height: 5 },
            shadowOpacity: 0.5,
            shadowRadius: 5,
            elevation: this.state.elevation
          },
          this.props.disabled ? [styles.disabled] : styles.border
        ]}
      >
        <TouchableHighlight
          underlayColor="rgba(0, 0, 0, .25)"
          onHideUnderlay={Platform.OS === "android" ? this._press_out : null}
          onShowUnderlay={Platform.OS === "android" ? this._press_in : null}
          onPressIn={() => this.setState({ active: !this.state.active })}
          style={[
            styles.button,
            this.props.disabled ? styles.disabled : styles.border,
            this.props.buttonBorderStyle
          ]}
          onPress={() => (this.props.disabled ? null : this.props.onPress())}
        >
          {this.props.image ? (
            <View style={styles.imageWrapper}>
              <View style={styles.imageStyle}>{this.props.image}</View>
              <View style={styles.imageTextStyle}>
                <Text
                  style={[
                    styles.buttonText,
                    this.props.buttonTextStyle,
                    {
                      shadowColor: "#000",
                      shadowOffset: { width: 0, height: 5 },
                      shadowOpacity: 0.3,
                      shadowRadius: 5
                    }
                  ]}
                >
                  {this.props.text}
                </Text>
              </View>
            </View>
          ) : (
            <View style={styles.textButtonStyle}>
              <Text
                style={[
                  styles.buttonText,
                  this.props.buttonTextStyle,
                  {
                    shadowColor: "#000",
                    shadowOffset: { width: 0, height: 5 },
                    shadowOpacity: 0.3,
                    shadowRadius: 5
                  }
                ]}
              >
                {this.props.text}
              </Text>
            </View>
          )}
        </TouchableHighlight>
      </Animated.View>
    );
  }
}

export default SmartButton;

const styles = StyleSheet.create({
  imageWrapper: { flexDirection: "row", padding: 10 },
  imageStyle: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: width > 375 ? 50 : 0
  },
  imageTextStyle: {
    flex: 3,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "white",
    paddingRight: width > 375 ? 50 : 0
  },
  textButtonStyle: {
    padding: 5
  },
  disabled: {
    backgroundColor: "rgba(71, 71, 71, 0.999)",
    backgroundColor: "#3D454C"
  },
  border: {
    backgroundColor: "rgba(102, 153, 204, 0.999)"
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 5,
    paddingBottom: 5
  },
  buttonText: {
    fontFamily: "segoeui",
    fontSize: 15,
    // flexWrap: 1,
    padding: 10,
    textAlign: "center",
    color: "white"
  }
});
