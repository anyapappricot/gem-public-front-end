import React, { PureComponent } from "react";

import { View, StyleSheet, Platform, Dimensions } from "react-native";
import { Text, List } from "native-base";

import CustomListItem from "../../src/components/CustomListItem";

import moment from "moment";

const width = Dimensions.get("window").width;

class ViewStatusListItem extends PureComponent {
  _setCharAt = (str, index, chr) => {
    if (index > str.length - 1) return str;
    return str.substr(0, index) + chr + str.substr(index + 1);
  };

  _the_mix_up = raw_string => {
    let string = raw_string;

    if (string) {
      let string_length = string.length - 1;

      const funx = ___ => {
        const index = Math.floor(Math.random() * string.length - 1);
        const replacement = "*";
        // console.warn("index", index);
        // console.warn("string_length", string_length);
        if (string_length) {
          string_length--;
          string = this._setCharAt(string, index, replacement);

          funx(string);
        }
      };
      // funx();
      return string;
    }
  };

  render() {
    const {
      nearly_identical,
      reg_id,
      last_name,
      first_name,
      app_email,
      user_fee,
      dob,
      email_confirmed,
      approve_date,
      reject_date,
      pay_flag,
      pay_date,
      received,
      used,
      in_queue
    } = this.props;

    return (
      <View style={style.box}>
        {!nearly_identical ? (
          <List style={style.listStyle}>
            <CustomListItem label="Registration ID:" text={reg_id} />
            <CustomListItem
              label="Name:"
              text={`${last_name}, ${first_name}`}
            />
            <CustomListItem label="DOB:" text={dob} />
            <CustomListItem label="Email:" text={app_email} />
            <CustomListItem
              label="User Fee:"
              text={user_fee ? user_fee : "N/A"}
            />
            <CustomListItem
              label="Email confirmed:"
              text={
                email_confirmed ? email_confirmed : "Pending for confirmation"
              }
            />
            <CustomListItem
              label="Registration Approval Date:"
              text={approve_date ? approve_date : "Pending for approval"}
            />
            <CustomListItem
              label="Fingerpring Rejection Date:"
              text={reject_date ? reject_date : "N/A"}
            />
            <CustomListItem label="Pay Type:" text={pay_flag} />
            <CustomListItem
              label="Pay Date:"
              text={pay_date ? pay_date : "Not Paid"}
            />
            <CustomListItem
              label="Received:"
              text={received ? moment(received).format("MMMM Do YYYY") : "N/A"}
            />
            {/* <CustomListItem label="Used:" text={used} /> */}
            {/* <CustomListItem label="In Queue:" text={in_queue} /> */}
            {/* <CustomListItem
          label="Registration Date:"
          text={registrationDate} // need this from user when he/she submits
        /> */}

            {/* <View style={{ paddingLeft: 20, paddingRight: 20 }}>
          <Text style={style.blue_line} />
        </View> */}
          </List>
        ) : (
          <List style={{ paddingTop: 20, paddingBottom: 20 }}>
            <CustomListItem label="Registration ID:" text={reg_id} />

            <CustomListItem
              label="Registration Approval Date:"
              text={approve_date ? approve_date : "Pending for approval"}
            />

            <CustomListItem label="Pay Type:" text={pay_flag} />

            {/* <CustomListItem
              label="Name:"
              text={`${last_name}, ${first_name}`}
            />
            <CustomListItem label="DOB:" text={dob} />
            <CustomListItem label="Email:" text={this._the_mix_up(app_email)} />
            <CustomListItem
              label="User Fee:"
              text={user_fee ? user_fee : "N/A"}
            />
            <CustomListItem
              label="Email confirmed:"
              text={
                email_confirmed ? email_confirmed : "Pending for confirmation"
              }
            />
            <CustomListItem
              label="Registration Approval Date:"
              text={approve_date ? approve_date : "Pending for approval"}
            /> */}
          </List>
        )}
      </View>
    );
  }
}

const ___ViewStatusListItem = ({
  reg_id,
  last_name,
  first_name,
  dob,
  app_email,
  registrationDate,
  emailConfirmDate,
  approve_date
}) => (
  <View style={style.box}>
    <List
      style={{
        paddingTop: 20,
        paddingBottom: 20,
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <CustomListItem label="Registration ID:" text={reg_id} />
      <CustomListItem label="Name:" text={`${last_name}, ${first_name}`} />
      <CustomListItem label="DOB:" text={dob} />
      <CustomListItem label="Email:" text={app_email} />
      <CustomListItem
        label="Registration Date:"
        text={registrationDate} // need this from user when he/she submits
      />
      <CustomListItem
        label="Email Confirm Date:"
        text={emailConfirmDate ? emailConfirmDate : "Pending for confirmation"}
      />
      <CustomListItem
        label="Approve Date:"
        text={approve_date ? approve_date : "Pending for approval"}
      />
      <View style={{ paddingLeft: 20, paddingRight: 20 }}>
        <Text style={style.blue_line} />
      </View>

      {/*  to be changed by B */}
      {/* <CustomListItem label="Fee:" text="FPO" />
      <CustomListItem label="Payment Type" text="{this.props.payment}" />
      <CustomListItem label="Payment Date:" text="FPO" /> */}
    </List>
  </View>
);

export default ViewStatusListItem;

const style = StyleSheet.create({
  listStyle: { paddingTop: 20, paddingBottom: 20 },
  subTitle: {
    fontFamily: "Montserrat-Regular",
    fontSize: 19,
    textAlign: "center",
    paddingBottom: Platform.OS === "ios" ? 30 : 20,
    color: "#01366C",
    //
    fontFamily: "Montserrat-Regular",
    fontSize: 24,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    color: "#01366C",
    paddingTop: 25
  },
  box: {
    justifyContent: "center",
    // alignItems: "flex-start",
    // top: Platform.OS === "ios" ? 0 : 5,
    // left: Platform.OS === "ios" ? 40 : 45,
    // bottom: Platform.OS === "ios" ? 0 : 50,
    width: width - 40,
    // height: Platform.OS === "ios" ? 650 : 560,
    textAlign: "center",
    borderRadius: 3,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#c8c8c8",
    backgroundColor: "#ffffff"
  },
  blue_line: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: "#01366C",
    height: 0.5
    // width: width - 40
    // ma
  }
});
