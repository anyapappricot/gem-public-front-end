import React, { Component } from "react";
import {
  StyleSheet,
  Platform,
  TouchableOpacity,
  Text,
  View
} from "react-native";
import { Header, Label, Title, Button, Icon, Body } from "native-base";
import Image from "react-native-remote-svg";

export default class CustomListItem extends Component {
  render() {
    return (
      <Header style={style.headerStyle}>
        <View
          style={{
            flex: 1
          }}
        >
          <Button
            transparent
            onPress={() => this.props.navigation.openDrawer()}
          >
            <Icon name="menu" ios="ios-menu" style={style.logo} />
          </Button>
        </View>

        <Text style={style.headerTitle}>
          Georgia Applicant Processing Service
        </Text>
        <View
          style={{
            flex: 1
          }}
        />
      </Header>
    );
  }
}

const style = StyleSheet.create({
  headerStyle: {
    // flex: 1,
    // flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    // width: "auto",
    height: 90,
    // height: 82,
    // backgroundColor: "#3e4649",
    // backgroundColor: "#6699CC",
    backgroundColor: "#3D454C",
    shadowRadius: 5,
    shadowOpacity: 1,
    // borderStyle: "solid",
    // borderWidth: 1,

    borderColor: "#707070"
  },
  logo: {
    color: "#ffffff",
    fontSize: 30
    // marginRight: Platform.OS === "ios" ? 0 : 35,
    // marginTop: Platform.OS === "ios" ? 0 : 20
  },
  headerTitle: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    fontFamily: "OpenSans-SemiBold",
    fontSize: 13,
    textAlign: "center",
    color: "#ffffff",
    flex: 3,
    flexWrap: "wrap"
  }
});
