import React, { PureComponent } from "react";
import { View, StyleSheet } from "react-native";

import { connect } from "react-redux";
import { Dropdown } from "react-native-material-dropdown";
import { TextField } from "react-native-material-textfield";
import height_data from "../../src/assets/json/height.json";
import payment_data from "../../src/assets/json/payments.json";

class SmartInput extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      name:
        this.props.input.name === "stateLicenseNumber" ||
        this.props.input.name === "state2"
          ? "state"
          : this.props.input.name
    };
  }

  _onValueChangeDecal = () => {
    const value = payment_data[2].description;
    return value;
  };

  _onValueChange = (value, onChange, setPaymentType) => {
    typeof setPaymentType === "function" ? setPaymentType(value) : null;

    if (this.props.type === "demographics") {
      const { name } = this.state;
      const obj = this.props[this.props.type][name].filter(item => {
        return item.description === value;
      });
      onChange(obj[0].fieldID);
    } else if (this.props.type === "height") {
      const obj = height_data.filter(item => {
        return item.description === value;
      });
      onChange(obj[0].fieldID);
    } else if (this.props.type === "payment") {
      const obj = payment_data.filter(item => {
        return item.description === value;
      });
      onChange(obj[0].fieldID);
    } else if (
      this.props.type === "ori" &&
      (this.props.UserInput.subgroup.subgrp_id === "PU" ||
        this.props.UserInput.subgroup.subgrp_id === "PR" ||
        this.props.UserInput.subgroup.subgrp_id === "CON" ||
        this.props.UserInput.subgroup.subgrp_id === "OTR" ||
        this.props.UserInput.subgroup.subgrp_id === "HD")
    ) {
      const obj = this.props.UserInput.subgroup.agency.filter((agency, i) => {
        return agency.name === value;
      });
      onChange(obj[0].ori);
    } else if (this.props.type === "") {
      const obj = payment_data.filter(item => {
        return item.description === value;
      });
      onChange(obj[0].fieldID);
    } else {
      onChange(value);
    }
  };

  _renderLabel = (label, meta) =>
    label
      ? meta.touched && meta.error
        ? `Required: ${label}`
        : meta.touched && meta.warning
        ? `Warning: ${meta.warning}`
        : label
      : "Please provide a label!";

  _renderDropdownData = type => {
    switch (type) {
      case "ori":
        if (
          this.props.UserInput.subgroup.subgrp_id === "PU" ||
          this.props.UserInput.subgroup.subgrp_id === "PR" ||
          this.props.UserInput.subgroup.subgrp_id === "CON" ||
          this.props.UserInput.subgroup.subgrp_id === "OTR" ||
          this.props.UserInput.subgroup.subgrp_id === "HD"
        ) {
          return this.props.UserInput.subgroup.agency
            .sort((a, b) => (a.name < b.name ? -1 : a.name > b.name ? 1 : 0))
            .map(agency => {
              return { value: agency.name };
            });
        } else {
          return this.props.UserInput.subgroup.agency.map(agency => {
            return { value: agency.ori };
          });
        }
      case "reason":
        return this.props.reasons.map(reason => {
          return { value: reason };
        });
      case "payment":
        return payment_data.map(item => {
          return { value: item.description };
        });
      case "suffix":
        return [
          { value: "none" },
          { value: "JR" },
          { value: "SR" },
          { value: "II" },
          { value: "III" },
          { value: "IV" }
        ];
      case "height":
        return height_data.map(item => {
          return { value: item.description };
        });

      case "demographics":
        if (this.state.name === "citizen" || this.state.name === "pob") {
          return this.props.demographics["citizen" || "pob"]
            .sort((a, b) =>
              a.description < b.description
                ? -1
                : a.description > b.description
                ? 1
                : 0
            )
            .reduce(
              (array, item) => {
                return array.concat({
                  value: item.description
                });
              },
              [
                {
                  value: "UNITED STATES",
                  id: 0,
                  fieldID: "US",
                  description: "UNITED STATES"
                }
              ]
            );
        } else {
          return this.props.demographics[this.state.name].map(item => {
            return { value: item.description };
          });
        }
      default:
        return [{ value: "Apple" }, { value: "Banana" }, { value: "Cherry" }];
    }
  };

  _ref_text_field = () => {
    const { input, auto_fill } = this.props;
    if (auto_fill) {
      input.onChange(auto_fill);
    }
  };

  _ref_drop_down = () => {
    const { input, auto_fill, setPaymentType } = this.props;
    if (auto_fill) {
      this._onValueChange(auto_fill, input.onChange, setPaymentType);
    }
  };

  render() {
    const {
      meta,
      input,
      label,
      type,
      grp_id,
      setPaymentType,
      disabledField,
      auto_fill,
      agency_info
    } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          {type === "EDGE_CASE" ? (
            <TextField
              onFocus={this.props.onFocus}
              value={agency_info}
              ref={this._ref_text_field}
              label={this._renderLabel(label, meta)}
              baseColor={
                meta.touched && meta.error
                  ? "rgb(178,34,34)"
                  : "rgba(0, 0, 0, .38)"
              }
              tintColor={"rgba(0, 0, 0, .38)"}
              onChangeText={input.onChange}
              onBlur={input.onBlur}
              disabled={disabledField ? disabledField.disabled : false}
            />
          ) : type === "text" ? (
            <TextField
              onFocus={this.props.onFocus}
              value={
                auto_fill
                  ? meta.pristine
                    ? auto_fill
                    : input.value
                  : grp_id === "DCH"
                  ? grp_id
                  : input.value
              }
              ref={this._ref_text_field}
              label={this._renderLabel(label, meta)}
              baseColor={
                meta.touched && meta.error
                  ? "rgb(178,34,34)"
                  : "rgba(0, 0, 0, .38)"
              }
              tintColor={"rgba(0, 0, 0, .38)"}
              onChangeText={input.onChange}
              onBlur={input.onBlur}
              disabled={disabledField ? disabledField.disabled : false}
            />
          ) : (
            <Dropdown
              ref={this._ref_drop_down}
              label={this._renderLabel(label, meta)}
              input={input.value}
              data={this._renderDropdownData(type)}
              onChangeText={value =>
                this._onValueChange(value, input.onChange, setPaymentType)
              }
              onBlur={input.onBlur}
              tintColor={"rgba(0, 0, 0, .38)"}
              baseColor={
                meta.touched && meta.error
                  ? "rgb(178,34,34)"
                  : "rgba(0, 0, 0, .38)"
              }
              value={auto_fill ? auto_fill : input.value}
              disabled={disabledField ? disabledField.disabled : false}
            />
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  UserInput: state.userInput.formData,
  demographics: state.demographics.data,
  reasons: state.userInput.reasons
});

export default connect(mapStateToProps)(SmartInput);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inputContainer: {
    paddingLeft: 40,
    paddingRight: 40
  }
});
